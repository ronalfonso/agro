import { MaterialIcons, SimpleLineIcons, Feather } from "@expo/vector-icons";

export const optionsForEditUser = [
  [
    {
      name: "name",
      label: "Nombre",
      type: "text",
      keyboardType: "default",
    },
    {
      name: "email",
      label: "E-mail",
      type: "text",
      keyboardType: "default",
    },
    {
      name: "document",
      label: "Documento",
      type: "text",
      keyboardType: "number-pad",
    },
    {
      name: "phone",
      label: "Telefono",
      type: "text",
      keyboardType: "number-pad",
    },
  ],
  [
    {
      name: "password_old",
      label: "Contraseña actual",
      type: "password",
      keyboardType: "default",
    },
    {
      name: "password",
      label: "Contraseña",
      type: "password",
      keyboardType: "default",
    },
    {
      name: "password_confirmation",
      label: "Confirmar contraseña",
      type: "password",
      keyboardType: "default",
    },
  ],
  [
    {
      name: "department_id",
      label: "Departamento",
      type: "select",
    },
    {
      name: "municipalite_id",
      label: "Municipio",
      type: "select",
    },
    {
      name: "address",
      label: "Direccion",
      type: "text",
      keyboardType: "default",
    },
    {
      name: "vereda",
      label: "Vereda",
      type: "text",
      keyboardType: "default",
    },
  ],
];

export const listOptionAccount = {
  publication: [
    {
      id: 1,
      title: "Publicaciones",
      icon: require("../assets/point-icon.png"),
      path: "list-products",
    },
    {
      id: 2,
      title: "Calificación",
      icon: require("../assets/heart-icon.png"),
      path: "",
    },
  ],
  configuration: [
    {
      id: 1,
      title: "Métricas",
      icon: require("../assets/metrics-icon.png"),
      path: "",
    },
    {
      id: 2,
      title: "Mi Perfil",
      icon: require("../assets/user-icon.png"),
      path: "user-profile",
    },
    {
      id: 3,
      title: "Ayuda",
      icon: require("../assets/question-icon.png"),
      path: "",
    },
  ],
};

export const optionUserEdit = [
  {
    id: 1,
    title: "Nombre, E-mail",
    iconLeft: require("../assets/user-icon.png"),
    iconRight: "arrow-right",
    label: "Edita tus datos",
    path: "SelectOpt",
  },
  {
    id: 2,
    title: "Contraseña",
    iconLeft: require("../assets/security-icon-green.png"),
    iconRight: "arrow-right",
    label: "Edita tus datos",
    path: "SelectOpt",
  },
  {
    id: 3,
    title: "Direcciones",
    label: "Modifica tus direcciones o agrega una nueva",
    iconLeft: require("../assets/address-icon.png"),
    iconRight: "arrow-right",
    path: "SelectOpt",
  },
];

export const optionsProfile = [
  {
    id: 1,
    title: "Mis Datos",
    label: "Gestion de datos personales",
    iconLeft: require("../assets/user-icon.png"),
    iconRight: "arrow-right",
    path: "user-info",
  },
  {
    id: 2,
    title: "Seguridad",
    label: "Configura tu seguridad",
    iconLeft: require("../assets/security-icon-green.png"),
    iconRight: "arrow-right",
    path: "user-document-selector",
  },
  {
    id: 3,
    title: "Privacidad",
    label: "Gestión del uso de información personal",
    iconLeft: require("../assets/privacy-icon-green.png"),
    iconRight: "arrow-right",
    path: "privacy-user",
  },
];

export const dataPolicy = [
  {
    id: 1,
    title: "Pedir reporte de datos",
    label:
      "Conoce cuales son los datos personales que guardamos y la actividad de tu cuenta",
    icon: <MaterialIcons name="post-add" size={24} color="#06C167" />,
  },
  {
    id: 2,
    title: "Configurar cookies",
    label:
      "Consulta los tipos de cookies que usamos para ayudarte a navegar en nuestro sitio web",
    icon: <SimpleLineIcons name="settings" size={24} color="#06C167" />,
  },
  {
    id: 3,
    title: "Conocer proceso de cancelacion de cuenta",
    label:
      "Conoce los pasos a seguir para cancelar tu cuenta y eliminar tus datos",
    icon: <Feather name="users" size={24} color="#06C167" />,
  },
];
