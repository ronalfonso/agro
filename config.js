import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

export const firebaseConfig = {
  apiKey: "AIzaSyBtY4cRv8ndsC1Y6q9ejEB2xaBry1VOn9Y",
  authDomain: "digiagro-app.firebaseapp.com",
  projectId: "digiagro-app",
  storageBucket: "digiagro-app.appspot.com",
  messagingSenderId: "311886612295",
  appId: "1:311886612295:web:0a11fbc2e202fa3c736e62",
  measurementId: "G-R70WZW96SS",
};

if (firebase.app.length) {
  firebase.initializeApp(firebaseConfig);
}
