// Import the effects and the middleware from redux-saga and react-native-offline
import { call, put, takeEvery, takeLatest, all } from 'redux-saga/effects';
import { networkSaga } from 'react-native-offline';


// Import the actions
import {REGISTER_USER_REQUEST, registerUserFailure, registerUserSuccess} from "../actions/offline";


// Import the API function to send the request
import {registerReferral} from "../actions/auth";


// Define the saga to handle the register user request
function* registerUserSaga(action) {
    try {
        // Call the API function with the payload
        const response = yield call(registerReferral, action.payload);
        // Check if the response is successful
        if (response.status === 200) {
            // Dispatch the success action with the response data
            yield put(registerUserSuccess(response.data));
        } else {
            // Dispatch the failure action with the response error
            yield put(registerUserFailure(response.error));
        }
    } catch (error) {
        // Dispatch the failure action with the error
        yield put(registerUserFailure(error));
    }
}

// Define the root saga to watch for the register user request action
function* rootSaga() {
    yield takeEvery(REGISTER_USER_REQUEST, registerUserSaga);
}

// Export the root saga and the network saga
export default function* () {
    yield all([rootSaga(), networkSaga()]);
};
