import { combineReducers } from "redux";

import authReducer from "./auth";
import categoriesReducer from "./categories";
import productsReducer from "./products";
import favoritiesReducer from "./favorities";
import storesReducer from "./stores";
import employeersReducer from "./employeers";
import filtersReducer from "./filter";
import noConnectionReducer from "./noConnection";
import notificationsReducer from "./notifications";
import trucks from "./trucks";
import referrals from "./referrals";
import {reducer as network} from 'react-native-offline';

const rootReducer = combineReducers({
  network,
  auth: authReducer,
  categories: categoriesReducer,
  products: productsReducer,
  favorities: favoritiesReducer,
  stores: storesReducer,
  employeers: employeersReducer,
  filters: filtersReducer,
  noConnection: noConnectionReducer,
  notifications: notificationsReducer,
  trucks: trucks,
  referrals: referrals,
});

export default rootReducer;
