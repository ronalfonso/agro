import {TRUCKS} from "../actions/trucks";


const initialState = {
    truckList: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case TRUCKS.TRUCK_LIST:
            return {
                ...state,
                truckList: action.trucks
            };
        default:
            return state;
    }
}