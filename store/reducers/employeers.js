import {
  LIST_JOBS,
  LIST_DETAIL_JOBS,
} from "../actions/employeers";

const initialState = {
  listJobs: [],
  listDetailJobs: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_JOBS:
      return {
        ...state,
        listJobs: action.listJobs,
      };
    case LIST_DETAIL_JOBS:
      return {
        ...state,
        listDetailJobs: action.listDetailJobs,
      };
    default:
      return state;
  }
};
