import { NO_CONNECTION, SET_API_ERRORS } from "../actions/noConnection";

const initialState = {
  noConnection: true,
  apiErrors: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case NO_CONNECTION:
      return {
        ...state,
        noConnection: action.noConnection,
      };
    case SET_API_ERRORS:
      return {
        ...state,
        apiErrors: action.apiErrors,
      };
    default:
      return state;
  }
};
