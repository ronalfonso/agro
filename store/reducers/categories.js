import {
  CATEGORI_JOBS,
  CATEGORI_PRODUCT,
  SIZES_PRODUCT,
  SUB_CATEGORI_PRODUCT,
  PRODUCT,
  CLEAR_LOADING_HOMES_CATEGORIES_JOBS,
  SET_LOADING_HOMES_CATEGORIES_JOBS,
  SET_LOADING_HOMES_CATEGORIES_PRODUCT,
  CLEAR_LOADING_HOMES_CATEGORIES_PRODUCT,
} from "../actions/categories";

const initialState = {
  categoriesJobs: {},
  categoriesProduct: [],
  subCategoriesProduct: [],
  product: [],
  sizesProduct: [],
  loadingCategoriesHomesProduct: true,
  loadingCategoriesHomesJobs: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CATEGORI_JOBS:
      return {
        ...state,
        categoriesJobs: action.categoriesJobs,
      };
    case CATEGORI_PRODUCT:
      return {
        ...state,
        categoriesProduct: action.categoriesProduct,
      };
    case SUB_CATEGORI_PRODUCT:
      return {
        ...state,
        subCategoriesProduct: action.subCategoriesProduct,
      };
    case PRODUCT:
      return {
        ...state,
        product: action.product,
      };
    case SIZES_PRODUCT:
      return {
        ...state,
        sizesProduct: action.sizesProduct,
      };
    case SET_LOADING_HOMES_CATEGORIES_PRODUCT:
      return {
        ...state,
        loadingCategoriesHomesProduct: action.loadingCategoriesHomesProduct,
      };
    case CLEAR_LOADING_HOMES_CATEGORIES_PRODUCT:
      return {
        ...state,
        loadingCategoriesHomesProduct: action.loadingCategoriesHomesProduct,
      };
    case SET_LOADING_HOMES_CATEGORIES_JOBS:
      return {
        ...state,
        loadingCategoriesHomesJobs: action.loadingCategoriesHomesJobs,
      };
    case CLEAR_LOADING_HOMES_CATEGORIES_JOBS:
      return {
        ...state,
        loadingCategoriesHomesJobs: action.loadingCategoriesHomesJobs,
      };
    default:
      return state;
  }
};
