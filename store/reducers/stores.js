import {
  STORES_ALL,
  STORES_DETAIL,
  CLEAR_LOADING_STORES_HOMES,
  SET_LOADING_STORES_HOMES,
} from "../actions/stores";

const initialState = {
  storesAll: {},
  storesDetail: {},
  loadingStoresHome: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case STORES_ALL:
      return {
        ...state,
        storesAll: action.storesAll,
      };
    case STORES_DETAIL:
      return {
        ...state,
        storesDetail: action.storesDetail,
      };
    case CLEAR_LOADING_STORES_HOMES:
      return { ...state, loadingStoresHome: action.loadingStoresHome };
    case SET_LOADING_STORES_HOMES:
      return { ...state, loadingStoresHome: action.loadingStoresHome };
    default:
      return state;
  }
};
