import {
  AUTHENTICATION,
  LOGOUT,
  MINICIPALITIES,
  DEPARTAMENT,
  SET_DID_TRY_AUTO_LOGIN,
  USER_DATA,
  USER_DATA_ADDRESS,
} from "../actions/auth";

const initialState = {
  didTryAutoLogin: false,
  token: null,
  userData: {},
  userAddress: {},
  municipalities: {},
  departament: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATION:
      return {
        ...state,
        token: action.token,
        didTryAutoLogin: true,
        userData: action.userData,
      };
    case USER_DATA:
      return {
        ...state,
        userData: action.userData,
      };
    case USER_DATA_ADDRESS:
      return {
        ...state,
        userAddress: action.userAddress,
      };
    case SET_DID_TRY_AUTO_LOGIN:
      return {
        ...state,
        didTryAutoLogin: true,
      };
    case MINICIPALITIES:
      return {
        ...state,
        municipalities: action.municipalities,
      };
    case DEPARTAMENT:
      return {
        ...state,
        departament: action.departament,
      };
    case LOGOUT:
      return {
        ...state,
        token: action.token,
        userData: action.userData,
        didTryAutoLogin: action.didTryAutoLogin,
      };

    default:
      return state;
  }
};
