import {REFERRALS} from "../actions/referrals";
import {REGISTER_USER_FAILURE, REGISTER_USER_REQUEST, REGISTER_USER_SUCCESS} from "../actions/offline";

const initialState = {
    referralDetail: {},
    referrals: {},
    referral: {},
    referralsFeatured: [],
    myReferrals: [],
    messageCreate: {},
    messageSendImage: {},
    loadingData: true,
    loadingAdd: true,
    loadingReferral: true,
    favoritesReferral: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case REFERRALS.MY_REFERRALS:
            return {
                ...state,
                myReferrals: action.payload,
            };
        case REFERRALS.REFERRAL_DETAIL:
            return {
                ...state,
                referralDetail: action.referralDetail,
            };
        case REGISTER_USER_REQUEST:
            return {
                ...state,
            };
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                referral: action.payload,
            };
        case REGISTER_USER_FAILURE:
            return {
                ...state,
            };

        default:
            return state;
    }
}