import {
  NOTIFICATIONS_TOTAL,
  LIST_NOTIFICATIONS,
} from "../actions/notifications";

const initialState = {
  totalNotification: false,
  listNotification: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATIONS_TOTAL:
      return {
        ...state,
        totalNotification: action.totalNotification,
      };
    case LIST_NOTIFICATIONS:
      return {
        ...state,
        listNotification: action.listNotification,
      };
    default:
      return state;
  }
};
