import {
  MESSAGE_DELETE,
  MESSAGE_ADD,
  SET_LOADING,
  CLEAR_LOADING,
  ALL_PRODUCTS_FAVORITIES,
  PRODUCTS_FAVORITIES,
  SET_LOADING_HOMES,
  CLEAR_LOADING_HOMES,
  FAVORITIES_ID,
} from "../actions/favorities";

const initialState = {
  messagesDelete: {},
  messagesAdd: {},
  allProductFavorities: [],
  productFavorities: [],
  loadingData: true,
  loadingDataHomes: true,
  favoritiesID: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ALL_PRODUCTS_FAVORITIES:
      return {
        ...state,
        allProductFavorities: action.allProductFavorities,
      };
    case PRODUCTS_FAVORITIES:
      return {
        ...state,
        productFavorities: action.productFavorities,
      };
    case MESSAGE_DELETE:
      return {
        ...state,
        messagesDelete: action.messagesDelete,
      };
    case MESSAGE_ADD:
      return {
        ...state,
        messagesAdd: action.messagesAdd,
      };
    case FAVORITIES_ID:
      return {
        ...state,
        favoritiesID: action.favoritiesID,
      };
    case SET_LOADING:
      return { ...state, loading: action.payload };
    case CLEAR_LOADING:
      return { ...state, loading: action.payload };
    case SET_LOADING_HOMES:
      return { ...state, loadingDataHomes: action.payload };
    case CLEAR_LOADING_HOMES:
      return { ...state, loadingDataHomes: action.payload };
    default:
      return state;
  }
};
