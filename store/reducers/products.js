import {
  PRODUCT_DETAIL,
  PRODUCTS,
  PRODUCTS_FEATURED,
  MY_PRODUCTS,
  MESSAGE_CREATE_PRODUCT,
  MESSAGE_SEND_IMAGE,
  CLEAR_LOADING,
  SET_LOADING,
  SET_LOADING_ADD,
  CLEAR_LOADING_ADD,
  SET_LOADING_HOMES_PRODUCT,
  CLEAR_LOADING_HOMES_PRODUCT,
  FAVORITIES_PRODUCT,
} from "../actions/products";

const initialState = {
  productDetail: {},
  products: {},
  productsFeatured: [],
  myProducts: [],
  messageCreate: {},
  messageSendImage: {},
  loadingData: true,
  loadingAdd: true,
  loadingProduct: true,
  favoritiesProduct: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTS:
      return {
        ...state,
        products: action.payload,
      };
    case FAVORITIES_PRODUCT:
      return {
        ...state,
        favoritiesProduct: action.favoritiesProduct,
      };
    case PRODUCTS_FEATURED:
      return {
        ...state,
        productsFeatured: action.productsFeatured,
      };
    case MY_PRODUCTS:
      return {
        ...state,
        myProducts: action.payload,
      };
    case PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: action.productDetail,
      };
    case MESSAGE_CREATE_PRODUCT:
      return {
        ...state,
        messageCreate: action.messageCreate,
      };
    case MESSAGE_SEND_IMAGE:
      return {
        ...state,
        messageSendImage: action.messageSendImage,
      };
    case SET_LOADING:
      return { ...state, loading: action.payload };
    case CLEAR_LOADING:
      return { ...state, loading: action.payload };
    case SET_LOADING_ADD:
      return { ...state, loadingAdd: action.payload };
    case CLEAR_LOADING_ADD:
      return { ...state, loadingAdd: action.payload };
    case SET_LOADING_HOMES_PRODUCT:
      return { ...state, loadingProduct: action.loadingProduct };
    case CLEAR_LOADING_HOMES_PRODUCT:
      return { ...state, loadingProduct: action.loadingProduct };
    default:
      return state;
  }
};
