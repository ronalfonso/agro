import {
  CATEGORI_FILTER,
  ORDER_FILTER,
  PRICE_FILTER,
  LOCATION_DEP_FILTER,
  LOCATION_MUNICIPIO_FILTER,
} from "../actions/filters";

const initialState = {
  categoriaFilter: {
    id: "",
    value: "",
    label: "",
  },
  orderFilter: {
    id: "",
    value: "",
    label: "",
  },
  priceFilter: {
    maxPrice: "",
    minPrice: "",
  },
  locationDepFilter: {
    id: "",
    value: "",
    label: "",
  },
  locationMuniFilter: {
    id: "",
    value: "",
    label: "",
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CATEGORI_FILTER:
      return {
        ...state,
        categoriaFilter: {
          ...state.categoriaFilter,
          id: action.payload.id,
          value: action.payload.value,
          label: action.payload.label,
        },
      };
    case ORDER_FILTER:
      return {
        ...state,
        orderFilter: {
          ...state.orderFilter,
          id: action.payload.id,
          value: action.payload.value,
          label: action.payload.label,
        },
      };
    case PRICE_FILTER:
      return {
        ...state,
        priceFilter: {
          ...state.priceFilter,
          maxPrice: action.payload.maxPrice,
          minPrice: action.payload.minPrice,
        },
      };
    case LOCATION_DEP_FILTER:
      return {
        ...state,
        locationDepFilter: {
          ...state.locationDepFilter,
          id: action.payload.id,
          value: action.payload.value,
          label: action.payload.label,
        },
      };
    case LOCATION_MUNICIPIO_FILTER:
      return {
        ...state,
        locationMuniFilter: {
          ...state.locationMuniFilter,
          id: action.payload.id,
          value: action.payload.value,
          label: action.payload.label,
        },
      };
    default:
      return state;
  }
};
