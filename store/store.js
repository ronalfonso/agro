// store.js

// Import the createStore, applyMiddleware and compose functions from redux
import { createStore, applyMiddleware } from 'redux';

// Import the createSagaMiddleware function from redux-saga
import createSagaMiddleware from 'redux-saga';

// Import the createNetworkMiddleware and createNetworkEnhancer functions from react-native-offline
import { createNetworkMiddleware } from 'react-native-offline';

// Import the root saga
import rootSaga from './saga/offline';
import rootReducer from "./reducers";
import ReduxThunk from "redux-thunk";



// Create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// Create the network middleware
const networkMiddleware = createNetworkMiddleware();

// Create the network enhancer
// const networkEnhancer = createNetworkEnhancer();

// Create the store with the root reducer, the saga middleware, the network middleware and the network enhancer
const store = createStore(
    rootReducer,
    // networkEnhancer, // Pass the network enhancer as second argument
    applyMiddleware(ReduxThunk, networkMiddleware, sagaMiddleware));

// Run the root saga
sagaMiddleware.run(rootSaga);

// Export the store
export default store;
