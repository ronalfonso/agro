import { productionServer } from "../../environment/environment";
import { getProductsAuthHomes } from "./products";

export const MESSAGE_DELETE = "MESSAGE_DELETE";
export const MESSAGE_ADD = "MESSAGE_ADD";
export const ALL_PRODUCTS_FAVORITIES = "ALL_PRODUCTS_FAVORITIES";
export const PRODUCTS_FAVORITIES = "PRODUCTS_FAVORITIES";
export const SET_LOADING = "SET_LOADING";
export const CLEAR_LOADING = "CLEAR_LOADING";
export const CLEAR_LOADING_HOMES = "CLEAR_LOADING_HOMES";
export const SET_LOADING_HOMES = "SET_LOADING_HOMES";
export const FAVORITIES_ID = "FAVORITIES_ID";

export const getProductFavorities = () => {
  return async (dispatch, getState) => {
    fetch(productionServer + `user-favorites`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    })
      .then((res) => res.json())
      .then(({ data }) => {
        dispatch({
          type: ALL_PRODUCTS_FAVORITIES,
          allProductFavorities: data,
        });
        dispatch(clearLoading());
      })
      .catch((error) => {
        console.log("ERROR", error);
      })
      .finally(() => {
        dispatch(clearLoading());
      });
  };
};

export const getProductFavoritiesHomes = () => {
  return async (dispatch, getState) => {
    fetch(productionServer + `user-favorites`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    })
      .then((res) => res.json())
      .then(({ data }) => {
        if (data) {
          const firstSixElements = data.slice(0, 4);
          dispatch({
            type: PRODUCTS_FAVORITIES,
            productFavorities: firstSixElements,
          });
        }
        dispatch(clearLoadingHomes());
      })
      .catch((error) => {
        console.log("ERROR", error);
        dispatch(clearLoadingHomes());
      })
      .finally(() => {
        dispatch(clearLoadingHomes());
      });
  };
};

export const deleteFavorities = (id) => {
  return async (dispatch, getState) => {
    fetch(productionServer + `favorite-delete/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    })
      .then((res) => res.json())
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        dispatch(getProductsAuthHomes());
      });
  };
};

export const deleteFavoritiesProduct = (id) => {
  return async (dispatch, getState) => {
    const response = await fetch(productionServer + `favorite-delete/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    });
    const data = await response.json();
    return data;
  };
};

export const addFavorities = (id) => {
  return async (dispatch, getState) => {
    const response = await fetch(productionServer + "favorite-add", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        product_user_id: id,
        type: "product_users",
      }),
    });
    const data = await response.json();
    return data;
  };
};

export const setLoading = () => ({
  type: SET_LOADING,
  payload: true,
});

const clearLoading = () => ({
  type: CLEAR_LOADING,
  payload: false,
});

export const setLoadingHomes = () => ({
  type: SET_LOADING_HOMES,
  payload: true,
});

const clearLoadingHomes = () => ({
  type: CLEAR_LOADING_HOMES,
  payload: false,
});
