import { productionServer } from "../../environment/environment";

export const CATEGORI_JOBS = "CATEGORI_JOBS";
export const CATEGORI_PRODUCT = "CATEGORI_PRODUCT";
export const SUB_CATEGORI_PRODUCT = "SUB_CATEGORI_PRODUCT";
export const PRODUCT = "PRODUCT";
export const SIZES_PRODUCT = "SIZES_PRODUCT";
export const SET_LOADING_HOMES_CATEGORIES_JOBS =
  "SET_LOADING_HOMES_CATEGORIES_JOBS";
export const CLEAR_LOADING_HOMES_CATEGORIES_JOBS =
  "CLEAR_LOADING_HOMES_CATEGORIES_JOBS";
export const SET_LOADING_HOMES_CATEGORIES_PRODUCT =
  "SET_LOADING_HOMES_CATEGORIES_PRODUCT";
export const CLEAR_LOADING_HOMES_CATEGORIES_PRODUCT =
  "CLEAR_LOADING_HOMES_CATEGORIES_PRODUCT";

export const getCategoriesJobs = () => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + "caregory-workers", {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch({ type: CATEGORI_JOBS, categoriesJobs: data });
    dispatch(clearLoadingHomesCategoriesJobs());
  };
};

export const getCategoriesProduct = () => {
  return async (dispatch, getState) => {
    const response = await fetch(productionServer + "category-product", {
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    });
    const data = await response.json();
    dispatch({ type: CATEGORI_PRODUCT, categoriesProduct: data });
    return data;
  };
};

export const getSubCategoriesProduct = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `subcategory-product/${id}`, {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: SUB_CATEGORI_PRODUCT, subCategoriesProduct: data });
  };
};

export const getProduct = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `product-list/${id}`, {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: PRODUCT, product: data });
  };
};

export const getUnidadProduct = () => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + "sizes", {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: SIZES_PRODUCT, sizesProduct: data });
    return data;
  };
};

export const setLoadingHomesCategoriesProduct = () => {
  return async (dispatch) => {
    dispatch({
      type: SET_LOADING_HOMES_CATEGORIES_PRODUCT,
      loadingCategoriesHomesProduct: true,
    });
  };
};

export const clearLoadingHomesCategoriesProduct = () => {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_LOADING_HOMES_CATEGORIES_PRODUCT,
      loadingCategoriesHomesProduct: false,
    });
  };
};

export const setLoadingHomesCategoriesJobs = () => {
  return async (dispatch) => {
    dispatch({
      type: SET_LOADING_HOMES_CATEGORIES_JOBS,
      loadingCategoriesHomesJobs: true,
    });
  };
};

export const clearLoadingHomesCategoriesJobs = () => {
  return async (dispatch) => {
    dispatch({
      type: CLEAR_LOADING_HOMES_CATEGORIES_JOBS,
      loadingCategoriesHomesJobs: false,
    });
  };
};
