import { productionServer } from "../../environment/environment";

export const PRODUCT_DETAIL = "PRODUCT_DETAIL";
export const MY_PRODUCTS = "MY_PRODUCTS";
export const PRODUCTS = "PRODUCTS";
export const PRODUCTS_FEATURED = "PRODUCTS_FEATURED";
export const MESSAGE_CREATE_PRODUCT = "MESSAGE_CREATE_PRODUCT";
export const MESSAGE_SEND_IMAGE = "MESSAGE_SEND_IMAGE";
export const SET_LOADING = "SET_LOADING";
export const CLEAR_LOADING = "CLEAR_LOADING";
export const SET_LOADING_ADD = "SET_LOADING_ADD";
export const CLEAR_LOADING_ADD = "CLEAR_LOADING_ADD";
export const CLEAR_LOADING_HOMES_PRODUCT = "CLEAR_LOADING_HOMES_PRODUCT";
export const SET_LOADING_HOMES_PRODUCT = "SET_LOADING_HOMES_PRODUCT";
export const FAVORITIES_PRODUCT = "FAVORITIES_PRODUCT";

export const getMyProducts = (pagination, page, setIsLoadingData) => {
  return async (dispatch, getState) => {
    const response = await fetch(
      productionServer + `user-products?pagination=${pagination}&page=${page}`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }
    );

    const { data, meta } = await response.json();
    dispatch({
      type: MY_PRODUCTS,
      payload: {
        data,
        to: meta.to,
        total: meta.total,
      },
    });
    return data;
  };
};

export const getProductsAuth = (categoriID) => {
  return async (dispatch, getState) => {
    const response = await fetch(
      productionServer +
        `all-products?pagination=${10}&page=${1}&categoria=${categoriID}&`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }
    );
    const { data, meta } = await response.json();

    const filteredData = data.filter((element) => {
      return element.favorite_id !== null;
    });
    const filteredIds = filteredData.map((element) => element.id);

    dispatch({
      type: PRODUCTS,
      payload: {
        data,
        to: meta.to,
        total: meta.total,
      },
    });
    dispatch({ type: FAVORITIES_PRODUCT, favoritiesProduct: filteredIds });

    return data;
  };
};

export const getProductsAuthHomes = () => {
  return async (dispatch, getState) => {
    fetch(productionServer + `all-products`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    })
      .then((res) => res.json())
      .then(({ data }) => {
        const firstSixElements = data.slice(0, 6);
        dispatch({
          type: PRODUCTS_FEATURED,
          productsFeatured: firstSixElements,
        });
        dispatch(clearLoadingProduct());
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getProductsOffline = () => {
  return async (dispatch) => {
    fetch(productionServer + `products`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then(({ data }) => {
        const firstSixElements = data.slice(0, 6);
        dispatch({ type: PRODUCTS, products: data });
        dispatch({
          type: PRODUCTS_FEATURED,
          productsFeatured: firstSixElements,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getProductsOfflineHomes = () => {
  return async (dispatch) => {
    fetch(productionServer + `products`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((res) => res.json())
      .then(({ data }) => {
        const firstSixElements = data.slice(0, 6);
        dispatch({
          type: PRODUCTS_FEATURED,
          productsFeatured: firstSixElements,
        });
        dispatch(clearLoadingProduct());
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const getProductDetail = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `product/${id}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const detailProduct = await response.json();
    return detailProduct;
  };
};

export const createProductUser = (
  user_id,
  product_id,
  size_id,
  price,
  stock,
  description,
  directionRegister,
  department_id,
  municipalite_id,
  address
) => {
  return async (dispatch, getState) => {
    let formdata = new FormData();
    formdata.append("user_id", user_id);
    formdata.append("product_id", product_id);
    formdata.append("size_id", size_id);
    formdata.append("price", price);
    formdata.append("stock", stock);
    formdata.append("is_active", "1");
    formdata.append("description", description);
    formdata.append("user_address", !!directionRegister ? 1 : 0);
    formdata.append("department_id", !!directionRegister ? "" : department_id);
    formdata.append(
      "municipalite_id",
      !!directionRegister ? "" : municipalite_id
    );
    formdata.append("address", !!directionRegister ? "" : address);
    formdata.append("latitude", "6.242433");
    formdata.append("longitude", "-75.55684");

    const response = await Promise.race([
      fetch(productionServer + "add-product-user", {
        method: "POST",
        headers: {
          "Content-Type": "multipart/form-data",
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const createImageProduct = (product_id, images) => {
  return async (dispatch, getState) => {
    let contadorData = images.length;

    var formdata = new FormData();
    formdata.append("imageable_id", product_id);
    formdata.append("imageable_type", "product_users");
    formdata.append("cover", "1");

    for (let index = 0; index < images.length; index++) {
      formdata.append(
        "imagen64",
        `data:image/jpeg;base64,${images[index].base64}`
      );

      const response = await Promise.race([
        fetch(productionServer + "upload-image64", {
          method: "POST",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${getState().auth.token}`,
          },
          body: formdata,
        }),
        new Promise((_, reject) =>
          setTimeout(() => reject(new Error("Timeout")), 25000)
        ),
      ]);

      contadorData--;
      const images64 = await response.json();
      if (contadorData === 0) {
        return images64;
      }
    }
  };
};

export const resetMessageCreate = () => {
  return async (dispatch) => {
    dispatch({ type: MESSAGE_CREATE_PRODUCT, messageCreate: {} });
  };
};

export const resetMessageImage = () => {
  return async (dispatch) => {
    dispatch({ type: MESSAGE_SEND_IMAGE, messageSendImage: {} });
  };
};

export const setLoading = () => ({
  type: SET_LOADING,
  payload: true,
});

const clearLoading = () => ({
  type: CLEAR_LOADING,
  payload: false,
});

export const setLoadingAdd = () => ({
  type: SET_LOADING_ADD,
  payload: true,
});

export const clearLoadingAdd = () => ({
  type: CLEAR_LOADING_ADD,
  payload: false,
});

export const setLoadingHomesProduct = () => ({
  type: SET_LOADING_HOMES_PRODUCT,
  loadingProduct: true,
});

export const clearLoadingProduct = () => ({
  type: CLEAR_LOADING_HOMES_PRODUCT,
  loadingProduct: false,
});
