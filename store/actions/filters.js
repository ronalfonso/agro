export const CATEGORI_FILTER = "CATEGORI_FILTER";
export const ORDER_FILTER = "ORDER_FILTER";
export const PRICE_FILTER = "PRICE_FILTER";
export const LOCATION_DEP_FILTER = "LOCATION_DEP_FILTER";
export const LOCATION_MUNICIPIO_FILTER = "LOCATION_MUNICIPIO_FILTER";

export const setFilterCategori = (categoria) => {
  return async (dispatch) => {
    dispatch({
      type: CATEGORI_FILTER,
      payload: {
        id: categoria.id,
        value: categoria.value,
        label: categoria.label,
      },
    });
  };
};

export const clearFilterCategori = () => {
  return async (dispatch) => {
    dispatch({
      type: CATEGORI_FILTER,
      payload: {
        id: "",
        value: "",
        label: "",
      },
    });
  };
};

export const setFilterOrder = (order) => {
  return async (dispatch) => {
    dispatch({
      type: ORDER_FILTER,
      payload: {
        id: order.id,
        value: order.value,
        label: order.label,
      },
    });
  };
};

export const clearFilterOrder = () => {
  return async (dispatch) => {
    dispatch({
      type: ORDER_FILTER,
      payload: {
        id: "",
        value: "",
        label: "",
      },
    });
  };
};

export const setFilterPrice = ({ maxPrice, minPrice }) => {
  return async (dispatch) => {
    dispatch({
      type: PRICE_FILTER,
      payload: {
        maxPrice: maxPrice,
        minPrice: minPrice,
      },
    });
  };
};

export const clearFilterPrice = () => {
  return async (dispatch) => {
    dispatch({
      type: PRICE_FILTER,
      payload: {
        maxPrice: "",
        minPrice: "",
      },
    });
  };
};

export const setFilterDepLocation = (departament) => {
  return async (dispatch) => {
    dispatch({
      type: LOCATION_DEP_FILTER,
      payload: {
        id: departament.id,
        value: departament.value,
        label: departament.label,
      },
    });
  };
};

export const clearFilterDepLocation = () => {
  return async (dispatch) => {
    dispatch({
      type: LOCATION_DEP_FILTER,
      payload: {
        id: "",
        value: "",
        label: "",
      },
    });
  };
};

export const setFilterMuniLocation = (mucipio) => {
  return async (dispatch) => {
    dispatch({
      type: LOCATION_MUNICIPIO_FILTER,
      payload: {
        id: mucipio.id,
        value: mucipio.value,
        label: mucipio.label,
      },
    });
  };
};

export const clearFilterMuniLocation = () => {
  return async (dispatch) => {
    dispatch({
      type: LOCATION_MUNICIPIO_FILTER,
      payload: {
        id: "",
        value: "",
        label: "",
      },
    });
  };
};
