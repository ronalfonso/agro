import {productionServer} from "../../environment/environment";
import axios from "axios";

export const TRUCKS = {
    TRUCK_LIST: 'TRUCK_LIST'
}

export const getTrucks = () => {
    return async (dispatch, getState) => {
        const response = await fetch(productionServer + "truck-list", {
            headers: {
                Accept: "application/json",
                Authorization: `Bearer ${getState().auth.token}`,
            },
        });
        const data = await response.json();
        dispatch({type: TRUCKS.TRUCK_LIST, trucks: data});
        return data;
    };
};
// {name, brand, model, year, image, type_truck_id}
export const createTruck = (data) => {
    return async (dispatch, getState) => {
        // let formdata = new FormData();
        // formdata.append("name", name);
        // formdata.append("brand", brand);
        // formdata.append("model", model);
        // formdata.append("year", year);
        // formdata.append("type_truck_id", type_truck_id);
        // formdata.append("image", image
        // );


        axios.post(
            productionServer + "truck-add",
            data,
            {
                headers: {
                    "Content-Type": "multipart/form-data",
                    Accept: "application/json",
                    Authorization: `Bearer ${getState().auth.token}`,
                }
            }
        )
            .then(resp => {
                console.log(resp);
                return resp;
            })
            .catch(error => console.error(error))

        // fetch(productionServer + "truck-add", {
        //     method: "POST",
        //     headers: {
        //         "Content-Type": "multipart/form-data",
        //         Accept: "application/json",
        //         Authorization: `Bearer ${getState().auth.token}`,
        //     },
        //     body: formdata,
        // })
        //     .then(response => response.json())
        //     .then(resp => {
        //         console.log('respuesta en servicio ', resp);
        //         return resp;
        //     })
        //     .catch(error => console.error(error))
    }
}

export const createTruck2 = ({
                                name,
                                brand,
                                model,
                                year,
                                image,
                                type_truck_id
                            }) => {
    return async (dispatch, getState) => {
        let formdata = new FormData();
        formdata.append("name", name);
        formdata.append("brand", brand);
        formdata.append("model", model);
        formdata.append("year", year);
        formdata.append("type_truck_id", type_truck_id);
        formdata.append("image", image);

        const response = await Promise.race([
            fetch(productionServer + "truck-add", {
                method: "POST",
                headers: {
                    "Content-Type": "multipart/form-data",
                    Accept: "application/json",
                    Authorization: `Bearer ${getState().auth.token}`,
                },
                body: formdata,
            }),
            new Promise((_, reject) =>
                setTimeout(() => reject(new Error("Timeout")), 25000)
            ),
        ]);

        const data = await response.json();
        return data;
    }
}