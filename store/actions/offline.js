export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

// Define the action to register a user
export const registerUser = user => ({
    type: REGISTER_USER_REQUEST,
    payload: user,
    meta: {
        retry: true,
    },
});

// Define the action to handle the success response
export const registerUserSuccess = user => ({
    type: REGISTER_USER_SUCCESS,
    payload: user,
});

// Define the action to handle the failure response
export const registerUserFailure = error => ({
    type: REGISTER_USER_FAILURE,
    payload: error,
});
