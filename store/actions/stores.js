import { productionServer } from "../../environment/environment";

export const STORES_ALL = "STORES_ALL";
export const STORES_DETAIL = "STORES_DETAIL";
export const CLEAR_LOADING_STORES_HOMES = "CLEAR_LOADING_STORES_HOMES";
export const SET_LOADING_STORES_HOMES = "SET_LOADING_STORES_HOMES";

export const getStoresList = () => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + "stores-list", {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch({ type: STORES_ALL, storesAll: data });
    return data;
  };
};

export const getStoresDetail = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `stores-list/${id}`, {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch({ type: STORES_DETAIL, storesDetail: data });
    return data
  };
};

export const setLoadingHomesStores = () => ({
  type: SET_LOADING_STORES_HOMES,
  loadingStoresHome: true,
});

const clearLoadingHomesStores = () => ({
  type: CLEAR_LOADING_STORES_HOMES,
  loadingStoresHome: false,
});
