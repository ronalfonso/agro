import AsyncStorage from "@react-native-async-storage/async-storage";
import { productionServer } from "../../environment/environment";
import { userData } from "../../environment/AsyncStorage";

export const AUTHENTICATION = "AUTHENTICATION";
export const LOGOUT = "LOGOUT";
export const DEPARTAMENT = "DEPARTAMENT";
export const MINICIPALITIES = "MINICIPALITIES";
export const SET_DID_TRY_AUTO_LOGIN = "SET_DID_TRY_AUTO_LOGIN";
export const USER_DATA = "USER_DATA";
export const USER_DATA_ADDRESS = "USER_DATA_ADDRESS";

export const authenticate = (data) => {
  return async (dispatch) => {
    const { user, access_token } = data;
    dispatch({
      type: AUTHENTICATION,
      token: access_token,
      userData: user,
    });
  };
};

export const login = (document, password, errors, setErrors, setStep) => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + "login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          user: document,
          password: password,
          type: "document",
        }),
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 15000)
      ),
    ]);

    const user = await response.json();

    switch (user.message) {
      case "La contraseña debe contener mínimo 6 caracteres":
        setErrors({
          ...errors,
          password: "La contraseña debe contener mínimo 6 caracteres",
        });
        break;
      case "La contraseña es incorrecta":
        setErrors({
          ...errors,
          password: "La contraseña es incorrecta",
        });
        break;
      case "El usuario no existe":
        setErrors({ ...errors, document: "El usuario no existe" });
        setStep((prev) => prev - 1);
        break;
      default:
        break;
    }

    if (response.status === 200) {
      dispatch(authenticate(user.data));
      saveDataToStorage(userData, "data", user.data);
    }
    return user;
  };
};

export const register = (credential) => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + "resgister-user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(credential),
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const registerReferral = credential => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + "resgister-user", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(credential),
      }),
      new Promise((_, reject) =>
          setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const getMunicipalities = (id) => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + `municipalities/${id}`, {
        headers: {
          Accept: "application/json",
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch({ type: MINICIPALITIES, municipalities: data });
  };
};

export const getDepartament = () => {
  return async (dispatch) => {
    const response = await Promise.race([
      fetch(productionServer + "departments", {
        headers: {
          Accept: "application/json",
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch({ type: DEPARTAMENT, departament: data });
    return data;
  };
};

export const logout = () => {
  return async (dispatch) => {
    await AsyncStorage.removeItem(userData);
    await dispatch({
      type: LOGOUT,
      token: null,
      userData: {},
      didTryAutoLogin: true,
    });
  };
};

export const saveDataToStorage = (id, key, data) => {
  AsyncStorage.setItem(
    id,
    JSON.stringify({
      [key]: data,
    })
  );
};

export const setDidTryAutoLogin = () => {
  return { type: SET_DID_TRY_AUTO_LOGIN };
};

export const loginOTP = (phone) => {
  return async () => {
    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");

    var formdata = new FormData();
    formdata.append("phone", phone);

    const response = await Promise.race([
      fetch(productionServer + "whatsapp-otp", {
        method: "POST",
        headers: myHeaders,
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    return data;
  };
};

export const validatePhone = (document) => {
  return async () => {
    let formdata = new FormData();
    formdata.append("documento", document);

    const response = await Promise.race([
      fetch(productionServer + "user/validate-document", {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    return data;
  };
};

export const forgotPassword = (password, password_confirmation, token) => {
  return async () => {
    let formdata = new FormData();
    formdata.append("password", password);
    formdata.append("password_confirmation", password_confirmation);
    formdata.append("token", token);

    const response = await Promise.race([
      fetch(productionServer + "update-password", {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const getInformationUser = () => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + "users/auth", {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: USER_DATA, userData: data.data.user });
    dispatch({ type: USER_DATA_ADDRESS, userAddress: data.data.address });
  };
};
