import { productionServer } from "../../environment/environment";
import { getInformationUser } from "./auth";

export const LIST_JOBS = "LIST_JOBS";
export const LIST_DETAIL_JOBS = "LIST_DETAIL_JOBS";
export const LIST_JOBS_CATEGORIES = "LIST_JOBS_CATEGORIES";

export const getListJobs = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `worker-list/category/${id}`, {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: LIST_JOBS, listJobs: data });
    return data;
  };
};

export const getDetailJobs = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `worker-list/job/${id}`, {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: LIST_DETAIL_JOBS, listDetailJobs: data });
  };
};

export const registerEmployee = (creadentials) => {
  return async (dispatch, getState) => {
    const { category_worker_id, user_id, description, cv } = creadentials;
    console.log(cv);

    var formdata = new FormData();
    formdata.append("category_worker_id", category_worker_id);
    formdata.append("user_id", user_id);
    formdata.append("description", description);
    formdata.append("cv", cv);
    formdata.append("status", "1");

    const response = await Promise.race([
      fetch(productionServer + `worker-employee`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch(getInformationUser());
    return data;
  };
};

export const registerEmployeer = (credentials) => {
  return async (dispatch, getState) => {
    const {
      category_worker_id,
      user_id,
      name,
      salary_from,
      description,
      benefits,
      directionRegister,
      department_id,
      municipalite_id,
      address,
    } = credentials;
    var formdata = new FormData();
    formdata.append("category_worker_id", category_worker_id);
    formdata.append("user_id", user_id);
    formdata.append("name", name);
    formdata.append("salary_from", salary_from);
    formdata.append("description", description);
    formdata.append("benefits", benefits);
    formdata.append("status", "1");
    formdata.append("user_address", !!directionRegister ? 1 : 0);
    formdata.append("department_id", !!directionRegister ? "" : department_id);
    formdata.append(
      "municipalite_id",
      !!directionRegister ? "" : municipalite_id
    );
    formdata.append("address", !!directionRegister ? "" : address);
    const response = await Promise.race([
      fetch(productionServer + `worker-add`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const validCvEmployee = () => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `my-cv`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};
