import { productionServer } from "../../environment/environment";
import { getInformationUser } from "./auth";

export const editProfile = (name, document, email, code, phone) => {
  return async (dispatch, getState) => {
    let formdata = new FormData();
    formdata.append("name", name);
    formdata.append("document", document);
    formdata.append("email", email);
    formdata.append("code", code);
    formdata.append("phone", phone);

    const response = await fetch(productionServer + "update-mydata", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
      body: formdata,
    });
    const data = await response.json();
    dispatch(getInformationUser());
    return data;
  };
};

export const editPassword = (
  old_password,
  password,
  password_confirmation,
  errors,
  setErrors
) => {
  return async (dispatch, getState) => {
    var formdata = new FormData();
    formdata.append("password_old", old_password);
    formdata.append("password", password);
    formdata.append("password_confirmation", password_confirmation);

    const response = await fetch(productionServer + "update-mypassword", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
      body: formdata,
    });
    const data = await response.json();

    if (data.message === "La anterior contraseña es incorrecta") {
      setErrors({
        ...errors,
        password_old: "La anterior contraseña es incorrecta",
      });
    }
    dispatch(getInformationUser());

    return data;
  };
};

export const editAddress = (
  department_id,
  municipalite_id,
  address,
  vereda
) => {
  return async (dispatch, getState) => {
    var formdata = new FormData();
    formdata.append("department_id", department_id);
    formdata.append("municipalite_id", municipalite_id);
    formdata.append("address", address);
    formdata.append("vereda", vereda);

    const response = await fetch(productionServer + "update-myaddress", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
      body: formdata,
    });
    const data = await response.json();
    dispatch(getInformationUser());
    return data;
  };
};

export const handleUpdateProfile = (photo) => {
  return async (dispatch, getState) => {
    var formdata = new FormData();
    formdata.append("profile_photo", `data:image/jpeg;base64,${photo}`);

    const response = await fetch(productionServer + "user/profile-photo", {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
      body: formdata,
    });

    const data = await response.json();
    dispatch(getInformationUser());
    return data;
  };
};

export const deletePhotoProfile = () => {
  return async (dispatch, getState) => {
    const response = await fetch(
      productionServer + "user/delete-profile-photo",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }
    );

    const data = await response.json();
    dispatch(getInformationUser());
    return data;
  };
};
