import { productionServer } from "../../environment/environment";

export const NOTIFICATIONS_TOTAL = "NOTIFICATIONS_TOTAL";
export const LIST_NOTIFICATIONS = "LIST_NOTIFICATIONS";

export const saveTokenDevice = (token) => {
  return async (dispatch, getState) => {
    var formdata = new FormData();
    formdata.append("device_key", token);

    const response = await Promise.race([
      fetch(productionServer + "user/devicekey", {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const getTotalNotification = () => {
  return async (dispatch, getState) => {
    const response = await fetch(productionServer + "notification/total", {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${getState().auth.token}`,
      },
    });

    const data = await response.json();
    if (data.status === "Success") {
      dispatch({
        type: NOTIFICATIONS_TOTAL,
        totalNotification: data.data.total,
      });
    }
  };
};

export const getListNotification = () => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + "notifcation/list", {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    dispatch({ type: LIST_NOTIFICATIONS, listNotification: data.data });
    return data;
  };
};

export const seeDetailNotification = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `notification/view/${id}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);
    const data = await response.json();
    return data;
  };
};

export const changeStatusNotification = (id) => {
  return async (dispatch, getState) => {
    const response = await Promise.race([
      fetch(productionServer + `notification/read/${id}`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    dispatch(getListNotification());
    dispatch(getTotalNotification())
    return data;
  };
};

export const grantedPermissionNotification = (isActive) => {
  return async (dispatch, getState) => {
    var formdata = new FormData();
    formdata.append("is_notification", isActive);

    const response = await Promise.race([
      fetch(productionServer + `user/is-notification`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${getState().auth.token}`,
        },
        body: formdata,
      }),
      new Promise((_, reject) =>
        setTimeout(() => reject(new Error("Timeout")), 25000)
      ),
    ]);

    const data = await response.json();
    return data;
  };
};
