import {productionServer} from "../../environment/environment";
import {MY_PRODUCTS} from "./products";


export const REFERRALS = {
    MY_REFERRALS: 'MY_REFERRALS',
    REFERRAL_DETAIL: 'REFERRAL_DETAIL'
}

export const getMyReferrals = (pagination, page, setIsLoadingData) => {
    return async (dispatch, getState) => {
        const response = await fetch(
            productionServer + `referrals`,
            {
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    Authorization: `Bearer ${getState().auth.token}`,
                },
            }
        );

        const { data } = await response.json();
        dispatch({
            type: REFERRALS.MY_REFERRALS,
            payload: {
                data,
            },
        });
        return data;
    };
};