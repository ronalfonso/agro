import NetInfo from "@react-native-community/netinfo";

export const NO_CONNECTION = "NO_CONNECTION";
export const SET_API_ERRORS = "SET_API_ERRORS";

export const checkInternetConnection = async () => {
  return async (dispatch) => {
    const netInfoResponse = await NetInfo.fetch();
    const isConnected = netInfoResponse.isConnected;
    dispatch({ type: NO_CONNECTION, noConnection: isConnected });
    return isConnected;
  };
};
