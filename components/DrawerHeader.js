import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { RFPercentage } from "react-native-responsive-fontsize";
import { useSelector } from "react-redux";
import {
  percentageToDPHeight,
  percentageToDPWidth,
} from "../utils/CalculatePD";

const DrawerHeader = () => {
  const userData = useSelector((state) => state.auth.userData);

  return (
    <View style={{ ...styles.screen, backgroundColor: "#06C167" }}>
      <View style={styles.flex}>
        <Image
          source={
            userData.profile_photo_path !== null
              ? {
                  uri: userData.profile_photo_path,
                }
              : require("../assets/user-circle-white.png")
          }
          style={{
            height: percentageToDPHeight(70),
            width: percentageToDPWidth(70),
            borderRadius: 100,
          }}
        />
        <Text style={styles.nameText}>Hola, {userData.name}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flexDirection: "row",
    height: hp(15),
    alignItems: "center",
    justifyContent: "space-between",
    paddingRight: 15,
    marginBottom: 15,
  },
  flex: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  nameText: {
    color: "#000",
    fontFamily: "Poppins-medium",
    fontSize: RFPercentage(2),
    textAlign: "center",
    marginLeft: 15,
  },
});

export default DrawerHeader;
