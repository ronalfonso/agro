import React from "react";
import { FontAwesome } from "@expo/vector-icons";
import { StyleSheet, View, Text } from "react-native";
import { SelectList as Select } from "react-native-dropdown-select-list";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

const SelectList = ({
  label,
  error,
  placeholder,
  data,
  setSelected,
  defaultOption,
}) => {
  return (
    <View style={styles.containerSelect}>
      <Text style={styles.textLabel}>{label}:</Text>
      <Select
        data={data}
        setSelected={setSelected}
        search={false}
        placeholder={placeholder}
        boxStyles={styles.boxStyles}
        dropdownStyles={styles.dropdownStyles}
        inputStyles={styles.inputStyles}
        dropdownTextStyles={styles.dropdownTextStyles}
        arrowicon={<FontAwesome name="chevron-down" size={12} color={"#000"} />}
        defaultOption={defaultOption}
      />
      <Text style={styles.textError} numberOfLines={1}>
        {error}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containerSelect: {
    width: wp(90),
    marginBottom: percentageToDPHeight(18),
  },

  textLabel: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins",
    color: "#212121",
  },

  boxStyles: {
    width: wp(90),
    borderWidth: 1,
    borderColor: "#000000",
    borderRadius: 5,
    fontSize: percentageToDPSize(14),
    zIndex: -1,
    fontFamily: "Poppins-medium",
    alignItems: "center",
  },

  dropdownStyles: {
    width: wp(90),
    backgroundColor: "#FFFF",
    borderRadius: 5,
    borderWidth: 1,
    justifyContent: "center",
  },

  inputStyles: {
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
  },

  dropdownTextStyles: {
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
    borderBottomWidth: 1,
    borderBottomColor: "#212121",
    color: "#000",
  },

  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },
});

export default SelectList;
