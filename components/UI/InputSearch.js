import React from "react";

import { Feather } from "@expo/vector-icons";
import { View, Text, TextInput, StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

const InputSearch = ({ placeholder, setValue, value }) => {
  return (
    <View style={styles.containerSearch}>
      <Feather name="search" size={24} style={styles.iconSearch} />
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        onChangeText={(text) => {
          setValue(text);
        }}
        value={value}
      />
    </View>
  );
};

export default InputSearch;

const styles = StyleSheet.create({
  containerSearch: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  input: {
    height: 50,
    paddingLeft: wp(14),
    backgroundColor: "#F4F4F4",
    width: wp(90),
    fontFamily: "Poppins",
    borderRadius: 10,
  },
  iconSearch: {
    color: "rgba(33,33,33,0.5)",
    position: "absolute",
    zIndex: 10,
    bottom: hp(1.7),
    left: wp(5),
  },
});
