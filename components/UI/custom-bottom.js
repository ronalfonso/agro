import React from "react";
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

const LargeButton = ({ isLoading, handleSubmit, text, disable }) => {
  return (
    <TouchableOpacity
      style={styles.containerButton}
      onPress={handleSubmit}
      disabled={disable}
      activeOpacity={0.5}
    >
      {isLoading ? (
        <ActivityIndicator size="small" color="#fff" />
      ) : (
        <Text style={styles.textButton}>{text}</Text>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerButton: {
    width: wp(90),
    height: percentageToDPHeight(56),
    backgroundColor: "#06C167",
    borderRadius: 5,
    justifyContent: "center",
  },
  textButton: {
    fontFamily: "Poppins",
    textAlign: "center",
    fontSize: percentageToDPSize(16),
    color: "#FFFFFF",
  },
});

export default LargeButton;
