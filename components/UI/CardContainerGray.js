import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useSelector } from "react-redux";

const CardContainerGray = ({
  children,
  title,
  footer,
  titleFooter,
  path,
  props,
  positionFooter,
}) => {
  const token = useSelector((state) => state.auth.token);
  return (
    <View style={styles.containerCard}>
      {title !== false && <Text style={styles.title}>{title}</Text>}
      {children}
      {footer !== false && (
        <TouchableOpacity
          style={{ ...positionFooter, paddingHorizontal: 10 }}
          onPress={() => {
            if (token !== null) {
              props.navigate.navigate(path);
            } else if (token === null) {
              props.navigate.navigate("WelcomeScreen");
            }
          }}
        >
          <Text style={styles.titleFooter}>{titleFooter}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default CardContainerGray;

const styles = StyleSheet.create({
  title: {
    fontFamily: "Poppins-medium",
    paddingHorizontal: 10,
    fontSize: fz(2.5),
  },
  containerCard: {
    width: wp(90),
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderRadius: 10,
    backgroundColor: "#F4F4F4",
  },
  titleFooter: {
    fontFamily: "Poppins",
    fontSize: fz(1.8),
    color: "#4B64EC",
  },
});
