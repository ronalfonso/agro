import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import {
  percentageToDPWidth,
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";
import { Ionicons } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const InputTextBorder = ({
  name,
  value,
  setCredentials,
  creadentials,
  label,
  error,
  setErrors,
  errorsItem,
  password,
  keyboardType,
  footerText,
}) => {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <View style={styles.containerInput}>
      <View style={styles.contentInput}>
        <TextInput
          keyboardType={keyboardType}
          style={{
            ...styles.customInput,
            borderBottomColor: error !== "" ? "#ff0e0e" : "#000",
          }}
          value={value}
          placeholder={label}
          onChangeText={(text) => {
            setCredentials({ ...creadentials, [name]: text });
          }}
          onFocus={() => {
            setErrors({ ...errorsItem, [name]: "" });
          }}
          secureTextEntry={password && !showPassword}
        />
        {password && value.length !== 0 && (
          <TouchableOpacity
            style={styles.containerIconPassword}
            onPress={() => {
              setShowPassword((prev) => !prev);
            }}
          >
            {showPassword ? (
              <Ionicons name="ios-eye-off-outline" size={20} color="#686868" />
            ) : (
              <Ionicons name="ios-eye-outline" size={20} color="#686868" />
            )}
          </TouchableOpacity>
        )}
      </View>
      <Text
        style={
          error === "" ? { ...styles.textFooter } : { ...styles.textError }
        }
        numberOfLines={1}
      >
        {error === "" ? footerText : error}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containerInput: {
    width: wp(90),
    marginBottom: percentageToDPHeight(20),
  },
  textLabel: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins",
    color: "#212121",
  },
  contentInput: {
    width: wp(90),
    height: percentageToDPHeight(45),
  },
  customInput: {
    width: wp(90),
    height: percentageToDPHeight(45),
    borderBottomWidth: 1,
    borderRadius: 5,
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
  },

  containerIconPassword: {
    width: percentageToDPWidth(40),
    height: percentageToDPHeight(45),
    position: "absolute",
    right: 0,
    zIndex: 55,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
  },

  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },

  textFooter: {
    color: "#21212180",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },
});

export default InputTextBorder;
