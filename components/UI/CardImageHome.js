import React from "react";
import { ImageBackground, Text, TouchableOpacity, View } from "react-native";
import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useSelector } from "react-redux";

const CardImageHome = ({
  props,
  children,
  image,
  text,
  colorBottom,
  backgroundBottom,
  path,
  position,
  sizeBottom,
}) => {
  const token = useSelector((state) => state.auth.token);

  return (
    <ImageBackground
      resizeMode="stretch"
      source={image}
      style={styles.imageBackground}
    >
      <View style={{ ...styles.contentImage, ...position }}>
        {children}
        <TouchableOpacity
          onPress={() => {
            if (token !== null) {
              props.navigation.navigate(path);
            } else if (token === null) {
              props.navigation.navigate("WelcomeScreen");
            }
          }}
          style={{
            ...styles.containerBottom,
            ...sizeBottom,
            backgroundColor: backgroundBottom,
          }}
        >
          <Text style={{ ...styles.textBtn, color: colorBottom }}>{text}</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default CardImageHome;

const styles = StyleSheet.create({
  imageBackground: {
    borderRadius: 10,
    width: wp(90),
    padding: 10,
    height: hp(25),
  },
  contentImage: {
    borderRadius: 10,
    height: "100%",
    width: "100%",
  },
  containerBottom: {
    borderRadius: 8,
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingVertical: 10,
    alignItems: "center",
    marginTop: hp(1),
  },
  textBtn: {
    fontFamily: "Poppins-bold",
    fontSize: fz(1.9),
  },
});
