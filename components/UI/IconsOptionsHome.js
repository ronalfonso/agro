import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useSelector } from "react-redux";

export const data = [
  {
    id: 1,
    text: "Tiendas de suministros",
    icon: require("../../assets/Home/icon3.png"),
    path: "StoreSupplies",
  },
  {
    id: 2,
    text: "Bolsa de empleos",
    icon: require("../../assets/Home/icon4.png"),
    path: "job-board",
  },
  {
    id: 3,
    text: "Ofertas de transportes",
    icon: require("../../assets/Home/icon2.png"),
    path: "OffertTransporter",
  },
  {
    id: 4,
    text: "Bolsa de precios",
    icon: require("../../assets/Home/icon1.png"),
    path: "PriceExchange",
  },
];

const IconsOptionsHome = ({ props }) => {
  const token = useSelector((state) => state.auth.token);

  return (
    <View style={styles.containerIcons}>
      {data.map(({ id, text, icon, path }) => {
        return (
          <View
            style={{ flexDirection: "column", alignItems: "center" }}
            key={id}
          >
            <TouchableOpacity
              style={styles.itemIcon}
              onPress={() => {
                if (token !== null) {
                  props.navigation.navigate(path);
                } else if (token === null) {
                  props.navigation.navigate("WelcomeScreen");
                }
              }}
            >
              <Image source={icon} />
            </TouchableOpacity>
            <Text style={styles.textIcon}>{text}</Text>
          </View>
        );
      })}
    </View>
  );
};

export default IconsOptionsHome;

const styles = StyleSheet.create({
  containerIcons: {
    width: wp(100),
    paddingHorizontal: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  itemIcon: {
    height: 70,
    width: 70,
    marginHorizontal: wp(2),
    borderRadius: 10,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#06C167",
  },
  textIcon: {
    width: 70,
    paddingTop: hp(1),
    fontSize: fz(1.2),
    textAlign: "center",
    fontFamily: "Poppins",
  },
});
