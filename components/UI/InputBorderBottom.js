import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";

const InputBorderBottom = ({ placeholder, label, type, error }) => {
  return (
    <View style={styles.containerInput}>
      <View
        style={{
          ...styles.borderInput,
          borderBottomColor: error !== "" ? "#FF9494" : "#000000",
        }}
      >
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          placeholderTextColor={"#000"}
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType="next"
          maxLength={40}
        />
        {type === "password" && (
          <TouchableOpacity style={styles.containerVisiblePassword}>
            <Ionicons name="eye-outline" size={24} color="#212121" />
            {/* <Ionicons name="eye-off-outline" size={24} color="#212121" /> */}
          </TouchableOpacity>
        )}
      </View>
      {error !== "" ? (
        <Text style={styles.textBottomError}>{error}</Text>
      ) : (
        <Text style={styles.textBottom}>{label}</Text>
      )}
    </View>
  );
};

export default InputBorderBottom;

const styles = StyleSheet.create({
  containerInput: {
    paddingHorizontal: 20,
    marginBottom: hp(1),
  },

  borderInput: {
    borderBottomWidth: 1,
    position: "relative",
  },

  input: {
    fontSize: fz(2),
    fontFamily: "Poppins-medium",
  },

  textBottom: {
    fontSize: fz(1.4),
    fontFamily: "Poppins",
    color: "rgba(33,33,33,0.5)",
    paddingBottom: hp(3),
    width: wp(90),
  },

  textBottomError: {
    fontSize: fz(1.4),
    fontFamily: "Poppins",
    color: "#FF9494",
    paddingBottom: hp(3),
    width: wp(90),
  },

  containerVisiblePassword: {
    position: "absolute",
    right: 0,
    bottom: 5,
  },
});
