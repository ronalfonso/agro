import React from "react";
import { StyleSheet, View, Text } from "react-native";
import {
  percentageToDPWidth,
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
} from "react-native-confirmation-code-field";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const InputCellCode = ({
  label,
  CELL_COUNT,
  name,
  value,
  setValue,
  credentials,
  errors,
  error,
  setErrors,
}) => {
  const inputReference = useBlurOnFulfill({ value, cellCount: CELL_COUNT });

  return (
    <View style={styles.containerInput}>
      <Text style={styles.textLabel}>{label}:</Text>
      <CodeField
        ref={inputReference}
        value={value}
        onChangeText={(text) => {
          setValue({ ...credentials, [name]: text });
          setErrors({ ...errors, [name]: "" });
        }}
        cellCount={6}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({ index, symbol, isFocused }) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
          >
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />
      <Text style={styles.textError}>{error}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containerInput: {
    width: wp(90),
    height: percentageToDPHeight(110),
    marginBottom: percentageToDPHeight(18),
  },
  textLabel: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins",
    color: "#212121",
  },
  contentInput: {
    width: wp(90),
    height: percentageToDPHeight(56),
  },
  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },

  codeFieldRoot: {
    width: wp(90),
  },

  cell: {
    borderColor: "#06C167",
    width: percentageToDPWidth(49.65),
    height: percentageToDPHeight(50.56),
    color: "#06C167",
    fontSize: percentageToDPSize(22),
    fontFamily: "Poppins-medium",
    borderWidth: 1,
    borderRadius: 5,
    textAlign: "center",
    padding: percentageToDPSize(10),
  },

  focusCell: {
    borderColor: "#06C167",
  },
});

export default InputCellCode;
