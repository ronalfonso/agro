import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";

const EmptyDateScreen = ({ botton, title, subtitle, path, titleBtn }) => {
  return (
    <View style={{ height: hp(100), backgroundColor: "#fff" }}>
      <View style={styles.containerEmpty}>
        <View style={styles.cardEmpty}>
          <Text style={styles.titleText}>{title}</Text>
          <Text style={styles.subTitleText}>{subtitle}</Text>

          {/* <TouchableOpacity style={styles.button}>
          <Text style={styles.textButton}>Publicar ahora</Text>
        </TouchableOpacity> */}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerEmpty: {
    width: wp(100),
    height: percentageToDPHeight(400),
    alignItems: "center",
    marginTop: percentageToDPWidth(50),
  },

  cardEmpty: {
    backgroundColor: "#F4F4F4",
    borderRadius: 10,
    width: wp(90),
    height: percentageToDPHeight(400),
    alignItems: "center",
    paddingVertical: 50,
  },

  titleText: {
    color: "#212121",
    textAlign: "center",
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins-medium",
  },
  subTitleText: {
    width: percentageToDPWidth(300),
    color: "#686868",
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
  },
  button: {
    marginTop: 20,
  },

  textButton: {
    textAlign: "center",
    color: "#5385E4",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(16),
  },
});

export default EmptyDateScreen;
