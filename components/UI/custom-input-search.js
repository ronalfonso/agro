import React from "react";
import { StyleSheet, TextInput, View } from "react-native";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Feather } from "@expo/vector-icons";

const TextInputSearch = ({ placeholder, value, setValue }) => {
  return (
    <View style={styles.containerInput}>
      <View style={styles.container}>
        <Feather
          name="search"
          size={24}
          style={styles.iconSearch}
          color="#21212180"
        />
        <TextInput
          style={styles.customInput}
          placeholder={placeholder}
          value={value}
          onChangeText={(text) => {
            setValue(text);
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerInput: {
    height: percentageToDPHeight(80),
    width: wp(90),
    borderBottomColor: "rgba(33, 33, 33, 0.50)",
    borderBottomWidth: 1,
    justifyContent: "center",
  },

  customInput: {
    width: wp(90),
    height: percentageToDPHeight(45),
    borderRadius: 10,
    backgroundColor: "#F4F4F4",
    paddingLeft: percentageToDPWidth(60),
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
  },

  container: {
    width: wp(90),
    height: percentageToDPHeight(45),
    position: "relative",
  },

  iconSearch: {
    position: "absolute",
    zIndex: 55,
    top: percentageToDPHeight(10),
    left: percentageToDPWidth(20),
  },
});

export default TextInputSearch;
