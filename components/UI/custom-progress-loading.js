import React from "react";
import { StyleSheet, View, Text } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import * as Progress from "react-native-progress";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";

const ProgressLoading = ({ label }) => {
  return (
    <View style={styles.containerLoading}>
      <Progress.CircleSnail
        color={["#06C167", "#000"]}
        size={70}
        strokeCap={"square"}
        style={{ bottom: percentageToDPHeight(50) }}
      />
      {!!label && <Text style={styles.label}>Validando datos...</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  containerLoading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFF",
    width: wp(100),
    height: hp(100),
  },
  label: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(22),
    marginTop: 20,
  },
});

export default ProgressLoading;
