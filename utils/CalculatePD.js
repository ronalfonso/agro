import { Dimensions } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export const percentageToDPWidth = (size) => {
  const { width } = Dimensions.get("window");
  const widthPercentage = (size / width) * 100;
  const widthInDP = wp(widthPercentage);
  return widthInDP;
};

export const percentageToDPHeight = (size) => {
  const { height } = Dimensions.get("window");
  const heightPercentage = (size / height) * 100;
  const heightInDP = hp(heightPercentage);
  return heightInDP;
};

export const percentageToDPSize = (size) => {
  const { height } = Dimensions.get("window");
  const sizePercentage = (size / height) * 100;
  const sizeInDP = RFPercentage(sizePercentage);
  return sizeInDP;
};
