import { useDispatch } from "react-redux";
import { FAVORITIES_PRODUCT } from "../store/actions/products";
import {
  addFavorities,
  deleteFavoritiesProduct,
} from "../store/actions/favorities";
import useMessage from "./useMessage";
import { useToast } from "react-native-toast-notifications";

const useFavoriteProduct = (favoritiesID) => {
  const dispatch = useDispatch();
  const Toast = useToast();
  const { showMessage } = useMessage(Toast);

  const addProduct = (product_id) => {
    dispatch({
      type: FAVORITIES_PRODUCT,
      favoritiesProduct: [...favoritiesID, product_id],
    });
    dispatch(addFavorities(product_id));
    showMessage("¡Se agregó el producto a favoritos!");
  };

  const removeProduct = (product_id) => {
    const index = favoritiesID.indexOf(product_id);
    if (index > -1) {
      dispatch({
        type: FAVORITIES_PRODUCT,
        favoritiesProduct: favoritiesID
          .slice(0, index)
          .concat(favoritiesID.slice(index + 1)),
      });
      dispatch(deleteFavoritiesProduct(product_id));
      showMessage("¡Se eliminó el producto de favoritos!");
    }
  };

  return { addProduct, removeProduct };
};

export default useFavoriteProduct;
