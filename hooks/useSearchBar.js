import React, { useState, useRef, useEffect } from "react";
import {
  TouchableOpacity,
  Animated,
  View,
  TextInput,
  StyleSheet,
  Text,
} from "react-native";
import { Feather, AntDesign, FontAwesome5 } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useSelector } from "react-redux";
import uuid from "react-native-uuid";

const UseSearchBar = () => {
  const token = useSelector((state) => state.auth.token);
  const [isFocused, setIsFocused] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [searchHistory, setSearchHistory] = useState([]);
  const textInputRef = useRef(null);

  const handleButtonPress = () => {
    textInputRef.current.blur();
  };

  useEffect(() => {
    loadSearchHistory();
  }, [token]);

  const loadSearchHistory = async () => {
    try {
      const value = await AsyncStorage.getItem(`@searchHistory:${token}`);
      if (value !== null) {
        setSearchHistory(JSON.parse(value));
      }
    } catch (error) {
      console.error("Error al cargar el historial de búsquedas:", error);
    }
  };

  const saveSearchHistory = async () => {
    try {
      const newSearch = {
        id: uuid.v1(),
        text: searchText,
        date: new Date().toISOString(),
      };
      const newSearchHistory = [...searchHistory, newSearch];
      await AsyncStorage.setItem(
        `@searchHistory:${token}`,
        JSON.stringify(newSearchHistory)
      );
      setSearchHistory(newSearchHistory);
      setSearchText("");
    } catch (error) {
      console.error("Error al guardar el historial de búsquedas:", error);
    }
  };

  const deleteSearchHistoryItem = async (id) => {
    try {
      const searchHistoryString = await AsyncStorage.getItem(
        `@searchHistory:${token}`
      );
      if (searchHistoryString) {
        const searchHistorys = JSON.parse(searchHistoryString);
        const index = searchHistorys.findIndex((item) => item.id === id);
        if (index !== -1) {
          searchHistory.splice(index, 1);
          await AsyncStorage.setItem(
            `@searchHistory:${token}`,
            JSON.stringify(searchHistory)
          );
          setSearchHistory(searchHistory);
          loadSearchHistory();
        }
      }
    } catch (error) {
      console.error(
        "Error al eliminar elemento del historial de búsquedas:",
        error
      );
    }
  };

  return (
    <View
      style={{
        ...styles.containerContent,
        paddingHorizontal: isFocused ? 0 : 14,
        backgroundColor: "#06C167",
      }}
    >
      <View>
        {isFocused ? (
          <TouchableOpacity
            onPress={handleButtonPress}
            style={styles.iconArrow}
          >
            <AntDesign name="arrowleft" size={25} color="rgba(33,33,33,0.7)" />
          </TouchableOpacity>
        ) : (
          <Feather name="search" size={20} style={styles.iconSearch} />
        )}

        <TextInput
          style={{
            ...styles.inputSearch,
            width: isFocused ? wp(100) : wp(80),
            height: isFocused ? hp(7) : hp(4.5),
            borderRadius: isFocused ? 0 : 25,
            backgroundColor: "#FFF",
            zIndex: 6000,
            elevation: isFocused ? 15 : 0,
          }}
          placeholder="Buscar en DigiAgro"
          onFocus={() => setIsFocused(true)}
          onBlur={() => setIsFocused(false)}
          ref={textInputRef}
          value={searchText}
          onChangeText={setSearchText}
          onSubmitEditing={() => {
            saveSearchHistory(searchText);
          }}
        />
      </View>
      {isFocused && (
        <View style={styles.containerIsFocused}>
          {searchHistory.map((items, index) => {
            return (
              <TouchableOpacity
                onLongPress={() => {
                  deleteSearchHistoryItem(items.id);
                }}
                key={index}
                style={{
                  paddingHorizontal: 20,
                  paddingTop: 20,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <FontAwesome5
                    name="history"
                    size={20}
                    color="rgba(33,33,33,0.5)"
                    style={{ marginRight: 15 }}
                  />
                  <Text style={{ fontFamily: "Poppins-medium", color: "" }}>
                    {items.text}
                  </Text>
                </View>
                <Feather
                  name="arrow-up-right"
                  size={24}
                  color="rgba(33,33,33,0.5)"
                />
              </TouchableOpacity>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default UseSearchBar;

const styles = StyleSheet.create({
  containerContent: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: hp(7),
    width: wp(90),
  },
  containerIsFocused: {
    height: hp(93),
    width: wp(100),
    position: "absolute",
    top: hp(7),
    backgroundColor: "#fff",
    zIndex: 5555,
  },
  inputSearch: {
    height: 40,
    paddingLeft: 50,
    backgroundColor: "#F4F4F4",
    fontFamily: "Poppins",
    paddingTop: 5,
  },
  iconSearch: {
    color: "rgba(33,33,33,0.5)",
    position: "absolute",
    zIndex: 9999,
    left: wp(4),
    paddingVertical: 8,
  },
  iconArrow: {
    color: "rgba(33,33,33,0.5)",
    position: "absolute",
    zIndex: 9999,
    top: hp(2),
    left: wp(4),
  },
  containerBadge: {
    backgroundColor: "#FF0000",
    height: hp(2),
    width: wp(4),
    borderRadius: 100,
    position: "absolute",
    right: -4,
    zIndex: 9999,
    alignItems: "center",
    justifyContent: "center",
  },
});
