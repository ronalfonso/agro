import React from "react";

const useMessage = (Toast) => {
  const showMessage = (message) => {
    Toast.show(message, {
      type: "custom",
      placement: "bottom",
      duration: 2000,
      offset: 30,
      animationType: "zoom-in",
      style: { width: "90%" },
    });
  };

  const successMessage = (message) => {
    Toast.show(message, {
      type: "success",
      placement: "bottom",
      duration: 3000,
      offset: 30,
      animationType: "zoom-in",
      style: { width: "90%" },
    });
  };

  const dangerMessage = (message) => {
    Toast.show(message, {
      type: "danger",
      placement: "top",
      duration: 3000,
      offset: 30,
      animationType: "zoom-in",
      style: { width: "90%" },
    });
  };

  return { showMessage, successMessage, dangerMessage };
};

export default useMessage;
