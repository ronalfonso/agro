import React, { useState } from "react";
import { useDispatch } from "react-redux";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { loginOTP } from "../store/actions/auth";

export const TOO_MANY_ERROR =
  "Ocurrio un error al enviar el código. Intentelo en un minuto";
export const OTHER_ERROR =
  "Ocurrió un error, Espere un minuto antes de volver a intentarlo.";
export const NETWORK_ERROR =
  "No se pudo enviar el código SMS. Verifique su conexión a Internet";
export const NUMBER_INVALID_ERROR = "El número de teléfono no es válido";

export const useSendCodeOtp = (
  options,
  numberPhone,
  token,
  recaptchaVerifier,
  dangerMessage,
  props,
  nameOption
) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const sendCodeMessage = () => {
    setIsLoading(true);
    switch (nameOption) {
      case "SMS":
        sendCodeSMS();
        break;
      case "WhatsApp":
        sendCodeWhatsApp();
        break;
      default:
        return;
    }
  };

  const sendCodeSMS = async () => {
    const phoneProvider = new firebase.auth.PhoneAuthProvider();
    try {
      const verificationID = await phoneProvider.verifyPhoneNumber(
        `+${numberPhone}`,
        recaptchaVerifier.current
      );
      props.navigation.navigate("ValidCode", {
        nameOption: nameOption,
        token: token,
        verificationID: verificationID,
        options: options,
        codeReceived: false,
      });
    } catch (error) {
      switch (error.code) {
        case "auth/too-many-requests":
          dangerMessage(TOO_MANY_ERROR);
          break;
        case "auth/invalid-phone-number":
          dangerMessage(NUMBER_INVALID_ERROR);
          break;
        case "auth/network-request-failed":
          dangerMessage(NETWORK_ERROR);
          break;
        default:
          dangerMessage(OTHER_ERROR);
          break;
      }
    }
    setIsLoading(false);
  };

  const sendCodeWhatsApp = () => {
    dispatch(loginOTP(numberPhone)).then((data) => {
      if (data.status === "Success") {
        props.navigation.navigate("ValidCode", {
          nameOption: nameOption,
          token: token,
          verificationID: false,
          options: options,
          codeReceived: data.data.otp,
        });
      } else {
        dangerMessage(data.message);
      }
      setIsLoading(false);
    });
  };

  return { sendCodeMessage, isLoading };
};
