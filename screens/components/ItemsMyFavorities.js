import React from "react";
import { Image, Text, View, TouchableOpacity } from "react-native";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useDispatch, useSelector } from "react-redux";
import {
  ALL_PRODUCTS_FAVORITIES,
  deleteFavorities,
} from "../../store/actions/favorities";
import * as Progress from "react-native-progress";
import { FavoritiesStyles } from "../styles/FavoritiesStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const ItemsMyFavorities = ({
  description,
  price,
  favorite_id,
  image,
  setIsLoadingIconID,
  isLoadingIconID,
  showMessage,
}) => {
  const dispatch = useDispatch();
  const allProduct = useSelector(
    (state) => state.favorities.allProductFavorities
  );

  const handleDeleteFav = async (id) => {
    setIsLoadingIconID(id);
    const updateData = await allProduct.filter(
      (item) => item.favorite_id !== id
    );

    setTimeout(async () => {
      await dispatch({
        type: ALL_PRODUCTS_FAVORITIES,
        allProductFavorities: updateData,
      });

      setIsLoadingIconID(null);

      showMessage("Listo eliminaste tu favorito!");
    }, 1000);

    await dispatch(deleteFavorities(favorite_id));
  };

  return (
    <>
      {isLoadingIconID === favorite_id && (
        <View
          style={{
            position: "absolute",
            zIndex: 100,
            left: wp(43),
            top: hp(5),
          }}
        >
          <Progress.CircleSnail
            color={["#000"]}
            size={60}
            strokeCap={"square"}
          />
        </View>
      )}
      {isLoadingIconID === favorite_id ? (
        <View style={{ ...FavoritiesStyles.containerProd, opacity: 0.1 }}>
          <Image source={{ uri: image }} style={FavoritiesStyles.imageProd} />
          <View>
            <Text style={FavoritiesStyles.textDesc}>{description}</Text>
            <Text style={FavoritiesStyles.textPrice}>$ {price}</Text>
          </View>
        </View>
      ) : (
        <View style={FavoritiesStyles.containerProd}>
          <Image source={{ uri: image }} style={FavoritiesStyles.imageProd} />
          <View>
            <Text style={FavoritiesStyles.textDesc}>{description}</Text>
            <Text style={FavoritiesStyles.textPrice}>$ {price}</Text>
            <TouchableOpacity
              onPress={() => {
                handleDeleteFav(favorite_id);
              }}
            >
              <Text
                style={{
                  fontFamily: "Poppins",
                  fontSize: fz(1.5),
                  color: "#5385E4",
                }}
              >
                Eliminar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </>
  );
};

export default ItemsMyFavorities;
