import React, { useState } from "react";
import { FlatList, Image, TouchableOpacity, View, Text } from "react-native";
import { productStyles } from "../styles/ListProductStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";

const ImageSwiperList = ({ imagenes, name }) => {
  return (
    <View
      style={{
        height: percentageToDPHeight(230),
        marginBottom: 20,
      }}
    >
      <FlatList
        data={imagenes}
        keyExtractor={(item) => item.id.toString()}
        renderItem={(itemData) => (
          <TouchableOpacity style={{ marginRight: 20 }}>
            <Image
              source={{ uri: itemData.item.image }}
              style={productStyles.imageList}
            />
          </TouchableOpacity>
        )}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          paddingHorizontal: 20,
        }}
      />
      <Text
        style={{
          fontFamily: "Poppins-medium",
          fontSize: percentageToDPSize(18),
          paddingHorizontal: 20,
        }}
      >
        {name}
      </Text>
    </View>
  );
};

export default ImageSwiperList;
