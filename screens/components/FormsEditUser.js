import React from "react";
import SelectList from "../../components/UI/custom-select-list";
import InputText from "../../components/UI/custom-input";
import { useSelector } from "react-redux";

const FormsEditUser = ({
  options,
  creadential,
  setCredentials,
  setErrors,
  errors,
}) => {
  const departament = useSelector((state) => state.auth.departament);
  const municipalities = useSelector((state) => state.auth.municipalities);
  const filterDepartament = departament?.data?.filter(
    (item) => item.id === creadential.department_id
  );

  const filterMunicipalite = municipalities?.data?.filter(
    (item) => item.id === creadential.municipalite_id
  );

  const renderForm = ({ label, name, type, keyboardType }) => {
    switch (type) {
      case "select":
        if (name === "department_id") {
          return (
            <SelectList
              key={name}
              label={label}
              error={errors.department_id}
              placeholder="Departamento"
              data={departament?.data?.map((item) => {
                return { key: item.id, value: item.name };
              })}
              setSelected={(val) => {
                setCredentials({ ...creadential, department_id: val });
                setErrors({ ...errors, department_id: "" });
              }}
              defaultOption={{
                key: filterDepartament?.[0]?.id,
                value: filterDepartament?.[0]?.name,
              }}
            />
          );
        } else if (name === "municipalite_id") {
          return (
            <SelectList
              key={name}
              label={label}
              error={errors.municipalite_id}
              placeholder="Municipio"
              data={municipalities?.data?.map((item) => {
                return { key: item.id, value: item.name };
              })}
              setSelected={(val) => {
                setCredentials({ ...creadential, municipalite_id: val });
                setErrors({ ...errors, municipalite_id: "" });
              }}
              defaultOption={{
                key: filterMunicipalite?.[0]?.id,
                value: filterMunicipalite?.[0]?.name,
              }}
            />
          );
        } else {
          return null;
        }
      case "password":
        return (
          <InputText
            key={name}
            name={name}
            value={creadential[name]}
            setCredentials={setCredentials}
            credentials={creadential}
            label={label}
            error={errors[name]}
            setErrors={setErrors}
            errorsItem={errors}
            password={true}
            keyboardType={keyboardType}
          />
        );
      default:
        return (
          <InputText
            key={name}
            name={name}
            value={creadential[name]}
            setCredentials={setCredentials}
            credentials={creadential}
            label={label}
            error={errors[name]}
            setErrors={setErrors}
            errorsItem={errors}
            password={false}
            keyboardType={keyboardType}
          />
        );
    }
  };

  return <>{options.map(renderForm)}</>;
};

export default FormsEditUser;
