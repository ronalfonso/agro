import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const NoConnection = ({ functionLoading }) => {
  return (
    <View
      style={{
        backgroundColor: "#F4F4F4",
        justifyContent: "center",
        alignItems: "center",
        height: hp(100),
        paddingHorizontal: 20,
      }}
    >
      <Image
        source={require("../../assets/noConnecting.png")}
        resizeMode="cover"
        style={{ width: wp(90), height: hp(30), top: hp(-20) }}
      />
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          paddingVertical: 40,
          top: hp(-18),
        }}
      >
        <Text
          style={{
            fontFamily: "Poppins",
            fontSize: RFPercentage(2.2),
            textAlign: "center",
          }}
        >
          ¡Parece que no hay internet!
        </Text>
        <Text
          style={{
            fontFamily: "Poppins",
            fontSize: RFPercentage(1.8),
            textAlign: "center",
          }}
        >
          Revisa tu conexión para seguir navegando.
        </Text>
      </View>
      <TouchableOpacity style={{ marginTop: 30 }} onPress={functionLoading}>
        <Text
          style={{
            textAlign: "center",
            color: "#0000FF",
            fontFamily: "Poppins-medium",
            fontSize: RFPercentage(2),
            top: hp(0),
          }}
        >
          Reintentar
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default NoConnection;
