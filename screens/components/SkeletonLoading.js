import React, { useState, useEffect } from "react";
import { View, StyleSheet, Animated } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const SkeletonLoading = () => {
  const [animation] = useState(new Animated.Value(0));

  useEffect(() => {
    const startAnimation = () =>
      Animated.timing(animation, {
        toValue: 1,
        duration: 1500,
        useNativeDriver: true,
      }).start(() => {
        animation.setValue(0);
        startAnimation();
      });

    startAnimation();
  }, [animation]);

  const opacity = animation.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0.25, 0.5, 0.25],
  });

  return (
    <View style={{ width: wp(100), height: hp(100), backgroundColor: "#FFF" }}>
      <View style={styles.container}>
        <Animated.View style={[styles.box, { opacity }]}>
          <Animated.View style={[styles.lineOne, { opacity }]}></Animated.View>
          <Animated.View style={[styles.lineTwo, { opacity }]}></Animated.View>
        </Animated.View>
      </View>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-around",
          marginBottom: hp(2),
          marginTop: hp(2),
        }}
      >
        <Animated.View style={[styles.circle, { opacity }]}></Animated.View>
        <Animated.View style={[styles.circle, { opacity }]}></Animated.View>
        <Animated.View style={[styles.circle, { opacity }]}></Animated.View>
        <Animated.View style={[styles.circle, { opacity }]}></Animated.View>
      </View>

      <View style={styles.container}>
        <Animated.View style={[styles.box, { opacity }]}>
          <Animated.View style={[styles.lineOne, { opacity }]}></Animated.View>
          <Animated.View style={[styles.lineTwo, { opacity }]}></Animated.View>
        </Animated.View>
      </View>

      <View style={styles.containerSmall}>
        <Animated.View style={[styles.boxSmall, { opacity }]}>
          <Animated.View
            style={[styles.circleTwo, { opacity }]}
          ></Animated.View>
          <Animated.View
            style={[styles.lineTwoCircle, { opacity }]}
          ></Animated.View>
        </Animated.View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    width: wp(100),
    alignItems: "center",
    paddingVertical: 20,
  },
  containerSmall: {
    justifyContent: "center",
    width: wp(100),
    alignItems: "center",
    paddingVertical: 20,
    marginBottom: hp(2),
  },
  box: {
    width: wp(90),
    height: hp(25),
    borderRadius: 8,
    backgroundColor: "#C4C4C4",
    justifyContent: "center",
    alignItems: "center",
    elevation: 5,
  },
  boxSmall: {
    width: wp(90),
    height: hp(10),
    borderRadius: 8,
    flexDirection: "row",
    backgroundColor: "#C4C4C4",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 20,
    elevation: 5,
  },
  lineOne: {
    backgroundColor: "#000",
    width: wp(60),
    height: hp(3),
    borderRadius: 8,
    marginBottom: 10,
  },
  lineTwo: {
    backgroundColor: "#000",
    width: wp(40),
    height: hp(2.5),
    borderRadius: 8,
  },
  lineTwoCircle: {
    backgroundColor: "#000",
    width: wp(60),
    height: hp(2.5),
    borderRadius: 8,
  },
  circle: {
    backgroundColor: "#000",
    width: wp(13),
    height: hp(6),
    borderRadius: 100,
  },
  circleTwo: {
    backgroundColor: "#000",
    width: wp(13),
    height: hp(6),
    borderRadius: 100,
    marginRight: 20,
  },
});

export default SkeletonLoading;
