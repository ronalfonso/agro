import React, { useState } from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import Collapsible from "react-native-collapsible";
import { MaterialIcons } from "@expo/vector-icons";
import RadioButtonRN from "radio-buttons-react-native";
import { FilterStyles } from "../styles/ModalFilterStyles";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { setFilterCategori } from "../../store/actions/filters";
import { useDispatch, useSelector } from "react-redux";

const FilterCategory = ({
  handleCollapseToggle,
  activeIndexCategori,
  categoriesProduct,
  viewAllCategori,
  setViewAllCategori,
}) => {
  const dispatch = useDispatch();
  const categoriFilter = useSelector((state) => state.filters.categoriaFilter);
  const { message } = categoriesProduct;

  const categoriasData = message
    ?.slice(0, viewAllCategori ? message?.lenght : 4)
    .map((item) => {
      return {
        id: item.id,
        label: item.name,
        value: item.id,
      };
    });

  return (
    <View style={FilterStyles.containerContent}>
      <TouchableOpacity
        style={FilterStyles.containerCollapse}
        onPress={() => {
          handleCollapseToggle(0);
          setViewAllCategori(false);
        }}
      >
        <View>
          <Text style={{ fontFamily: "Poppins-medium", fontSize: fz(1.4) }}>
            Elegir categoria:
          </Text>
          {categoriFilter.id !== "" && (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              {categoriFilter.label}
            </Text>
          )}
        </View>

        {activeIndexCategori !== 0 ? (
          <MaterialIcons name="keyboard-arrow-up" size={27} color="#06C167" />
        ) : (
          <MaterialIcons name="keyboard-arrow-down" size={27} color="#06C167" />
        )}
      </TouchableOpacity>
      <Collapsible collapsed={activeIndexCategori !== 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <RadioButtonRN
            boxStyle={FilterStyles.boxStylesRadio}
            textStyle={{ fontFamily: "Poppins", fontSize: fz(1.4) }}
            activeColor="#06C167"
            circleSize={8}
            data={categoriasData}
            selectedBtn={(e) => {
              dispatch(setFilterCategori(e));
            }}
            boxActiveBgColor="transparent"
          />
          {!viewAllCategori && (
            <TouchableOpacity
              onPress={() => setViewAllCategori(true)}
              style={FilterStyles.seeAll}
            >
              <Text style={{ fontFamily: "Poppins", color: "#06C167" }}>
                Ver todo
              </Text>
              <MaterialIcons
                name="keyboard-arrow-down"
                size={27}
                color="#06C167"
              />
            </TouchableOpacity>
          )}
        </ScrollView>
      </Collapsible>
    </View>
  );
};

export default FilterCategory;
