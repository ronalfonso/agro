import React, { useState } from "react";
import {
  Text,
  View,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { FilterStyles } from "../styles/ModalFilterStyles";
import FilterCategory from "./FilterCategory";
import FilterLocation from "./FilterLocation";
import FilterOrder from "./FilterOrder";
import FilterPrice from "./FilterPrice";
import { useDispatch, useSelector } from "react-redux";
import FilterMunicipality from "./FilterMunicipality";
import {
  clearFilterCategori,
  clearFilterOrder,
  clearFilterPrice,
  clearFilterDepLocation,
  clearFilterMuniLocation,
} from "../../store/actions/filters";

const ModalFilter = ({ setModalVisible, modalVisible, isLoadingFilters }) => {
  const dispatch = useDispatch();
  const categoriesProduct = useSelector(
    (state) => state.categories.categoriesProduct
  );
  const locationDepFilter = useSelector(
    (state) => state.filters.locationDepFilter
  );
  const municipalities = useSelector((state) => state.auth.municipalities);
  const [expandedCategori, setExpandedCategori] = useState(false);
  const [activeIndexCategori, setActiveIndexCategori] = useState(null);

  const [expandedOrder, setExpandedOrder] = useState(false);
  const [activeIndexOrder, setActiveIndexOrder] = useState(null);

  const [expandedPrice, setExpandedPrice] = useState(false);
  const [activeIndexPrice, setActiveIndexPrice] = useState(null);

  const [expandedLocation, setExpandedLocation] = useState(false);
  const [activeIndexLocation, setActiveIndexLocation] = useState(null);

  const [expandedMuni, setExpandedMuni] = useState(false);
  const [activeIndexMuni, setActiveIndexMuni] = useState(null);

  const [viewAllCategori, setViewAllCategori] = useState(false);
  const [viewAllDepartement, setViewAllDepartement] = useState(false);
  const [viewAllMuni, setViewAllMuni] = useState(false);

  const handleCollapseToggle = (
    expandedState,
    setExpandedState,
    activeIndexState,
    setActiveIndexState,
    index
  ) => {
    if (activeIndexState !== null && activeIndexState !== index) {
      setExpandedState(false);
      setActiveIndexState(null);
    } else {
      setExpandedState(!expandedState);
      setActiveIndexState(activeIndexState === index ? null : index);
    }
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(false);
      }}
    >
      <TouchableOpacity
        disabled={isLoadingFilters}
        style={FilterStyles.modalBackground}
        onPress={() => {
          setModalVisible(false);
        }}
        activeOpacity={1}
      >
        <View style={FilterStyles.modalView}>
          {isLoadingFilters && (
            <View style={FilterStyles.loadingFilter}>
              <ActivityIndicator size={30} color="#06C167" />
            </View>
          )}
          <View style={FilterStyles.triangle}></View>
          <View style={FilterStyles.containerFilter}>
            <Text style={{ fontSize: fz(2), fontFamily: "Poppins-medium" }}>
              Filtrar por:
            </Text>
            <TouchableOpacity
              onPress={() => {
                dispatch(clearFilterCategori());
                dispatch(clearFilterOrder());
                dispatch(clearFilterPrice());
                dispatch(clearFilterDepLocation());
                dispatch(clearFilterMuniLocation());
              }}
            >
              <Text style={FilterStyles.textFilter}>Limpiar filtros</Text>
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <FilterCategory
              handleCollapseToggle={() =>
                handleCollapseToggle(
                  expandedCategori,
                  setExpandedCategori,
                  activeIndexCategori,
                  setActiveIndexCategori,
                  0
                )
              }
              activeIndexCategori={activeIndexCategori}
              categoriesProduct={categoriesProduct}
              viewAllCategori={viewAllCategori}
              setViewAllCategori={setViewAllCategori}
            />
            {/* <FilterOrder
              handleCollapseToggle={() =>
                handleCollapseToggle(
                  expandedOrder,
                  setExpandedOrder,
                  activeIndexOrder,
                  setActiveIndexOrder,
                  0
                )
              }
              activeIndexOrder={activeIndexOrder}
              expandedOrder={expandedOrder}
              setExpandedOrder={setExpandedOrder}
            /> */}
            {/* <FilterPrice
              handleCollapseToggle={() =>
                handleCollapseToggle(
                  expandedPrice,
                  setExpandedPrice,
                  activeIndexPrice,
                  setActiveIndexPrice,
                  0
                )
              }
              activeIndexPrice={activeIndexPrice}
              setExpandedPrice={setExpandedPrice}
              expandedPrice={expandedPrice}
            /> */}

            {/* <FilterLocation
              handleCollapseToggle={() =>
                handleCollapseToggle(
                  expandedLocation,
                  setExpandedLocation,
                  activeIndexLocation,
                  setActiveIndexLocation,
                  0
                )
              }
              activeIndexLocation={activeIndexLocation}
              viewAllDepartement={viewAllDepartement}
              setViewAllDepartement={setViewAllDepartement}
            /> */}

            {/* {locationDepFilter.id !== "" && municipalities?.data && (
              <FilterMunicipality
                handleCollapseToggle={() =>
                  handleCollapseToggle(
                    expandedMuni,
                    setExpandedMuni,
                    activeIndexMuni,
                    setActiveIndexMuni,
                    0
                  )
                }
                activeIndexMuni={activeIndexMuni}
                setViewAllMuni={setViewAllMuni}
                viewAllMuni={viewAllMuni}
              />
            )} */}
          </ScrollView>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

export default ModalFilter;
