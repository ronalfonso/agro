import React, { useState } from 'react';
import { View, Text, Image } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";


const FirstRoute = () => (
    <View style={{ flexDirection: "column", padding: 10, backgroundColor: "#F4F4F4" }}>
        {[1, 2, 3, 4, 5, 6, 7].map((index) => {
            return (
                <View key={index} style={{ flexDirection: "row", justifyContent: "space-between", alignItems: 'center', height: 50 }}>
                    <View style={{ width: 30, height: 30, backgroundColor: "#D9D9D9", borderRadius: 100 }} />
                    <Text>Product 1</Text>
                    <Text>$4.500</Text>
                    {/*<Text>(KG)</Text>*/}
                    {/*<Image source={require("../../assets/row-icon.png")} />*/}
                </View>
            )
        })}
    </View>
);

const SecondRoute = () => (
    <View style={{ flexDirection: "column", padding: 10, backgroundColor: "#F4F4F4" }}>
        {[1, 2, 3, 4].map((index) => {
            return (
                <View key={index} style={{ flexDirection: "row", justifyContent: "space-between", alignItems: 'center', height: 50 }}>
                    <View style={{ width: 30, height: 30, backgroundColor: "#D9D9D9", borderRadius: 100 }} />
                    <Text>Product 1</Text>
                    <Text>$4.500</Text>
                    <Text>(KG)</Text>
                    <Image source={require("../../assets/row-icon-red.png")} />
                </View>
            )
        })}
    </View>
);

const MetricsItems = () => {
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'First', icon: "/assets/row-icon.png" },
        // { key: 'second', title: 'Second', icon: "/assets/heart-icon.png" },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });

    const renderTabBar = (props) => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: '#06C167' }}
            style={{ backgroundColor: '#EAEAEA' }}
            labelStyle={{ display: "none" }}
            renderIcon={({ route }) => {
                return (
                    <Image source={route.title === "Second" ? require("../../assets/heart-icon.png") : require("../../assets/row-icon.png")} />
                )
            }}
        />
    );

    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            renderTabBar={renderTabBar}
        />
    );
};

export default MetricsItems;