import React from "react";
import { AntDesign } from "@expo/vector-icons";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { createProductStyles } from "../styles/CreateProductStyles";
import * as ImagePicker from "expo-image-picker";

export const RenderImage = (imagenes) => {
  return (
    <View style={createProductStyles.containerImageTwo}>
      {imagenes.map((item) => {
        return (
          <View style={createProductStyles.imageContainer} key={item.base64}>
            <Image
              source={{ uri: item.uri }}
              style={createProductStyles.image}
            />
          </View>
        );
      })}
    </View>
  );
};

const ImagesProducts = ({
  creadentials,
  setCredentials,
  setErrors,
  errors,
}) => {
  const handleImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsMultipleSelection: true,
      quality: 1,
      base64: true,
    });
    if (!result.canceled) {
      setCredentials({ ...creadentials, images: result.assets });
      setErrors({ ...errors, images: "" });
    }
  };

  return (
    <View style={createProductStyles.containerRenderImage}>
      <View style={createProductStyles.containerDirection}>
        <Text style={createProductStyles.textTitleImage}>Imagen:</Text>
        {creadentials.images.length !== 0 && (
          <TouchableOpacity
            onPress={handleImage}
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <AntDesign
              name="clouduploado"
              size={23}
              color="#5385E4"
              style={{ marginRight: 5 }}
            />
            <Text style={createProductStyles.textSubTitleImage}>
              Subir imagen
            </Text>
          </TouchableOpacity>
        )}
      </View>

      {creadentials.images.length === 0 ? (
        <View style={createProductStyles.containerImages}>
          <TouchableOpacity
            onPress={handleImage}
            style={createProductStyles.contentImage}
          >
            <AntDesign name="plus" size={50} color="#06C167" />
          </TouchableOpacity>
          <Text style={createProductStyles.textError}>{errors.images}</Text>
        </View>
      ) : (
        RenderImage(creadentials.images)
      )}
    </View>
  );
};

export default ImagesProducts;
