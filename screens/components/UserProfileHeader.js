import React from "react";
import {
  percentageToDPHeight,
  percentageToDPWidth,
  percentageToDPSize,
} from "../../utils/CalculatePD";
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const UserProfileHeader = ({
  userData,
  pickImage,
  isLoadingImage,
  deletePhoto,
}) => {
  const IsLoadingImage = () => {
    return (
      <View style={{ position: "absolute", zIndex: 55 }}>
        <ActivityIndicator color="#06C167" size={40} />
      </View>
    );
  };

  return (
    <View style={styles.containerHeaderDates}>
      <View style={styles.containerCardHeader}>
        <TouchableOpacity style={styles.containerCardLeft} onPress={pickImage}>
          {isLoadingImage && <IsLoadingImage />}
          <Image
            source={
              userData.profile_photo_path !== null
                ? {
                    uri: userData.profile_photo_path,
                  }
                : require("../../assets/user-circle-green.png")
            }
            resizeMode="cover"
            style={{
              height: percentageToDPHeight(80),
              width: percentageToDPWidth(80),
              borderRadius: 100,
            }}
          />
        </TouchableOpacity>
        <View style={styles.containerCardRight}>
          <Text
            style={{
              fontFamily: "Poppins-bold",
              fontSize: percentageToDPSize(20),
            }}
            numberOfLines={1}
          >
            {userData.name}
          </Text>
          <Text
            style={{
              fontFamily: "Poppins-medium",
              fontSize: percentageToDPSize(17),
            }}
            numberOfLines={1}
          >
            +{`${userData.code}-${userData.phone}`}
          </Text>

          <TouchableOpacity
            onPress={() => {
              if (userData.profile_photo_path !== null) {
                deletePhoto();
              }
            }}
            style={{ height: 15 }}
          >
            {userData.profile_photo_path !== null && (
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: percentageToDPSize(12),
                  color: "#4B64EC",
                }}
              >
                Eliminar foto
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default UserProfileHeader;

const styles = StyleSheet.create({
  containerHeaderDates: {
    height: hp(18),
    width: wp(100),
    backgroundColor: "#e4e4e4",
    padding: 13,
  },

  containerCardHeader: {
    borderRadius: 5,
    height: "100%",
    width: "100%",
    backgroundColor: "#FFF",
    elevation: 10,
    shadowColor: "#000",
    flexDirection: "row",
  },

  containerCardLeft: {
    height: "100%",
    width: "30%",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },

  containerCardRight: {
    height: "100%",
    width: "70%",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
});
