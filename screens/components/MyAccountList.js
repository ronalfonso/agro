import React from "react";
import { Text, TouchableOpacity, Image, View } from "react-native";
import { MyAccountStyles } from "../styles/MyAccountStyles";

const MyAccountList = ({ optionsScreen, navigate }) => {
  return (
    <>
      <View style={MyAccountStyles.containerView}>
        <Text style={MyAccountStyles.titleSeccion}>Publicaciones:</Text>
        {optionsScreen.publication.map(({ id, title, icon, path }) => {
          return (
            <TouchableOpacity
              key={id}
              style={MyAccountStyles.containerOption}
              onPress={() => {
                navigate(path);
              }}
            >
              <Image source={icon} style={MyAccountStyles.imageIcon} />
              <Text style={MyAccountStyles.nameOption}>{title}</Text>
            </TouchableOpacity>
          );
        })}
      </View>

      <View style={MyAccountStyles.containerView}>
        <Text style={MyAccountStyles.titleSeccion}>Configuración:</Text>
        {optionsScreen.configuration.map(({ id, title, icon, path }) => {
          return (
            <TouchableOpacity
              key={id}
              style={MyAccountStyles.containerOption}
              onPress={() => {
                navigate(path);
              }}
            >
              <Image source={icon} style={MyAccountStyles.imageIcon} />
              <Text style={MyAccountStyles.nameOption}>{title}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
};

export default MyAccountList;
