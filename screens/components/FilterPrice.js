import React, { useState } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from "react-native";
import { FilterStyles } from "../styles/ModalFilterStyles";
import {
  RFPercentage as fz,
  RFPercentage,
} from "react-native-responsive-fontsize";
import Collapsible from "react-native-collapsible";
import { MaterialIcons } from "@expo/vector-icons";
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { setFilterPrice } from "../../store/actions/filters";
import { AntDesign } from "@expo/vector-icons";

const FilterPrice = ({ handleCollapseToggle, activeIndexPrice }) => {
  const dispatch = useDispatch();
  const priceFilter = useSelector((state) => state.filters.priceFilter);

  const [priceObj, setPriceObj] = useState({
    maxPrice: "",
    minPrice: "",
  });

  const handleMaxPriceChange = (value) => {
    setPriceObj({
      ...priceObj,
      maxPrice: value,
    });
  };

  const handleMinPriceChange = (value) => {
    setPriceObj({
      ...priceObj,
      minPrice: value,
    });
  };

  const handleSubmit = () => {
    dispatch(setFilterPrice(priceObj));
  };

  const handleResetMaxPrice = async () => {
    await handleMaxPriceChange("");
    dispatch(setFilterPrice({ ...priceObj, maxPrice: "" }));
  };

  const handleResetMinPrice = async () => {
    await handleMinPriceChange("");
    dispatch(setFilterPrice({ ...priceObj, minPrice: "" }));
  };

  return (
    <View style={FilterStyles.containerContent}>
      <TouchableOpacity
        style={FilterStyles.containerCollapse}
        onPress={() => handleCollapseToggle(0)}
      >
        <View>
          <Text style={{ fontFamily: "Poppins-medium", fontSize: fz(1.4) }}>
            Precio:
          </Text>
          {priceFilter.maxPrice !== "" && priceFilter.minPrice === "" && (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              Hasta U$S {priceFilter.maxPrice}
            </Text>
          )}
          {priceFilter.minPrice !== "" && priceFilter.maxPrice === "" && (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              Desde U$S {priceFilter.minPrice}
            </Text>
          )}
          {priceFilter.maxPrice !== "" && priceFilter.minPrice !== "" && (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              U$S{priceFilter.minPrice} a U$S{priceFilter.maxPrice}
            </Text>
          )}
        </View>
        {activeIndexPrice !== 0 ? (
          <MaterialIcons name="keyboard-arrow-up" size={27} color="#06C167" />
        ) : (
          <MaterialIcons name="keyboard-arrow-down" size={27} color="#06C167" />
        )}
      </TouchableOpacity>
      <Collapsible collapsed={activeIndexPrice !== 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              backgroundColor: "#F4F4F4",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 20,
              paddingVertical: 10,
            }}
          >
            <View style={{ position: "relative" }}>
              <Text
                style={{
                  color: "#ccc",
                  fontFamily: "Poppins-medium",
                  fontSize: RFPercentage(1.3),
                }}
              >
                Mínimo
              </Text>
              <TextInput
                placeholder="U$S"
                placeholderTextColor="#cccccc"
                keyboardType="numeric"
                style={{
                  backgroundColor: "transparent",
                  width: widthPercentageToDP(30),
                  height: heightPercentageToDP(5),
                  border: "none",
                  borderBottomWidth: 2,
                  borderBottomColor: "#06C167",
                  fontFamily: "Poppins-medium",
                }}
                value={
                  priceFilter.minPrice !== ""
                    ? priceFilter.minPrice
                    : priceObj.minPrice
                }
                onChangeText={handleMinPriceChange}
                onSubmitEditing={() => handleSubmit()}
              />
              {priceFilter.minPrice !== "" && (
                <TouchableOpacity
                  style={{ position: "absolute", left: 95, top: 30 }}
                  onPress={handleResetMinPrice}
                >
                  <AntDesign name="close" size={18} color="#06C167" />
                </TouchableOpacity>
              )}
            </View>
            <View style={{ position: "relative" }}>
              <Text
                style={{
                  color: "#ccc",
                  fontFamily: "Poppins-medium",
                  fontSize: RFPercentage(1.3),
                }}
              >
                Máximo
              </Text>
              <TextInput
                placeholder="U$S"
                placeholderTextColor="#cccccc"
                keyboardType="numeric"
                style={{
                  backgroundColor: "transparent",
                  width: widthPercentageToDP(30),
                  height: heightPercentageToDP(5),
                  border: "none",
                  borderBottomWidth: 2,
                  borderBottomColor: "#06C167",
                  fontFamily: "Poppins-medium",
                }}
                value={
                  priceFilter.maxPrice !== ""
                    ? priceFilter.maxPrice
                    : priceObj.maxPrice
                }
                onChangeText={handleMaxPriceChange}
                onSubmitEditing={() => handleSubmit()}
              />
              {priceFilter.maxPrice !== "" && (
                <TouchableOpacity
                  style={{ position: "absolute", left: 95, top: 30 }}
                  onPress={handleResetMaxPrice}
                >
                  <AntDesign name="close" size={18} color="#06C167" />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
      </Collapsible>
    </View>
  );
};

export default FilterPrice;
