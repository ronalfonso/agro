import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import { FilterStyles } from "../styles/ModalFilterStyles";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import Collapsible from "react-native-collapsible";
import { MaterialIcons } from "@expo/vector-icons";
import RadioButtonRN from "radio-buttons-react-native";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { setFilterDepLocation } from "../../store/actions/filters";

const FilterLocation = ({
  handleCollapseToggle,
  activeIndexLocation,
  viewAllDepartement,
  setViewAllDepartement,
}) => {
  const dispatch = useDispatch();
  const departament = useSelector((state) => state.auth.departament);
  const locationDepFilter = useSelector(
    (state) => state.filters.locationDepFilter
  );

  const { data } = departament;

  const locationData = data
    ?.slice(0, viewAllDepartement ? data.lenght : 4)
    .map((item) => {
      return {
        id: item.id,
        label: item.name,
        value: item.id,
      };
    });

  return (
    <View style={FilterStyles.containerContent}>
      <TouchableOpacity
        style={FilterStyles.containerCollapse}
        onPress={() => {
          handleCollapseToggle(0);
          setViewAllDepartement(false);
        }}
      >
        <View>
          <Text style={{ fontFamily: "Poppins-medium", fontSize: fz(1.4) }}>
            Ubicación:
          </Text>
          {locationDepFilter.id !== "" && (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              {locationDepFilter.label}
            </Text>
          )}
        </View>
        {activeIndexLocation !== 0 ? (
          <MaterialIcons name="keyboard-arrow-up" size={27} color="#06C167" />
        ) : (
          <MaterialIcons name="keyboard-arrow-down" size={27} color="#06C167" />
        )}
      </TouchableOpacity>
      <Collapsible collapsed={activeIndexLocation !== 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <RadioButtonRN
            boxStyle={FilterStyles.boxStylesRadio}
            textStyle={{ fontFamily: "Poppins", fontSize: fz(1.5) }}
            activeColor="#06C167"
            circleSize={10}
            data={locationData}
            selectedBtn={(e) => {
              dispatch(setFilterDepLocation(e));
            }}
            boxActiveBgColor="transparent"
          />
          {!viewAllDepartement && (
            <TouchableOpacity
              onPress={() => setViewAllDepartement(true)}
              style={FilterStyles.seeAll}
            >
              <Text style={{ fontFamily: "Poppins", color: "#06C167" }}>
                Ver todo
              </Text>
              <MaterialIcons
                name="keyboard-arrow-down"
                size={27}
                color="#06C167"
              />
            </TouchableOpacity>
          )}
        </ScrollView>
      </Collapsible>
    </View>
  );
};

export default FilterLocation;
