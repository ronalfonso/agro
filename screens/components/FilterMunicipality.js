import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import { FilterStyles } from "../styles/ModalFilterStyles";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import Collapsible from "react-native-collapsible";
import { MaterialIcons } from "@expo/vector-icons";
import RadioButtonRN from "radio-buttons-react-native";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { setFilterMuniLocation } from "../../store/actions/filters";

const FilterMunicipality = ({
  handleCollapseToggle,
  activeIndexMuni,
  setViewAllMuni,
  viewAllMuni,
}) => {
  const dispatch = useDispatch();
  const municipalities = useSelector((state) => state.auth.municipalities);
  const locationMuniFilter = useSelector(
    (state) => state.filters.locationMuniFilter
  );
  const { data } = municipalities;

  const muniData = data?.slice(0, viewAllMuni ? data.lenght : 4).map((item) => {
    return {
      id: item.id,
      label: item.name,
      value: item.id,
    };
  });

  return (
    <View style={FilterStyles.containerContent}>
      <TouchableOpacity
        style={FilterStyles.containerCollapse}
        onPress={() => {
          handleCollapseToggle(0);
          setViewAllMuni(false);
        }}
      >
        <View>
          <Text style={{ fontFamily: "Poppins-medium", fontSize: fz(1.4) }}>
            Municipio:
          </Text>
          {locationMuniFilter.id !== "" ? (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              {locationMuniFilter.label}
            </Text>
          ) : (
            false
          )}
        </View>
        {activeIndexMuni !== 0 ? (
          <MaterialIcons name="keyboard-arrow-up" size={27} color="#06C167" />
        ) : (
          <MaterialIcons name="keyboard-arrow-down" size={27} color="#06C167" />
        )}
      </TouchableOpacity>
      <Collapsible collapsed={activeIndexMuni !== 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <RadioButtonRN
            boxStyle={FilterStyles.boxStylesRadio}
            textStyle={{ fontFamily: "Poppins", fontSize: fz(1.5) }}
            activeColor="#06C167"
            circleSize={10}
            data={muniData}
            selectedBtn={(e) => {
              dispatch(setFilterMuniLocation(e));
            }}
            boxActiveBgColor="transparent"
          />
          {!viewAllMuni && (
            <TouchableOpacity
              onPress={() => setViewAllMuni(true)}
              style={FilterStyles.seeAll}
            >
              <Text style={{ fontFamily: "Poppins", color: "#06C167" }}>
                Ver todo
              </Text>
              <MaterialIcons
                name="keyboard-arrow-down"
                size={27}
                color="#06C167"
              />
            </TouchableOpacity>
          )}
        </ScrollView>
      </Collapsible>
    </View>
  );
};

export default FilterMunicipality;
