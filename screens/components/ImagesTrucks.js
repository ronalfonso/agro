import * as ImagePicker from "expo-image-picker";
import ImagesProducts, {RenderImage} from "./ImagesProducts";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {createProductStyles} from "../styles/CreateProductStyles";
import {AntDesign} from "@expo/vector-icons";
import React, {useState} from "react";

const ImagesTrucks = ({truck, setTruck}) => {
    const [error, setError] = useState(null);
    const handleImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsMultipleSelection: true,
            quality: 1,
            base64: true,
        });

        if (!result.canceled) {
            setTruck( {...truck, image: result.assets[0]} );
            setError({ ...error, imageTruck: "" });
        }
    };


    return (
        <View style={createProductStyles.containerRenderImage}>
            <View style={createProductStyles.containerDirection}>
                <Text style={createProductStyles.textTitleImage}>Imagen:</Text>

                <TouchableOpacity
                        onPress={handleImage}
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                        }}
                    >
                        <AntDesign
                            name="clouduploado"
                            size={23}
                            color="#5385E4"
                            style={{ marginRight: 5 }}
                        />
                        <Text style={createProductStyles.textSubTitleImage}>
                            {!truck.image ? 'Subir imagen' : 'Cambiar imagen'}
                        </Text>
                    </TouchableOpacity>
                {
                    truck.image ? (
                        <View style={{
                            borderRadius: 8,
                            marginBottom: 16,
                            shadowColor: "#000000",
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.4,
                            shadowRadius: 4,
                            elevation: 5,
                        }}>
                            <Image source={{ uri: truck.image.uri }}
                                   style={{
                                       width: 100,
                                       height: 100,
                                       borderRadius: 8,
                                   }} />
                        </View>
                    ) : (
                        <Text style={{
                            color: "red",
                            marginTop: 16,
                        }}>{error}</Text>
                    )
                }
            </View>
        </View>
    );
}

export default ImagesTrucks;