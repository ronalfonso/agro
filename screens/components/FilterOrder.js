import React from "react";
import { Text, View, TouchableOpacity, ScrollView } from "react-native";
import { FilterStyles } from "../styles/ModalFilterStyles";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import Collapsible from "react-native-collapsible";
import { MaterialIcons } from "@expo/vector-icons";
import RadioButtonRN from "radio-buttons-react-native";
import { setFilterOrder } from "../../store/actions/filters";
import { useDispatch, useSelector } from "react-redux";

const FilterOrder = ({ handleCollapseToggle, activeIndexOrder }) => {
  const dispatch = useDispatch();
  const orderFilter = useSelector((state) => state.filters.orderFilter);

  const ordersData = [
    { id: 1, label: "Mayor precio", value: "Mayor precio" },
    { id: 2, label: "Menor precio", value: "Menor precio" },
  ];

  return (
    <View style={FilterStyles.containerContent}>
      <TouchableOpacity
        style={FilterStyles.containerCollapse}
        onPress={() => handleCollapseToggle(0)}
      >
        <View>
          <Text style={{ fontFamily: "Poppins-medium", fontSize: fz(1.4) }}>
            Ordernar por:
          </Text>
          {orderFilter.id !== "" ? (
            <Text
              style={{
                fontFamily: "Poppins",
                fontSize: fz(1.2),
                color: "#06C167",
              }}
            >
              {orderFilter.label}
            </Text>
          ) : (
            false
          )}
        </View>
        {activeIndexOrder !== 0 ? (
          <MaterialIcons name="keyboard-arrow-up" size={27} color="#06C167" />
        ) : (
          <MaterialIcons name="keyboard-arrow-down" size={27} color="#06C167" />
        )}
      </TouchableOpacity>
      <Collapsible collapsed={activeIndexOrder !== 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <RadioButtonRN
            boxStyle={FilterStyles.boxStylesRadio}
            textStyle={{ fontFamily: "Poppins", fontSize: fz(1.5) }}
            activeColor="#06C167"
            circleSize={10}
            data={ordersData}
            selectedBtn={(e) => {
              dispatch(setFilterOrder(e));
            }}
            boxActiveBgColor="transparent"
          />
        </ScrollView>
      </Collapsible>
    </View>
  );
};

export default FilterOrder;
