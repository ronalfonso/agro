import React from "react";
import { SimpleLineIcons } from "@expo/vector-icons";
import { TouchableOpacity, View, Image, Text } from "react-native";
import { MyAccountStyles } from "../styles/MyAccountStyles";
import {
  percentageToDPHeight,
  percentageToDPWidth,
} from "../../utils/CalculatePD";
import { optionsForEditUser } from "../../data/user-data";
import { useSelector } from "react-redux";

const UserProfileOptions = ({ options, navigate }) => {
  const userData = useSelector((state) => state.auth.userData);

  return (
    <>
      {options.map(({ id, title, label, iconLeft, iconRight, path }) => {
        return (
          <TouchableOpacity
            style={MyAccountStyles.containerOptionItem}
            key={id}
            onPress={() => {
              const isPath = "SelectOpt";
              if (path !== isPath) {
                navigate(path);
              } else {
                navigate(path, {
                  options: optionsForEditUser[id - 1],
                  token: false,
                  numberPhone: `${userData.code}${userData.phone}`,
                });
              }
            }}
          >
            <View style={MyAccountStyles.containerLeftItem}>
              <View style={MyAccountStyles.containerCircleItem}>
                <Image
                  source={iconLeft}
                  style={{
                    width: percentageToDPWidth(60),
                    height: percentageToDPHeight(60),
                  }}
                />
              </View>
            </View>
            <View style={MyAccountStyles.containerCenterItem}>
              <Text style={MyAccountStyles.textTitle}>{title}</Text>
              <Text style={MyAccountStyles.textLabel} numberOfLines={2}>
                {label}
              </Text>
            </View>
            <View style={MyAccountStyles.containerRightItem}>
              <SimpleLineIcons name={iconRight} size={20} color="#06C167" />
            </View>
          </TouchableOpacity>
        );
      })}
    </>
  );
};

export default UserProfileOptions;
