import React, { useEffect } from "react";
import {ScrollView, SectionList, StyleSheet, Text} from "react-native";
import { Image, TouchableOpacity, View, FlatList } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP, widthPercentageToDP as wp,
  widthPercentageToDP,
} from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { setFilterCategori } from "../../store/actions/filters";
import {
  getCategoriesProduct,
  setLoadingHomesCategoriesProduct,
} from "../../store/actions/categories";

const ItemsCategories = (props) => {
  const dispatch = useDispatch();
  const categories = useSelector((state) => state.categories.categoriesProduct);

  return (
    <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.container}>
      {
        categories.message !== 'Unauthenticated.' &&
        categories.message.map((item) => {
          return (
              <View
                  style={{ flexDirection: "column", alignItems: "center" }}
                  key={item.id}
              >
                <TouchableOpacity
                    style={{ alignItems: "center" }}
                    onPress={() => {
                      dispatch(
                          setFilterCategori({ id: item.id, label: item.name, value: item.id })
                      );
                      props.props.navigation.navigate("allProducts");
                    }}
                >
                  <View style={styles.itemContainer}>
                    <Image source={{ uri: item.icon }} style={styles.itemImage} />
                  </View>
                  <Text style={styles.itemName}>{item.name}</Text>
                </TouchableOpacity>
              </View>
          )
        })
      }
    </ScrollView>
  );
};

export default ItemsCategories;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: heightPercentageToDP(1),
  },
  itemContainer: {
    height: 70,
    width: 70,
    marginHorizontal: widthPercentageToDP(2),
    borderRadius: 10,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#06C167",
  },
  itemImage: {
    width: 60,
    height: 60,
  },
  itemName: {
    fontFamily: "Poppins-medium",
    fontSize: RFPercentage(1.4),
    marginTop: 5,
  },
});
