import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import { productStyles } from "../styles/ListProductStyles";
import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import useFavoriteProduct from "../../hooks/useFavoritieProduct";

export const Container = ({ children, isTouchableOpacity, handleSubmit }) => {
  if (isTouchableOpacity) {
    return (
      <TouchableOpacity
        style={productStyles.containerItem}
        onPress={handleSubmit}
      >
        {children}
      </TouchableOpacity>
    );
  } else {
    return <View style={productStyles.containerItem}>{children}</View>;
  }
};

export const RenderIconFav = ({ favoritiesID, productID }) => {
  const { addProduct, removeProduct } = useFavoriteProduct(favoritiesID);
  const isFavorite = favoritiesID.includes(productID);

  return (
    <TouchableOpacity
      style={productStyles.containerFavoriteIcon}
      onPress={() => {
        if (isFavorite) {
          removeProduct(productID);
        } else {
          addProduct(productID);
        }
      }}
    >
      {isFavorite ? (
        <Ionicons name="heart-sharp" size={25} color="#06C167" />
      ) : (
        <Ionicons name="heart-outline" size={25} color="#06C167" />
      )}
    </TouchableOpacity>
  );
};

const ListItemsProduct = ({
  isTouchableOpacity,
  image,
  name,
  price,
  description,
  category_name,
  subcategory_name,
  handleSubmit,
  id,
}) => {
  const favorities = useSelector((state) => state.products.favoritiesProduct);

  return (
    <Container
      isTouchableOpacity={isTouchableOpacity}
      handleSubmit={handleSubmit}
    >
      {!isTouchableOpacity && (
        <RenderIconFav favoritiesID={favorities} productID={id} />
      )}
      <View style={productStyles.containerLeft}>
        <Image
          style={{ width: "100%", height: "100%" }}
          source={{ uri: image }}
          resizeMode="cover"
        />
      </View>
      <View style={productStyles.containerRight}>
        <Text style={productStyles.textNameProduct} numberOfLines={2}>
          {name}
        </Text>
        <Text style={productStyles.textPriceProduct}>${price}</Text>
        <Text style={{ ...productStyles.textOtherProduct }} numberOfLines={2}>
          {description}
        </Text>
        <Text style={productStyles.textOtherProduct} numberOfLines={2}>
          {category_name}/{subcategory_name}
        </Text>
      </View>
    </Container>
  );
};

export default ListItemsProduct;
