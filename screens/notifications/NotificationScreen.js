import React, { useEffect, useState } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { FavoritiesStyles } from "../styles/FavoritiesStyles";
import { NotificationStyles } from "../styles/NotificationStyles";
import * as Progress from "react-native-progress";
import {
  changeStatusNotification,
  getListNotification,
} from "../../store/actions/notifications";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { homeStyles } from "../styles/HomeStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { Ionicons } from "@expo/vector-icons";

export const GrantedNotification = () => {
  return (
    <View style={NotificationStyles.containerGrantedNotification}>
      <View style={NotificationStyles.contentGrantedNotification}>
        <View style={NotificationStyles.contentGrantedLeft}>
          <View style={NotificationStyles.containerGrantedLeft}>
            <Ionicons name="notifications-outline" size={40} color="#FFF" />
          </View>
        </View>
        <View style={NotificationStyles.contentGrantedRight}>
          <Text style={NotificationStyles.titleGrantedRight}>
            Activa las notificaciones
          </Text>
          <Text style={NotificationStyles.textGrantedRight}>
            Te enviaremos a tu celular las novedades.
          </Text>
          <View style={{ flexDirection: "row", marginTop: hp(1.5) }}>
            <TouchableOpacity
              style={{
                ...NotificationStyles.customBottomGranted,
                backgroundColor: "#06C167",
              }}
            >
              <Text
                style={{
                  color: "#FFF",
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.6),
                }}
              >
                Activar
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                ...NotificationStyles.customBottomGranted,
                backgroundColor: "transparent",
                marginLeft: wp(5),
              }}
            >
              <Text
                style={{
                  color: "#06C167",
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.6),
                }}
              >
                Ahora no
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const NotificationScreen = (props) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const userData = useSelector((state) => state.auth.userData);
  const [isLoading, setIsLoading] = useState(true);
  const listNotification = useSelector(
    (state) => state.notifications.listNotification
  );

  // console.log(userData.is_notification);

  useEffect(() => {
    dispatch(getListNotification()).then((data) => {
      if (data.status === "Success") {
        setIsLoading(false);
      }
    });
  }, []);

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      dispatch(getListNotification()).then((data) => {
        if (data.status === "Success") {
          setIsLoading(false);
        }
      });
    });
    return onFocusData;
  }, [navigation]);

  const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  if (listNotification?.length === 0 && !isLoading) {
    return (
      <>
        <GrantedNotification />
        <View style={FavoritiesStyles.containerEnableBackground}>
          <Text style={FavoritiesStyles.titleNotEnable}>
            No tienes notificationes
          </Text>
          <Text style={FavoritiesStyles.textNotEnable}>
            Aun no tienes notificaciones pendientes
          </Text>
        </View>
      </>
    );
  }

  if (isLoading) {
    return (
      <View
        style={{ ...homeStyles.containerLoading, top: hp(-5), width: wp(100) }}
      >
        <Progress.CircleSnail
          color={["#06C167", "#000"]}
          size={70}
          strokeCap={"square"}
        />
      </View>
    );
  }

  return (
    <>
      {/* <GrantedNotification /> */}
      <ScrollView
        style={{ backgroundColor: "#FFF" }}
        showsVerticalScrollIndicator={false}
      >
        {listNotification.map(({ id, title, message, status, created_at }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                dispatch(changeStatusNotification(id));
                props.navigation.navigate("detail-notification", {
                  id: id,
                });
              }}
              key={id}
              style={NotificationStyles.containerContentNotification}
            >
              <View
                style={{
                  ...NotificationStyles.containerContent,
                  borderBottomWidth: 1,
                  borderBottomColor: "#ccc",
                }}
              >
                <View style={NotificationStyles.containerIcon}>
                  <Ionicons name="md-gift-outline" size={35} color="#FFF" />
                </View>
                <View style={{ width: "70%" }}>
                  <Text
                    style={NotificationStyles.spanTextTitle}
                    ellipsizeMode="tail"
                    numberOfLines={1}
                  >
                    {title}
                  </Text>
                  <Text
                    style={NotificationStyles.spanTextDate}
                    ellipsizeMode="tail"
                    numberOfLines={1}
                  >
                    {message}
                  </Text>
                  <Text
                    style={{
                      ...NotificationStyles.spanTextDate,
                      color: "#06C167",
                    }}
                  >
                    {formatDate(created_at)}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </>
  );
};

export default NotificationScreen;
