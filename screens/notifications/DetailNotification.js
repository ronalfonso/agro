import React, { useEffect, useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { seeDetailNotification } from "../../store/actions/notifications";
import { useDispatch } from "react-redux";
import * as Progress from "react-native-progress";
import { homeStyles } from "../styles/HomeStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

const DetailNotification = (props) => {
  const { id } = props.route.params;
  const dispatch = useDispatch();
  const [infoNotification, setInfoNotification] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    dispatch(seeDetailNotification(id)).then((data) => {
      if (data.status === "Success") {
        setInfoNotification(data.data);
        setIsLoading(false);
      }
    });
  }, []);

  const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  if (isLoading) {
    return (
      <View
        style={{ ...homeStyles.containerLoading, top: hp(-5), width: wp(100) }}
      >
        <Progress.CircleSnail
          color={["#06C167", "#000"]}
          size={70}
          strokeCap={"square"}
        />
      </View>
    );
  }

  return (
    <ScrollView style={styles.containerNotification}>
      <Text style={styles.titleNotification}>{infoNotification.title}</Text>
      <Text style={styles.typeNotification}>
        Notificacion de {infoNotification.message_type}
      </Text>
      <Text style={styles.dateNotification}>
        Fecha: {formatDate(infoNotification.created_at)}
      </Text>
      <Text style={styles.messageNotification}>{infoNotification.message}</Text>
    </ScrollView>
  );
};

export default DetailNotification;

const styles = StyleSheet.create({
  containerNotification: {
    width: wp(100),
    height: hp(100),
    padding: 20,
  },
  titleNotification: {
    textAlign: "center",
    fontFamily: "Poppins-bold",
    fontSize: fz(2.2),
  },

  typeNotification: {
    textAlign: "center",
    fontFamily: "Poppins-medium",
    fontSize: fz(2.2),
  },

  dateNotification: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: fz(1.8),
  },
  messageNotification: {
    marginTop: hp(2),
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: fz(1.8),
  },
});
