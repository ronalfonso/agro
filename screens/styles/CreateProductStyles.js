import { StyleSheet } from "react-native";
import {
  percentageToDPSize,
  percentageToDPWidth,
  percentageToDPHeight,
} from "../../utils/CalculatePD";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export const createProductStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },

  containerHeader: {
    height: percentageToDPHeight(80),
    width: wp(100),
    alignItems: "center",
    justifyContent: "center",
    borderBottomColor: "rgba(33,33,33,0.5)",
    borderBottomWidth: 1,
  },

  textTitle: {
    color: "#212121",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(18),
    textAlign: "center",
    marginVertical: percentageToDPHeight(20),
  },

  containerCheck: {
    flexDirection: "row",
    width: wp(90),
    alignItems: "center",
    paddingTop: 30,
  },

  labelCheck: {
    width: wp(70),
    fontFamily: "Poppins-medium",
    color: "#000",
    fontSize: percentageToDPSize(16),
  },

  containerRenderImage: {
    paddingBottom: 40,
    width: wp(90),
  },

  containerDirection: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  textTitleImage: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins",
    color: "#212121",
  },

  textSubTitleImage: {
    fontSize: percentageToDPSize(13),
    fontFamily: "Poppins",
    color: "#5385E4",
  },

  contentImage: {
    width: wp(90),
    height: percentageToDPHeight(168),
    borderWidth: 1,
    borderColor: "#06C167",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },

  imageContainer: {
    marginHorizontal: 8,
    width: percentageToDPWidth(168),
    height: percentageToDPHeight(168),
    borderRadius: 10,
    overflow: "hidden",
    marginTop: 30,
  },

  containerImages: {
    width: wp(90),
    height: percentageToDPHeight(190),
  },

  image: {
    flex: 1,
    resizeMode: "cover",
  },

  containerImageTwo: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
  },

  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },
});
