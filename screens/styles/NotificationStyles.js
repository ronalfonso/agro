import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const NotificationStyles = StyleSheet.create({
  containerContentNotification: {
    flexDirection: "row",
    alignItems: "center",
    width: wp(100),
    height: hp(13),
  },

  containerContent: {
    alignItems: "center",
    flexDirection: "row",
    width: wp(100),
    height: hp(13),
    padding: 20,
    backgroundColor: "#FFF",
  },

  containerIcon: {
    marginRight: wp(5),
    backgroundColor: "#06C167",
    width: wp(15),
    alignItems: "center",
    justifyContent: "center",
    height: hp(7),
    borderRadius: 100,
  },

  spanTextTitle: {
    fontWeight: "500",
    fontFamily: "Poppins",
    fontSize: fz(2),
  },
  spanTextDate: {
    fontSize: fz(1.6),
    fontFamily: "Poppins",
  },

  containerGrantedNotification: {
    height: hp(17),
    width: wp(100),
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    backgroundColor: "#F4F4F4",
  },

  contentGrantedNotification: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: "100%",
  },

  contentGrantedLeft: {
    width: "30%",
    height: "100%",
    alignItems: "center",
    padding: 10,
  },

  containerGrantedLeft: {
    height: hp(7),
    width: wp(14),
    borderRadius: 100,
    backgroundColor: "#06C167",
    justifyContent: "center",
    alignItems: "center",
  },

  contentGrantedRight: {
    width: "70%",
    height: "100%",
    padding: 10,
  },

  titleGrantedRight: {
    color: "#000",
    fontFamily: "Poppins-bold",
    fontSize: fz(1.8),
  },

  textGrantedRight: {
    color: "#686868",
    fontFamily: "Poppins",
    fontSize: fz(1.5),
  },

  customBottomGranted: {
    height: hp(4),
    width: wp(20),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
