import { StyleSheet } from "react-native";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export const oneTimeCodeStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },

  titleVerification: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
    color: "#000",
  },

  textVerification: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
    color: "#686868",
  },

  textSpan: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins-medium",
    alignSelf: "flex-start",
    color: "#000",
    marginTop: 20,
    marginLeft: 20,
    marginBottom: percentageToDPWidth(10),
  },

  containerOption: {
    flexDirection: "row",
    width: wp(90),
    height: hp(12),
    backgroundColor: "#F4F4F4",
    borderRadius: 10,
    marginBottom: 20,
  },

  containerOptionLeft: {
    width: "25%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  containerOptionRight: {
    width: "75%",
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },

  containerIcons: {
    backgroundColor: "#06C167",
    borderRadius: 100,
    width: wp(15),
    height: hp(7),
    alignItems: "center",
    justifyContent: "center",
  },

  textName: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins-medium",
    color: "#000",
  },

  containerText: {
    width: percentageToDPWidth(300),
    height: percentageToDPHeight(60),
  },

  textCode: {
    textAlign: "center",
    fontFamily: "Poppins",
    lineHeight: 19,
    fontSize: percentageToDPSize(16),
  },

  containerInputCode: {
    position: "relative",
    width: wp(90),
    height: percentageToDPHeight(115),
    marginTop: percentageToDPHeight(50),
    marginBottom: percentageToDPHeight(30),
  },
});
