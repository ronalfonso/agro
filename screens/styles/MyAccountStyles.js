import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import {
  percentageToDPHeight,
  percentageToDPWidth,
  percentageToDPSize,
} from "../../utils/CalculatePD";

export const MyAccountStyles = StyleSheet.create({
  scrollStyle: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },

  containerHeader: {
    height: percentageToDPHeight(100),
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33, 33, 33, 0.25)",
    justifyContent: "center",
  },

  containerView: {
    paddingVertical: 20,
    width: wp(100),
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33, 33, 33, 0.25)",
    alignSelf: "flex-start",
  },

  titleSeccion: {
    fontFamily: "Poppins-medium",
    paddingHorizontal: 20,
    color: "#21212180",
    fontSize: percentageToDPSize(17),
  },

  containerOption: {
    width: wp(100),
    paddingHorizontal: 20,
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 5,
  },

  nameOption: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(16),
    color: "#212121",
    marginLeft: 10,
  },

  imageIcon: {
    width: percentageToDPWidth(40),
    height: percentageToDPHeight(40),
    resizeMode: "cover",
  },

  containerFooter: {
    paddingHorizontal: 20,
    height: percentageToDPHeight(80),
    width: wp(100),
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33, 33, 33, 0.25)",
  },

  containerIconFooter: {
    backgroundColor: "#06C167",
    height: hp(3.5),
    width: wp(8),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
  },

  footerName: {
    fontFamily: "Poppins",
    color: "#21212180",
    fontSize: fz(2),
    marginLeft: 10,
  },

  containerLogoutLeft: {
    alignItems: "center",
    justifyContent: "center",
    width: "10%",
  },

  containerLogoutRight: {
    width: "90%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },

  containerOptionItem: {
    paddingHorizontal: 20,
    flexDirection: "row",
    height: hp(10),
    alignItems: "center",
    width: "100%",
  },

  containerLeftItem: {
    width: "20%",
    height: "100%",
    justifyContent: "center",
  },

  containerCenterItem: {
    width: "75%",
    height: "100%",
    justifyContent: "center",
  },

  containerRightItem: {
    width: "5%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },

  containerCircleItem: {
    width: percentageToDPWidth(50),
    height: percentageToDPHeight(50),
    backgroundColor: "#FFF",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#ccc",
  },

  textLabel: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
    color: "#21212180",
  },

  textTitle: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(18),
  },
});
