import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const PaginationStyles = StyleSheet.create({
  containerPagination: {
    backgroundColor: "#F4F4F4",
    marginTop: 30,
    width: wp(90),
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
    height: hp(7),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  nexAndPrevPagination: {
    height: hp(4.5),
    width: wp(10),
    borderRadius: 1000,
    justifyContent: "center",
    alignItems: "center",
  },
  nexAndPrevPage: {
    height: hp(4.5),
    width: wp(10),
    borderRadius: 1000,
    justifyContent: "center",
  },
  textNextAndPrev: {
    textAlign: "center",
    fontFamily: "Poppins-medium",
    color: "#fff",
  },
  currentContainer: {
    height: hp(5),
    width: wp(11),
    backgroundColor: "#06C167",
    borderRadius: 1000,
    justifyContent: "center",
  },
  textCurrentPage: {
    textAlign: "center",
    fontFamily: "Poppins-bold",
    color: "#fff",
  },
});
