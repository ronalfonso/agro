import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";

export const storeStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },
  containerHeader: {
    marginTop: 20,
    height: percentageToDPHeight(80),
    width: wp(90),
    flexDirection: "row",
  },
  containerImage: {
    justifyContent: "center",
    alignItems: "center",
    width: percentageToDPWidth(89),
  },
  containerName: {
    width: percentageToDPWidth(280),
    justifyContent: "center",
  },
  imageBackground: {
    width: percentageToDPWidth(89),
    height: percentageToDPHeight(62),
    resizeMode: "cover",
    flexShrink: 0,
  },
  nameStore: {
    color: "rgba(0, 0, 0, 0.5)",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(20),
  },
  containerDescription: {
    width: wp(90),
    minHeight: percentageToDPHeight(100),
    marginTop: percentageToDPHeight(20),
    justifyContent: "center",
  },
  titleDescription: {
    color: "#000",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(17),
  },
  textDescription: {
    color: "#000",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(15),
  },
  directionStore: {
    color: "#686868",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(15),
  },
  containerButtons: {
    width: wp(90),
    height: percentageToDPHeight(200),
    justifyContent: "center",
    alignItems: "center",
  },
  contactBottom: {
    width: percentageToDPWidth(200),
    height: percentageToDPHeight(50),
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#06C167",
    borderRadius: 10,
  },

  textBottom: {
    color: "#06C167",
    fontFamily: "Poppins-bold",
    fontSize: percentageToDPSize(16),
  },

  containerReputation: {
    width: wp(90),
    height: percentageToDPHeight(120),
    borderBottomColor: "#00000059",
    borderBottomWidth: 1,
  },

  opinionsStore: {
    color: "#5385E4",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
  },
  containerOpitions: {
    width: wp(90),
    marginVertical: 30,
  },
  marginVertical: 20,

  container: {
    width: wp(90),
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },

  containerLeft: { width: wp(15) },

  containerRight: { width: wp(75) },

  textContainer: {
    color: "#212121",
    fontSize: percentageToDPSize(15),
    fontFamily: "Poppins",
    textAlign: "center",
    marginRight: 10,
  },

  subTitleContainer: {
    color: "#686868",
    fontSize: percentageToDPSize(13),
    fontFamily: "Poppins",
  },
});
