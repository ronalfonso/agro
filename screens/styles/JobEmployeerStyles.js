import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";

export const JobEmployeerStyles = StyleSheet.create({
  scrollStyle: {
    paddingTop: 20,
    width: wp(100),
    backgroundColor: "#FFFFFF",
  },

  container: {
    width: wp(100),
    paddingHorizontal: 20,
  },

  containerLineTop: {
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },

  textTitle: {
    fontSize: fz(2.2),
    fontFamily: "Poppins-medium",
    paddingVertical: 20,
    textAlign: "center",
  },

  containerBottoms: {
    paddingBottom: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },

  customButton: {
    borderRadius: 6,
    backgroundColor: "#06C167",
    width: wp(40),
    height: hp(5),
    justifyContent: "center",
  },

  textBottomEmployer: {
    color: "#fff",
    fontFamily: "Poppins",
    textAlign: "center",
  },

  containerLine: {
    marginVertical: hp(2),
    borderWidth: 1,
    borderColor: "#ccc",
  },

  containerCategoriItems: {
    flexDirection: "column",
    padding: 10,
    width: "100%",
    height: "100%",
  },

  containerCategoriItem: {
    height: "100%",
    width: "100%",
    position: "relative",
    flexDirection: "row",
  },

  containerItemLeft: {
    width: "20%",
    height: "100%",
    padding: 10,
    justifyContent: "center",
  },

  containerItemRight: {
    width: "80%",
    height: "100%",
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    justifyContent: "center",
  },

  circleCategori: {
    height: hp(5),
    width: wp(11),
    backgroundColor: "#D9D9D9",
    borderRadius: 100,
  },

  containerUploadDates: {
    width: wp(90),
    flexDirection: "column",
    marginBottom: 30,
  },

  bottomUploadDates: {
    backgroundColor: "#F4F4F4",
    padding: 10,
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 4,
  },

  textDatesUpload: {
    color: "#06C167",
    marginLeft: 8,
    fontSize: fz(1.5),
    fontFamily: "Poppins-medium",
  },

  containerTerms: {
    flexDirection: "row",
    paddingBottom: 20,
    justifyContent: "center",
    alignItems: "center",
  },

  textTerms: {
    width: wp(70),
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: fz(1.5),
  },

  customBottom: {
    width: wp(90),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#06C167",
    borderRadius: 10,
    marginBottom: hp(4),
    height: hp(7),
  },

  textBottom: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: fz(2),
    color: "#fff",
    fontFamily: "Poppins",
  },

  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },
});
