import { StyleSheet } from "react-native";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";

export const productStyles = StyleSheet.create({
  containerHeader: {
    height: percentageToDPHeight(45),
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    alignItems: "center",
    backgroundColor: "#FFF",
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
  },

  textResult: {
    color: "rgba(33, 33, 33, 0.5)",
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
  },

  containerFilterText: {},
});
