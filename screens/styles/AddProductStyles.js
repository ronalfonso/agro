import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const AddProductStyles = StyleSheet.create({
  containerLine: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33,33,33,0.5)",
  },
  containerLoading: {
    marginTop: hp(5),
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  container: {
    backgroundColor: "#fff",
    width: wp(100),
  },
  title: {
    textAlign: "center",
    fontSize: fz(2),
    fontFamily: "Poppins-bold",
    paddingTop: hp(5),
    color: "#212121",
    paddingBottom: hp(5),
  },
  textError: {
    color: "#FF9494",
    fontSize: fz(1.5),
    marginBottom: hp(1),
    alignSelf: "flex-start",
    fontFamily: "Poppins-bold",
    width: wp(90),
    paddingHorizontal: 20,
  },
  imageContainer: {
    margin: 8,
    width: 150,
    height: 150,
    borderRadius: 8,
    overflow: "hidden",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  containerInputs: {
    width: wp(100),
    paddingTop: hp(5),
    paddingHorizontal: 20,
  },
  borderInput: {
    borderBottomColor: "#000000",
    borderBottomWidth: 1,
  },
  input: {
    fontSize: fz(2),
    fontFamily: "Poppins-medium",
  },
  bottomRegister: {
    width: wp(90),
    backgroundColor: "#06C167",
    height: hp(7),
    borderRadius: 6,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  textRegister: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: fz(2),
    color: "#fff",
    fontFamily: "Poppins",
  },
  arrowLeft: {
    width: wp(20),
    position: "absolute",
    paddingLeft: 20,
    marginTop: 20,
    zIndex: 1,
  },
  containterContent: {
    paddingTop: 20,
    marginBottom: hp(5),
  },
  containterContent2: {
    marginBottom: hp(5),
  },
  contentCategory: {
    paddingHorizontal: 20,
  },
  textCategory: {
    textAlign: "center",
    paddingTop: 20,
    color: "rgba(104,104,104,0.5)",
    fontSize: fz(2),
    fontFamily: "Poppins",
  },
  contentImage: {
    width: wp(90),
    height: hp(20),
    borderWidth: 1,
    borderColor: "#000",
    justifyContent: "center",
    alignItems: "center",
  },
  boxStyles: {
    width: wp(90),
    height: 58,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#000",
  },
  dropdownStyles: {
    width: wp(90),
    backgroundColor: "transparent",
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#000",
  },
  inputStyles: {
    marginTop: 3,
    fontWeight: "bold",
    fontSize: fz(2),
  },
  dropdownTextStyles: {
    fontSize: fz(1.8),
    fontFamily: "Poppins-bold",
    borderBottomWidth: 1,
    borderBottomColor: "#000",
    color: "#000",
  },
  containerCheck: {
    flexDirection: "row",
    width: wp(100),
    paddingTop: hp(5),
    paddingHorizontal: 20,
    alignItems: "center",
  },
  containerText: {
    width: wp(70),
    fontFamily: "Poppins",
    color: "#000",
    fontSize: fz(2),
  },
});
