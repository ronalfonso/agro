import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const FavoritiesStyles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  containerFav: {
    width: wp(100),
  },
  containerProd: {
    paddingVertical: 30,
    flexDirection: "row",
    width: wp(100),
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33,33,33,0.3)",
    backgroundColor: "#FFFFFF",
    paddingHorizontal: 20,
  },

  containerFilter: {
    height: hp(7),
    width: wp(100),
    zIndex: -1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#06C167",
    shadowColor: "#000000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.1,
    elevation: 5,
  },
  filter: {
    color: "#fff",
    flexDirection: "row",
    alignItems: "center",
    width: wp(20),
  },
  textFilter: {
    color: "#000000",
    fontFamily: "Poppins-medium",
  },
  textDesc: {
    fontSize: fz(1.5),
    width: wp(90),
    fontFamily: "Poppins-medium",
    lineHeight: 14,
    marginBottom: hp(1),
  },
  textPrice: {
    width: wp(45),
    fontFamily: "Poppins-medium",
    fontSize: fz(2.1),
  },
  textSend: {
    fontSize: fz(1.4),
    width: wp(60),
    fontFamily: "Poppins",
    color: "#06C167",
  },
  imageProd: {
    marginRight: wp(5),
    width: wp(30),
    height: hp(13),
    borderRadius: 5,
  },
  input: {
    height: hp(4),
    margin: 12,
    paddingLeft: wp(10),
    backgroundColor: "#F4F4F4",
    width: wp(70),
    borderRadius: 50,
  },
  containerIcon: {
    justifyContent: "center",
    height: hp(3.5),
    width: wp(8),
    backgroundColor: "#fff",
    alignItems: "center",
    borderRadius: 100,
    left: wp(17),
    zIndex: 55,
    bottom: hp(-1),
  },
  SearchIcon: {
    color: "rgba(33,33,33,0.5)",
    position: "absolute",
    zIndex: 10,
    bottom: hp(2.2),
    left: wp(6),
  },
  cleanIcon: {
    position: "absolute",
    zIndex: 10,
    bottom: hp(2.2),
    right: wp(0),
  },

  containerEnableBackground: {
    paddingHorizontal: 20,
    width: wp(100),
    alignItems: "center",
    height: hp(100),
    backgroundColor: "#F4F4F4",
    marginTop: hp(10),
  },
  textNotEnable: {
    color: "#686868",
    fontFamily: "Poppins",
    textAlign: "center",
    fontSize: fz(1.8),
  },
  titleNotEnable: {
    color: "#686868",
    fontFamily: "Poppins-medium",
    fontSize: fz(2.2),
    textAlign: "center",
  },
});
