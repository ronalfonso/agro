import { Dimensions, StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  percentageToDPSize,
  percentageToDPWidth,
  percentageToDPHeight,
} from "../../utils/CalculatePD";

const { height } = Dimensions.get("window");

export const welcomeStyles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#FFF",
    height: hp(100),
    width: wp(100),
    position: "relative",
  },

  containerContent: {
    height: hp(100),
    alignItems: "center",
    justifyContent: "center",
  },

  backgroundImage: {
    position: "absolute",
    zIndex: -1,
  },

  imageBottom: {
    bottom: 0,
    right: 0,
  },

  imageRight: {
    right: 0,
    transform: [{ translateY: height / 3 }],
  },

  textGreen: {
    color: "#06C167",
    textAlign: "center",
    fontFamily: "Poppins-medium",
    lineHeight: 18,
    fontSize: percentageToDPSize(16),
    marginTop: percentageToDPHeight(18),
  },

  imageLogo: {
    position: "relative",
    bottom: percentageToDPHeight(100),
  },

  containerTerms: {
    width: percentageToDPWidth(274),
    height: percentageToDPHeight(67),
    position: "relative",
    top: percentageToDPHeight(100),
  },

  textDark: {
    fontSize: percentageToDPSize(10),
    textAlign: "center",
    color: "#686868",
    fontFamily: "Poppins",
  },
  textOther: {
    fontSize: percentageToDPSize(10),
    textAlign: "center",
    color: "#8091E9",
    fontFamily: "Poppins",
  },
});
