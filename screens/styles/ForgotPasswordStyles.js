import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const ForgotPasswordStyles = StyleSheet.create({
  scrollStyle: {
    paddingTop: 20,
    width: wp(100),
    backgroundColor: "#fff",
  },

  textSpan: {
    fontFamily: "Poppins",
    fontSize: fz(1.5),
    marginBottom: 20,
    textAlign: "center",
  },

  textAccount: {
    fontSize: fz(2),
    fontFamily: "Poppins-medium",
    textAlign: "center",
  },

  containerOption: {
    flexDirection: "row",
    height: hp(12),
    backgroundColor: "#F4F4F4",
    borderRadius: 10,
    marginBottom: 20,
  },

  containerOptionLeft: {
    width: "25%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  containerOptionRight: {
    width: "75%",
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },

  containerIcons: {
    backgroundColor: "#06C167",
    borderRadius: 100,
    width: wp(15),
    height: hp(7),
    alignItems: "center",
    justifyContent: "center",
  },

  containerInputCell: {
    height: hp(20),
    width: "100%",
    justifyContent: "center",
  },

  cell: {
    width: 50,
    height: 50,
    color: "#000",
    fontFamily: "Poppins",
    fontSize: fz(2.5),
    borderWidth: 1,
    borderColor: "rgba(33,33,33,0.3)",
    borderRadius: 5,
    textAlign: "center",
    paddingTop: 9,
  },

  focusCell: {
    borderColor: "#06C167",
  },

  customBottom: {
    width: wp(90),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#06C167",
    borderRadius: 10,
    marginBottom: hp(2),
    height: hp(7),
  },

  textBottom: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: fz(2),
    color: "#fff",
    fontFamily: "Poppins",
  },

  containerLoading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "transparent",
    top: hp(-3),
    width: wp(100),
    height: hp(100),
  },

  textError: {
    color: "#FF9494",
    fontSize: fz(1.5),
    alignSelf: "flex-start",
    fontFamily: "Poppins-medium",
    width: wp(90),
  },

  input: {
    height: 56,
    borderWidth: 1,
    border: "1px solid #000",
    padding: 10,
    width: "100%",
    borderRadius: 6,
  },
});
