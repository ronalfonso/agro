import { StyleSheet } from "react-native";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../utils/CalculatePD";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export const productStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFF",
  },

  containerItem: {
    width: wp(100),
    height: percentageToDPHeight(180),
    borderBottomColor: "rgba(33,33,33,0.5)",
    borderBottomWidth: 1,
    flexDirection: "row",
    padding: 10,
    backgroundColor: "#FFF",
    alignItems: "center",
    position: "relative",
  },

  containerLeft: {
    width: "40%",
    height: "90%",
    backgroundColor: "#EAEAEA",
    padding: 5,
  },

  containerRight: {
    width: "60%",
    height: "100%",
    paddingHorizontal: 10,
    justifyContent: "center",
  },

  textNameProduct: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(16),
    color: "#212121",
  },

  textPriceProduct: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(16),
    color: "#212121",
  },

  textOtherProduct: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(12),
    color: "#212121",
  },

  containerHeader: {
    width: wp(100),
    height: percentageToDPHeight(80),
    borderBottomColor: "#212121",
    borderBottomWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  textHeader: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(16),
    color: "#212121",
  },

  containerBody: {
    alignItems: "center",
    marginBottom: 50,
  },

  imageCover: {
    height: percentageToDPHeight(200),
    width: wp(90),
    resizeMode: "cover",
    marginVertical: 30,
    borderRadius: 4,
  },

  imageList: {
    height: percentageToDPHeight(180),
    width: percentageToDPWidth(280),
    resizeMode: "cover",
    borderRadius: 4,
  },

  containerRatings: {
    width: wp(90),
    flexDirection: "row",
    justifyContent: "space-between",
    height: percentageToDPHeight(40),
    alignItems: "center",
  },

  textRating: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
    color: "#212121",
    marginLeft: 5,
  },

  buttonShere: {
    height: percentageToDPHeight(35),
    width: percentageToDPWidth(35),
  },

  textDesc: {
    color: "#DE8657",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
  },

  textDescription: {
    color: "#000",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
  },

  textRebate: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(16),
  },

  textStock: {
    color: "#212121",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(16),
    marginBottom: 20,
  },

  bottomTransport: {
    height: percentageToDPHeight(50),
    width: wp(90),
    borderWidth: 1,
    borderColor: "#06C067",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 30,
  },

  textTransport: {
    fontWeight: "bold",
    fontSize: percentageToDPSize(15),
    color: "#06C067",
  },

  textUserView: {
    color: "#2C2C2C",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
    marginLeft: 10,
  },

  textClient: {
    color: "#2C2C2C",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
    textDecorationLine: "underline",
    marginLeft: 10,
  },

  textQuestion: {
    color: "#2C2C2C",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(15),
    marginLeft: 10,
  },

  containerFavoriteIcon: {
    backgroundColor: "rgba(6, 193, 103, 0.1)",
    padding: 3,
    borderRadius: 100,
    position: "absolute",
    zIndex: 55,
    right: 10,
    top: 10,
  },
});
