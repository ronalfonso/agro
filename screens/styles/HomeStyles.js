import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

export const homeStyles = StyleSheet.create({
  scrollStyle: {
    paddingTop: 20,
    width: wp(100),
    backgroundColor: "#FFFFFF",
  },
  containerLoading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F4F4F4",
    width: wp(90),
    borderRadius: 10,
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: hp(5),
    position: "relative",
  },
  containerImage: {
    borderRadius: 10,
    width: wp(90),
    height: hp(25),
  },
  bottomContainerBackground: {
    padding: 20,
    justifyContent: "flex-end",
    height: hp(25),
  },
  centerContainerBackground: {
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
    height: hp(25),
  },
  titleBackgroundImage: {
    color: "#fff",
    fontFamily: "Poppins",
    fontSize: fz(2),
  },
  customButtom: {
    width: wp(50),
    padding: 10,
    borderRadius: 10,
  },
  textBackgroundImage: {
    fontFamily: "Poppins-bold",
    textAlign: "center",
    fontSize: fz(2.1),
  },
  containerIcons: {
    width: wp(22.5),
    height: hp(8),
    justifyContent: "center",
    alignItems: "center",
  },
  textIcon: {
    width: wp(22.5),
    paddingTop: hp(1),
    fontSize: fz(1.3),
    textAlign: "center",
    fontFamily: "Poppins",
  },
  offerTransportBtn: {
    width: wp(90),
    backgroundColor: "#079D55",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    height: hp(7),
  },
  offerTransportText: {
    color: "#fff",
    fontFamily: "Poppins-medium",
    fontSize: fz(2),
  },

  containerLogout: {
    width: wp(90),
    backgroundColor: "#F4F4F4",
    borderRadius: 10,
    padding: 10,
  },

  textAccoutsExperiencia: {
    fontFamily: "Poppins-medium",
    textAlign: "center",
    fontSize: fz(1.8),
    marginBottom: 10,
  },

  containerBtnAccout: {
    backgroundColor: "#06C167",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    height: hp(4),
    borderRadius: 5,
  },

  btnRegisterAccoutns: {
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    height: hp(4),
  },

  textCardBackground: {
    color: "#fff",
    fontFamily: "Poppins",
    fontSize: fz(1.7),
  },

  textCardBackgroundBold: {
    color: "#fff",
    fontFamily: "Poppins-bold",
    fontSize: fz(2.2),
  },

  textCardBackgroundMedium: {
    color: "#fff",
    fontFamily: "Poppins-medium",
    fontSize: fz(2.5),
  },
});
