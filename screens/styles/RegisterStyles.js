import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  percentageToDPSize,
  percentageToDPWidth,
  percentageToDPHeight,
} from "../../utils/CalculatePD";

export const registerStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },

  containerGlobal: {
    marginBottom: 30,
    alignItems: "center",
  },

  titleRegister: {
    color: "#212121",
    fontFamily: "Poppins-bold",
    lineHeight: 19,
    fontSize: percentageToDPSize(16),
    textAlign: "center",
    marginVertical: percentageToDPHeight(52),
  },

  containerTerms: {
    width: wp(80),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 10,
    marginVertical: percentageToDPHeight(30),
  },

  textDark: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(12),
    lineHeight: 19,
    textAlign: "center",
    color: "#686868",
  },
  textOther: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(12),
    lineHeight: 19,
    textAlign: "center",
    color: "#8091E9",
  },

  textErrorTerms: {
    position: "relative",
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
    bottom: percentageToDPHeight(15),
    textAlign: "center",
    width: percentageToDPWidth(320),
  },

  titleVerification: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
    color: "#000",
  },

  textVerification: {
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(13),
    color: "#686868",
  },

  textSpan: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins-medium",
    alignSelf: "flex-start",
    color: "#000",
    marginTop: 20,
    marginBottom: percentageToDPWidth(10),
  },

  containerOption: {
    flexDirection: "row",
    width: wp(90),
    height: hp(12),
    backgroundColor: "#F4F4F4",
    borderRadius: 10,
    marginBottom: 20,
  },

  containerOptionLeft: {
    width: "25%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  containerOptionRight: {
    width: "75%",
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },

  containerIcons: {
    backgroundColor: "#06C167",
    borderRadius: 100,
    width: wp(15),
    height: hp(7),
    alignItems: "center",
    justifyContent: "center",
  },

  textName: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins-medium",
    color: "#000",
  },

  containerText: {
    width: percentageToDPWidth(300),
    height: percentageToDPHeight(60),
  },

  textCode: {
    textAlign: "center",
    fontFamily: "Poppins",
    lineHeight: 19,
    fontSize: percentageToDPSize(16),
  },

  containerInputCode: {
    position: "relative",
    width: wp(90),
    height: percentageToDPHeight(115),
    marginTop: percentageToDPHeight(50),
    marginBottom: percentageToDPHeight(30),
  },

  inputStyles: {
    color: "#212121",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(14),
  },

  dropdownTextStyles: {
    color: "#212121",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(14),
  },

  boxStyles: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    borderWidth: 1,
    borderColor: "#000000",
    height: percentageToDPHeight(56),
    width: wp(25),
    fontSize: percentageToDPSize(14),
    zIndex: -1,
    fontFamily: "Poppins-medium",
    alignItems: "center",
  },

  dropdownStyles: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#000000",
    backgroundColor: "#FFF",
    width: percentageToDPWidth(90),
    fontSize: percentageToDPSize(14),
    zIndex: 55,
  },

  customInput: {
    width: wp(65),
    height: percentageToDPHeight(56),
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: "#000000",
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    paddingLeft: 10,
    fontSize: percentageToDPSize(14),
    fontFamily: "Poppins-medium",
  },

  textLabel: {
    fontSize: percentageToDPSize(16),
    fontFamily: "Poppins",
    color: "#212121",
  },

  textError: {
    color: "#ff0e0e",
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    marginTop: percentageToDPHeight(5),
  },

  containerIconBack: {
    width: wp(90),
    zIndex: 55,
    position: "absolute",
    top: 20,
  },
});
