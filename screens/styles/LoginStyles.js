import { StyleSheet } from "react-native";
import {
  percentageToDPSize,
  percentageToDPWidth,
  percentageToDPHeight,
} from "../../utils/CalculatePD";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Constants from "expo-constants";

export const loginStyles = StyleSheet.create({
  scrollView: {
    width: wp(100),
    height: hp(100),
    backgroundColor: "#FFFFFF",
  },

  containerContent: {
    height: hp(100) - Constants.statusBarHeight,
    alignItems: "center",
    justifyContent: "center",
  },

  containerHeader: {
    position: "relative",
    width: percentageToDPWidth(237),
    height: percentageToDPHeight(42),
    bottom: percentageToDPHeight(100),
  },

  welcomeText: {
    color: "#686868",
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(16),
    lineHeight: 19,
  },

  textGreen: {
    color: "#06C167",
    textAlign: "center",
    fontFamily: "Poppins-medium",
    lineHeight: 18,
    fontSize: percentageToDPSize(16),
    marginTop: percentageToDPHeight(18),
  },

  containerIconBack: {
    width: wp(90),
    zIndex: 55,
    position: "absolute",
    top: 20,
  },
});
