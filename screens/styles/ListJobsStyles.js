import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import {
  percentageToDPHeight,
  percentageToDPSize,
} from "../../utils/CalculatePD";

export const ListJobsStyles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp(100),
  },

  containerHeader: {
    width: wp(90),
    paddingVertical: 15,
    borderBottomColor: "rgba(33, 33, 33, 0.30)",
    borderBottomWidth: 1,
  },

  containerOrder: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },

  containerOffer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },

  textOrder: {
    color: "rgba(33, 33, 33, 0.50)",
    marginRight: 10,
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
  },

  textOffers: {
    color: "rgba(33, 33, 33, 0.50)",
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(14),
  },

  containerOffers: {
    height: percentageToDPHeight(160),
    borderBottomColor: "rgba(33, 33, 33, 0.30)",
    borderBottomWidth: 1,
    justifyContent: "center",
    alignSelf: "flex-start",
    width: wp(90),
  },

  textBlackOffers: {
    fontFamily: "Poppins-medium",
    color: "#000",
    fontSize: percentageToDPSize(14),
  },

  textOtherOffers: {
    fontFamily: "Poppins",
    color: "#000000B2",
    fontSize: percentageToDPSize(14),
  },
});
