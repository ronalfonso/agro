import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { authenticate, setDidTryAutoLogin } from "../store/actions/auth";
import { userData } from "../environment/AsyncStorage";

const StartupScreen = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const tryLogin = async () => {
      const savedToken = await AsyncStorage.getItem(userData);
      if (!savedToken) {
        dispatch(setDidTryAutoLogin());
        return;
      }

      try {
        const transformedData = JSON.parse(savedToken);
        const { data } = transformedData;
        await dispatch(authenticate(data));
      } catch (err) {
        throw new Error(err.message);
      }
    };

    tryLogin();
  }, [dispatch]);

  return false;
};

export default StartupScreen;
