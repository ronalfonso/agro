import React, { useState } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { loginStyles } from "../../styles/LoginStyles";
import InputText from "../../../components/UI/custom-input";
import LargeButton from "../../../components/UI/custom-bottom";
import { login, validatePhone } from "../../../store/actions/auth";
import { useDispatch } from "react-redux";
import { handleValidateDocument, handleValidatePassword } from "./validateUser";
import { MaterialIcons } from "@expo/vector-icons";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../../hooks/useMessage";
import ProgressLoading from "../../../components/UI/custom-progress-loading";

const LoginScreen = (props) => {
  const Toast = useToast();
  const dispatch = useDispatch();
  const { successMessage, dangerMessage } = useMessage(Toast);
  const [isLoadingForgot, setIsLoadingForgot] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [step, setStep] = useState(1);
  const [creadentials, setCredentials] = useState({
    document: "",
    password: "",
  });
  const [errors, setErrors] = useState({
    document: "",
    password: "",
  });

  const handleForgotPassword = () => {
    setIsLoadingForgot(true);
    dispatch(validatePhone(creadentials.document)).then((data) => {
      setIsLoadingForgot(false);
      if (data.status === "Success") {
        props.navigation.navigate("SelectOpt", {
          options: false,
          token: data.data.token,
          numberPhone: data.data.code + data.data.phone,
        });
      } else {
        setStep(1);
        dangerMessage("Número de identidad no registrado");
      }
    });
  };

  const handleLogin = () => {
    const { document, password } = creadentials;
    let isValid = true;

    if (step === 1) {
      isValid = handleValidateDocument(
        document,
        setErrors,
        errors,
        setIsLoading
      );
    } else if (step === 2) {
      isValid = handleValidatePassword(
        password,
        setErrors,
        errors,
        setIsLoading
      );
    }

    if (isValid && step === 1) {
      setStep((prev) => prev + 1);
    }

    if (isValid && step === 2) {
      setIsLoading(true);
      dispatch(login(document, password, errors, setErrors, setStep)).then(
        (data) => {
          if (data.status === "Success") {
            successMessage("¡Inicio de sesión exitoso!");
            setCredentials((prev) => ({ ...prev, document: "", password: "" }));
          }
          setIsLoading(false);
        }
      );
    }
  };

  if (isLoadingForgot) {
    return <ProgressLoading label={true} />;
  }

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={loginStyles.scrollView}
    >
      <View style={{ alignItems: "center" }}>
        {step === 1 && (
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={loginStyles.containerIconBack}
          >
            <MaterialIcons name="keyboard-arrow-left" size={30} color="#000" />
          </TouchableOpacity>
        )}
        {step === 2 && (
          <TouchableOpacity
            onPress={() => {
              setStep((prev) => prev - 1);
            }}
            style={loginStyles.containerIconBack}
          >
            <MaterialIcons name="keyboard-arrow-left" size={30} color="#000" />
          </TouchableOpacity>
        )}
        <View style={loginStyles.containerContent}>
          <View style={loginStyles.containerHeader}>
            <Text style={loginStyles.welcomeText}>
              {step === 1
                ? "Hola! Para seguir, ingresa tu documento de identidad"
                : "Hola! Para iniciar sesión, ingresa tu contraseña."}
            </Text>
          </View>
          {step === 1 ? (
            <InputText
              name="document"
              value={creadentials.document}
              setCredentials={setCredentials}
              creadentials={creadentials}
              label="Documento"
              error={errors.document}
              setErrors={setErrors}
              errorsItem={errors}
              password={false}
              keyboardType="number-pad"
            />
          ) : (
            <InputText
              name="password"
              value={creadentials.password}
              setCredentials={setCredentials}
              creadentials={creadentials}
              label="Contraseña"
              error={errors.password}
              setErrors={setErrors}
              errorsItem={errors}
              password={true}
              keyboardType="default"
            />
          )}
          <LargeButton
            isLoading={isLoading}
            handleSubmit={handleLogin}
            text={step === 1 ? "Continuar" : "Iniciar sesión"}
            disable={isLoading}
          />
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => {
              setErrors({ ...errors, document: "", password: "" });
              if (step === 1) {
                props.navigation.navigate("RegisterScreen");
              } else {
                handleForgotPassword();
              }
            }}
          >
            <Text style={loginStyles.textGreen}>
              {step === 1 ? "Crear cuenta" : "No sé mi clave"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default LoginScreen;
