export const handleValidateDocument = (
  document,
  setErrors,
  errors,
  setIsLoading
) => {
  let isValid = true;

  if (document.length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      document: "El documento de identidad requerido",
    });
  }

  return isValid;
};

export const handleValidatePassword = (
  password,
  setErrors,
  errors,
  setIsLoading
) => {
  let isValid = true;

  if (password.length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password: "La contraseña es requerida",
    });
  }

  return isValid;
};
