import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { welcomeStyles } from "../styles/WelcomeStyles";
import LargeButton from "../../components/UI/custom-bottom";

export const dataImage = [
  {
    id: 1,
    src: require("../../assets/auth/texture_top.png"),
    styles: {
      ...welcomeStyles.backgroundImage,
      ...welcomeStyles.imageTop,
    },
  },
  {
    id: 2,
    src: require("../../assets/auth/texture_bottom.png"),
    styles: {
      ...welcomeStyles.backgroundImage,
      ...welcomeStyles.imageBottom,
    },
  },
  {
    id: 3,
    src: require("../../assets/auth/texture_right.png"),
    styles: {
      ...welcomeStyles.backgroundImage,
      ...welcomeStyles.imageRight,
    },
  },
  {
    id: 4,
    src: require("../../assets/auth/logo_top.png"),
    styles: {
      ...welcomeStyles.backgroundImage,
      ...welcomeStyles.imageTop,
      margin: 20,
    },
  },
];

const WelcomeScreen = (props) => {
  const changeScreen = () => {
    props.navigation.navigate("RegisterScreen");
  };

  return (
    <ScrollView
      style={welcomeStyles.scrollView}
      showsVerticalScrollIndicator={false}
    >
      {dataImage.map(({ id, src, styles }) => {
        return <Image style={styles} source={src} key={id} />;
      })}
      <View style={welcomeStyles.containerContent}>
        <Image
          source={require("../../assets/auth/brand_complete.png")}
          style={welcomeStyles.imageLogo}
        />
        <LargeButton
          isLoading={false}
          handleSubmit={changeScreen}
          text={"Registrarme ahora"}
          disable={false}
        />
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("LoginScreen");
          }}
        >
          <Text style={welcomeStyles.textGreen}>Ya tengo cuenta</Text>
        </TouchableOpacity>

        <View style={welcomeStyles.containerTerms}>
          <Text style={welcomeStyles.textDark}>
            Al inscribirme, declaro que soy mayor de edad y acepto las{" "}
            <Text style={welcomeStyles.textOther}>Políticas de Privacidad</Text>{" "}
            y los <Text style={welcomeStyles.textOther}>Términos</Text> y{" "}
            <Text style={welcomeStyles.textOther}>
              condiciones de Digiagro.
            </Text>
          </Text>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({});

export default WelcomeScreen;
