import React, { useState } from "react";
import { View, ScrollView, Text } from "react-native";
import { oneTimeCodeStyles } from "../styles/OneTimeCodeStyles";
import LargeButton from "../../components/UI/custom-bottom";
import InputCellCode from "../../components/UI/custom-input-code";
import { validCode } from "./register/valid-inputs-two";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import useMessage from "../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";

export const createCredential = (verificationId, enterCode) => {
  return firebase.auth.PhoneAuthProvider.credential(verificationId, enterCode);
};

export const handleError = (error, setErrors, errors) => {
  switch (error.code) {
    case "auth/invalid-verification-code":
      setErrors({ ...errors, codeOPT: "El código ingresado es incorrecto" });
      break;
    case "auth/code-expired":
      setErrors({ ...errors, codeOPT: "El código ingresado ha expirado" });
      break;
    default:
      setErrors({
        ...errors,
        codeOPT:
          "Ocurrió un error, Espere un minuto antes de volver a intentarlo.",
      });
      break;
  }
};

const EnterCodeOtpScreen = (props) => {
  const { nameOption, token, verificationID, options, codeReceived } =
    props.route.params;
  const Toast = useToast();
  const { successMessage } = useMessage(Toast);
  const [isLoading, setIsLoading] = useState(false);
  const [credentials, setCredentials] = useState({
    codeOPT: "",
  });
  const [errors, setErrors] = useState({
    codeOPT: "",
  });

  const handlePage = () => {
    if (token === false) {
      props.navigation.navigate("user-edit-info", {
        options: options,
      });
    } else {
      props.navigation.navigate("ForgotPasswordScreen", {
        token: token,
      });
    }
  };

  const validateCode = async () => {
    setIsLoading(true);
    const { codeOPT } = credentials;
    const isValid = validCode(codeOPT, errors, setErrors, setIsLoading);
    if (isValid) {
      if (nameOption === "SMS") {
        const credential = createCredential(verificationID, codeOPT);
        try {
          await firebase.auth().signInWithCredential(credential);
          handlePage();
        } catch (error) {
          handleError(error, setErrors, errors);
        }
      } else if (nameOption === "WhatsApp") {
        if (codeOPT == codeReceived) {
          handlePage();
        } else {
          setErrors({
            ...errors,
            codeOPT: "El código ingresado es incorrecto",
          });
        }
      }
      setIsLoading(false);
    }
  };

  return (
    <ScrollView style={oneTimeCodeStyles.scrollView}>
      <View style={{ alignItems: "center", marginTop: 30 }}>
        <View style={oneTimeCodeStyles.containerText}>
          <Text style={oneTimeCodeStyles.textCode}>
            Ingresa el código que te enviamos vía {nameOption}
          </Text>
        </View>
        <View style={oneTimeCodeStyles.containerInputCode}>
          <InputCellCode
            label="Código de verificación"
            CELL_COUNT={6}
            name="codeOPT"
            value={credentials.codeOPT}
            setValue={setCredentials}
            credentials={credentials}
            errors={errors}
            error={errors.codeOPT}
            setErrors={setErrors}
          />
        </View>
        <LargeButton
          isLoading={isLoading}
          handleSubmit={validateCode}
          text={"Validar"}
          disable={isLoading}
        />
      </View>
    </ScrollView>
  );
};

export default EnterCodeOtpScreen;
