import { phoneNumberRegex } from "../../../environment/regex";

export const validInputsTwo = (
  department_id,
  municipalite_id,
  vereda,
  address,
  setErrors,
  errors
) => {
  let isValid = true;

  if (department_id.length === 0) {
    isValid = false;
    setErrors({
      ...errors,
      department_id: "El departamento es requerido",
    });
  }

  if (municipalite_id.length === 0) {
    isValid = false;
    setErrors({
      ...errors,
      municipalite_id: "El municipio es requerido",
    });
  }

  if (address.length === 0) {
    isValid = false;
    setErrors({ ...errors, address: "La direccion es requerida" });
  } else if (address.length !== 0 && address.length < 6) {
    isValid = false;
    setErrors({
      ...errors,
      address: "La direccion debe tener al menos 6 caracteres",
    });
  }

  if (vereda.length === 0) {
    isValid = false;
    setErrors({ ...errors, vereda: "La vereda es requerida" });
  } else if (vereda.length > 0 && vereda.length < 6) {
    setErrors({
      ...errors,
      vereda: "La vereda debe tener al menos 6 caracteres",
    });
  }

  return isValid;
};

export const validPhone = (phone, errors, setErrors, setIsLoading) => {
  let isValid = true;

  if (phone.trim().length === 0) {
    isValid = false;
    setErrors({ ...errors, phone: "El número de telefono es requerido" });
    setIsLoading(false);
  }

  if (phone.trim().length !== 0 && !phoneNumberRegex.test(phone)) {
    isValid = false;
    setErrors({ ...errors, phone: "El número de telefono no es valido" });
    setIsLoading(false);
  }

  return isValid;
};

export const validCode = (codeOPT, errors, setErrors, setIsLoading) => {
  let isValid = true;

  if (codeOPT.trim().length === 0) {
    isValid = false;
    setErrors({ ...errors, codeOPT: "El código es requerido" });
    setIsLoading(false);
  }

  if (codeOPT.trim().length !== 0 && codeOPT.trim().length < 6) {
    isValid = false;
    setErrors({ ...errors, codeOPT: "El código no es valido" });
    setIsLoading(false);
  }

  return isValid;
};
