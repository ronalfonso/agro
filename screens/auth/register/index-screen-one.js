import React from "react";
import InputTextBorder from "../../../components/UI/custom-input-border";
import {Text, View} from "react-native";
import LargeButton from "../../../components/UI/custom-bottom";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import {registerStyles} from "../../styles/RegisterStyles";
import {validInputsOne} from "./valid-inputs-one";
import {useSelector} from "react-redux";

const RegisterScreenOne = ({
                               credentials,
                               setCredentials,
                               errors,
                               setErrors,
                               setStepRegister,
                           }) => {
    const auth = useSelector((state) => state.auth.userData);
    const handleValid = () => {
        const {first_name, last_name, document, email, password, terms} =
            credentials;

        const isValid = validInputsOne(
            first_name,
            last_name,
            document,
            email,
            password,
            terms,
            setErrors,
            errors
        );

        if (isValid) {
            setStepRegister((prev) => prev + 1);
        }
    };
    return (
        <>
            <InputTextBorder
                name="first_name"
                value={credentials.first_name}
                setCredentials={setCredentials}
                creadentials={credentials}
                label="Nombre"
                error={errors.first_name}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType="default"
                footerText="Ingresa tal como figura en tu documento"
            />
            <InputTextBorder
                name="last_name"
                value={credentials.last_name}
                setCredentials={setCredentials}
                creadentials={credentials}
                label="Apellido"
                error={errors.last_name}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType="default"
                footerText="Ingresa tal como figura en tu documento"
            />
            <InputTextBorder
                name="document"
                value={credentials.document}
                setCredentials={setCredentials}
                creadentials={credentials}
                label="Número de documento"
                error={errors.document}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType="numeric"
                footerText="Ingresa tal como figura en tu documento"
            />
            <InputTextBorder
                name="email"
                value={credentials.email}
                setCredentials={setCredentials}
                creadentials={credentials}
                label="E-mail"
                error={errors.email}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType="default"
                footerText="Asegúrate de tener acceso a este e-mail"
            />
            <InputTextBorder
                name="password"
                value={credentials.password}
                setCredentials={setCredentials}
                creadentials={credentials}
                label="Clave"
                error={errors.password}
                setErrors={setErrors}
                errorsItem={errors}
                password={true}
                keyboardType="default"
                footerText="Tu clave debe tener entre 6 y 20 caracteres"
            />


            <View style={registerStyles.containerTerms}>
                <BouncyCheckbox
                    size={25}
                    fillColor="#06C167"
                    unfillColor="#FFFFFF"
                    onPress={(isChecked) => {
                        setErrors({...errors, terms: ""});
                        setCredentials({...credentials, terms: isChecked});
                    }}
                />

                <Text style={registerStyles.textDark}>
                    Acepto las{" "}
                    <Text style={registerStyles.textOther}>
                        Politicas de Privacidad y los Terminos
                    </Text>{" "}
                    y{" "}
                    <Text style={registerStyles.textOther}>condiciones de Digiagro.</Text>
                </Text>
            </View>


            <Text style={registerStyles.textErrorTerms}>{errors.terms}</Text>
            <LargeButton
                isLoading={false}
                handleSubmit={handleValid}
                text={"Continuar"}
                disable={false}
            />
        </>
    );
};

export default RegisterScreenOne;
