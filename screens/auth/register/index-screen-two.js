import React from "react";
import SelectList from "../../../components/UI/custom-select-list";
import InputTextBorder from "../../../components/UI/custom-input-border";
import LargeButton from "../../../components/UI/custom-bottom";
import { useSelector } from "react-redux";
import { validInputsTwo } from "./valid-inputs-two";

const RegisterScreenTwo = ({
  credentials,
  setCredentials,
  errors,
  setErrors,
  setStepRegister,
}) => {
  const municipalities = useSelector((state) => state.auth.municipalities);
  const departament = useSelector((state) => state.auth.departament);

  const handleValid = () => {
    const { department_id, municipalite_id, vereda, address } = credentials;

    const isValid = validInputsTwo(
      department_id,
      municipalite_id,
      vereda,
      address,
      setErrors,
      errors
    );

    if (isValid) {
      setStepRegister((prev) => prev + 1);
    }
  };

  return (
    <>
      <SelectList
        name="department_id"
        label="Departamento"
        errors={errors}
        error={errors.department_id}
        placeholder="Departamento"
        data={departament.data.map((item) => {
          return {
            key: item.id,
            value: item.name,
          };
        })}
        setSelected={(val) => {
          setCredentials({ ...credentials, department_id: val });
          setErrors({ ...errors, department_id: "" });
        }}
      />
      {Object.entries(municipalities).length !== 0 && (
        <SelectList
          label="Municipio"
          error={errors.municipalite_id}
          placeholder="Municipio"
          data={municipalities.data.map((item) => {
            return {
              key: item.id,
              value: item.name,
            };
          })}
          setSelected={(val) => {
            setCredentials({ ...credentials, municipalite_id: val });
            setErrors({ ...errors, municipalite_id: "" });
          }}
        />
      )}
      <InputTextBorder
        name="vereda"
        value={credentials.vereda}
        setCredentials={setCredentials}
        creadentials={credentials}
        label="Vereda"
        error={errors.vereda}
        setErrors={setErrors}
        errorsItem={errors}
        password={false}
        keyboardType="default"
        footerText=""
      />
      <InputTextBorder
        name="address"
        value={credentials.address}
        setCredentials={setCredentials}
        creadentials={credentials}
        label="Direccion"
        error={errors.address}
        setErrors={setErrors}
        errorsItem={errors}
        password={false}
        keyboardType="default"
        footerText=""
      />
      <LargeButton
        isLoading={false}
        handleSubmit={handleValid}
        text={"Continuar"}
        disable={false}
      />
    </>
  );
};

export default RegisterScreenTwo;
