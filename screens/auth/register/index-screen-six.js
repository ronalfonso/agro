import React, { useState } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import LargeButton from "../../../components/UI/custom-bottom";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../../utils/CalculatePD";
import { login } from "../../../store/actions/auth";
import { useDispatch } from "react-redux";
import useMessage from "../../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";

const RegisterScreenSix = ({ credentials }) => {
  const Toast = useToast();
  const { successMessage } = useMessage(Toast);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const handleLogin = () => {
    const { document, password } = credentials;
    setIsLoading(true);
    dispatch(login(document, password, false, false, false)).then((data) => {
      if (data.status === "Success") {
        successMessage("¡Inicio de sesión exitoso!");
      }
      setIsLoading(false);
    });
  };

  return (
    <>
      <Image
        source={require("../../../assets/auth/finish.png")}
        style={styles.image}
      />
      <View style={styles.containerText}>
        <Text style={styles.text}>
          ¡Felicitaciones! Ya eres parte de la revolución del campo.
        </Text>
      </View>
      <LargeButton
        isLoading={isLoading}
        handleSubmit={handleLogin}
        text={"Ir a mi cuenta"}
        disable={false}
      />
    </>
  );
};

const styles = StyleSheet.create({
  containerText: {
    width: percentageToDPWidth(270),
    marginVertical: percentageToDPHeight(50),
  },
  text: {
    fontFamily: "Poppins-medium",
    textAlign: "center",
    fontSize: percentageToDPSize(17),
    color: "#686868",
    lineHeight: 19,
  },
  image: {
    width: percentageToDPWidth(120),
    height: percentageToDPHeight(120),
  },
});

export default RegisterScreenSix;
