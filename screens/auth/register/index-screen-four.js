import React, { useRef, useState } from "react";
import { View, Text, TextInput } from "react-native";
import { registerStyles } from "../../styles/RegisterStyles";
import { SelectList } from "react-native-dropdown-select-list";
import { FontAwesome } from "@expo/vector-icons";
import LargeButton from "../../../components/UI/custom-bottom";
import { validPhone } from "./valid-inputs-two";
import { FirebaseRecaptchaVerifierModal } from "expo-firebase-recaptcha";
import { firebaseConfig } from "../../../config";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { loginOTP } from "../../../store/actions/auth";
import { useDispatch } from "react-redux";

const RegisterScreenFour = ({
  credentials,
  setCredentials,
  errors,
  setErrors,
  setStepRegister,
  sendMessage,
  setVerificationId,
  setCodeReceived,
}) => {
  const dispatch = useDispatch();
  const recaptchaVerifier = useRef(null);
  const numberPhone = `${credentials.code}${credentials.phone}`;
  const [isLoading, setIsLoading] = useState(false);

  const data = [
    {
      key: "+58",
      value: "+58",
    },
    {
      key: "+57",
      value: "+57",
    },
  ];

  const sendCodeMessage = async () => {
    setIsLoading(true);
    const { phone } = credentials;
    const isValid = validPhone(phone, errors, setErrors, setIsLoading);

    if (isValid) {
      if (sendMessage === "SMS") {
        sendCodeSMS();
      } else {
        sendCodeWhatsApp();
      }
    }
  };

  const sendCodeSMS = async () => {
    try {
      const phoneProvider = new firebase.auth.PhoneAuthProvider();
      const verificationId = await phoneProvider.verifyPhoneNumber(
        numberPhone,
        recaptchaVerifier.current
      );
      setIsLoading(false);
      setVerificationId(verificationId);
      setStepRegister((prev) => prev + 1);
    } catch (error) {
      setIsLoading(false);
      console.log(error);
      switch (error.code) {
        case "auth/too-many-requests":
          setErrors({
            ...errors,
            phone:
              "Ha ocurrido un error al enviar el código. Espere un minuto antes de volver a intentarlo",
          });
          break;
        default:
          setErrors({
            ...errors,
            phone:
              "Ocurrió un error, Espere un minuto antes de volver a intentarlo.",
          });
          break;
      }
    }
  };

  const sendCodeWhatsApp = () => {
    dispatch(loginOTP(numberPhone.slice(1))).then((data) => {
      if (data.status === "Success") {
        setCodeReceived(data.data.otp);
        setIsLoading(false);
        setStepRegister((prev) => prev + 1);
      } else {
        setIsLoading(false);
        setErrors({ ...errors, phone: data.message });
      }
    });
  };

  return (
    <>
      <FirebaseRecaptchaVerifierModal
        ref={recaptchaVerifier}
        firebaseConfig={firebaseConfig}
        attemptInvisibleVerification={true}
      />
      <View style={registerStyles.containerText}>
        <Text style={registerStyles.textCode}>
          Enviaremos un código a través de {sendMessage} al número ingresado
        </Text>
      </View>
      <View style={registerStyles.containerInputCode}>
        <Text style={registerStyles.textLabel}>Telefono:</Text>
        <View style={{ flexDirection: "row" }}>
          <SelectList
            setSelected={(val) => {
              setCredentials({ ...credentials, code: val });
            }}
            data={data}
            boxStyles={registerStyles.boxStyles}
            dropdownStyles={registerStyles.dropdownStyles}
            dropdownTextStyles={registerStyles.dropdownTextStyles}
            search={false}
            placeholder="+58"
            inputStyles={registerStyles.inputStyles}
            arrowicon={
              <FontAwesome
                name="chevron-down"
                size={12}
                color={"#212121"}
                style={{ bottom: 1 }}
              />
            }
            defaultOption={[
              {
                key: "+58",
                value: "+58",
              },
            ]}
          />
          <TextInput
            style={registerStyles.customInput}
            keyboardType="number-pad"
            value={credentials.phone}
            onChangeText={(text) => {
              setCredentials({ ...credentials, phone: text });
            }}
            onFocus={() => {
              setErrors({ ...errors, phone: "" });
            }}
          />
        </View>
        <Text style={registerStyles.textError} numberOfLines={1}>
          {errors.phone}
        </Text>
      </View>
      <LargeButton
        isLoading={isLoading}
        handleSubmit={sendCodeMessage}
        text={"Continuar"}
        disable={isLoading}
      />
    </>
  );
};

export default RegisterScreenFour;
