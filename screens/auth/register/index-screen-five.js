import React, { useState } from "react";
import { Text, View } from "react-native";
import InputCellCode from "../../../components/UI/custom-input-code";
import { registerStyles } from "../../styles/RegisterStyles";
import LargeButton from "../../../components/UI/custom-bottom";
import { validCode } from "./valid-inputs-two";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";

export const createCredential = (verificationId, enterCode) => {
  return firebase.auth.PhoneAuthProvider.credential(verificationId, enterCode);
};

export const handleError = (error, setErrors, errors) => {
  switch (error.code) {
    case "auth/invalid-verification-code":
      setErrors({ ...errors, codeOPT: "El código ingresado es incorrecto" });
      break;
    case "auth/code-expired":
      setErrors({ ...errors, codeOPT: "El código ingresado ha expirado" });
      break;
    default:
      setErrors({
        ...errors,
        codeOPT:
          "Ocurrió un error, Espere un minuto antes de volver a intentarlo.",
      });
      break;
  }
};

const RegisterScreenFive = ({
  credentials,
  setCredentials,
  errors,
  setErrors,
  sendMessage,
  verificationId,
  codeReceived,
  handleRegister,
}) => {
  const [isLoading, setIsLoading] = useState(false);

  const validateCode = async () => {
    setIsLoading(true);
    const { codeOPT } = credentials;
    const isValid = validCode(codeOPT, errors, setErrors, setIsLoading);

    if (isValid) {
      if (sendMessage === "SMS") {
        const credential = createCredential(verificationId, codeOPT);
        try {
          await firebase.auth().signInWithCredential(credential);
          handleRegister(setIsLoading);
        } catch (error) {
          setIsLoading(false);
          handleError(error, setErrors, errors);
        }
      } else if (sendMessage === "WhatsApp") {
        if (codeOPT == codeReceived) {
          handleRegister(setIsLoading);
        } else {
          setIsLoading(false);
          setErrors({
            ...errors,
            codeOPT: "El código ingresado es incorrecto",
          });
        }
      }
    }
  };

  return (
    <>
      <View style={registerStyles.containerText}>
        <Text style={registerStyles.textCode}>
          Ingresa el código que te enviamos vía {sendMessage}
        </Text>
      </View>
      <View style={registerStyles.containerInputCode}>
        <InputCellCode
          label="Código de verificación"
          CELL_COUNT={6}
          name="codeOPT"
          value={credentials.codeOPT}
          setValue={setCredentials}
          credentials={credentials}
          errors={errors}
          error={errors.codeOPT}
          setErrors={setErrors}
        />
      </View>

      <LargeButton
        isLoading={isLoading}
        handleSubmit={validateCode}
        text={"Validar"}
        disable={isLoading}
      />
    </>
  );
};

export default RegisterScreenFive;
