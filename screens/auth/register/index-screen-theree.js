import React, { useState } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { registerStyles } from "../../styles/RegisterStyles";
import {
  MaterialIcons,
  MaterialCommunityIcons,
  FontAwesome,
} from "@expo/vector-icons";
import LargeButton from "../../../components/UI/custom-bottom";

const RegisterScreenThree = ({ setSendMessage, setStepRegister }) => {
  const [optionActive, setOptionActive] = useState(false);

  const options = [
    { id: 1, name: "SMS" },
    { id: 2, name: "WhatsApp" },
  ];

  const stepHandle = () => {
    setStepRegister((prev) => prev + 1);
  };

  return (
    <>
      <Text style={registerStyles.titleVerification}>
        Verifica que esta cuenta te pertenece
      </Text>
      <Text style={registerStyles.textVerification}>
        Elige como recibir el código de verificación.
      </Text>
      <Text style={registerStyles.textSpan}>Seleccione un opcion:</Text>
      <View style={{ alignItems: "center" }}>
        {options.map(({ id, name }) => {
          return (
            <TouchableOpacity
              style={{
                ...registerStyles.containerOption,
                borderWidth: optionActive === id ? 2 : 0,
                borderColor: optionActive === id ? "#06C167" : "",
              }}
              key={id}
              onPress={() => {
                setOptionActive(id);
                setSendMessage(name);
              }}
            >
              <View style={registerStyles.containerOptionLeft}>
                <View style={registerStyles.containerIcons}>
                  {name === "SMS" ? (
                    <MaterialCommunityIcons
                      name="message-text-outline"
                      size={30}
                      color="#FFF"
                    />
                  ) : (
                    <FontAwesome name="whatsapp" size={30} color="#FFF" />
                  )}
                </View>
              </View>
              <View style={registerStyles.containerOptionRight}>
                <Text style={registerStyles.textName}>{name}</Text>
                <MaterialIcons
                  name="arrow-forward-ios"
                  size={17}
                  color="rgba(33,33,33,0.3)"
                />
              </View>
            </TouchableOpacity>
          );
        })}
        {optionActive !== false && (
          <LargeButton
            isLoading={false}
            handleSubmit={stepHandle}
            text={"Continuar"}
            disable={false}
          />
        )}
      </View>
    </>
  );
};

export default RegisterScreenThree;
