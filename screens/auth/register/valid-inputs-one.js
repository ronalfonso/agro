import { emailRegex, numberDocumentRegex } from "../../../environment/regex";

export const validInputsOne = (
  first_name,
  last_name,
  document,
  email,
  password,
  terms,
  setErrors,
  errors
) => {
  let isValid = true;

  if (first_name === "") {
    setErrors({
      ...errors,
      first_name: "El nombre es requerido",
    });
    isValid = false;
  }

  if (first_name.trim().length > 0 && first_name.trim().length < 3) {
    setErrors({
      ...errors,
      first_name: "El nombre debe tener al menos 3 caracteres",
    });
    isValid = false;
  }

  if (last_name === "") {
    setErrors({
      ...errors,
      last_name: "El apellido es requerido",
    });
    isValid = false;
  }

  if (last_name.trim().length > 0 && last_name.trim().length < 3) {
    setErrors({
      ...errors,
      last_name: "El apellido debe tener al menos 3 caracteres",
    });
    isValid = false;
  }

  if (password.length > 0 && password.length < 6) {
    isValid = false;
    setErrors({
      ...errors,
      password: "La clave debe tener al menos 6 caracteres",
    });
  } else if (password.length === 0) {
    isValid = false;
    setErrors({
      ...errors,
      password: "La clave es requerida",
    });
  }

  if (email.length > 0 && !emailRegex.test(email)) {
    isValid = false;
    setErrors({ ...errors, email: "El E-mail es inválido" });
  }

  if (document.length > 0 && document.length < 6) {
    isValid = false;
    setErrors({
      ...errors,
      document: "El documento debe tener al menos 6 caracteres",
    });
  } else if (document.length === 0) {
    isValid = false;
    setErrors({ ...errors, document: "El número de documento es requerido" });
  }

  if (document.length > 0 && !numberDocumentRegex.test(document)) {
    isValid = false;
    setErrors({ ...errors, document: "El número de documento no valido" });
  }

  if (!terms) {
    isValid = false;
    setErrors({
      ...errors,
      terms: "Debe aceptar los terminos y condiciones para continuar",
    });
  }

  return isValid;
};
