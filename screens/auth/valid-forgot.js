export const ValidForgotPassword = (
  password,
  password_confirmation,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;
  if (password.trim().length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, password: "El minimo de caracteres es de 6" });
  } else if (password.trim().length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, password: "Campo requerido" });
  }

  if (password_confirmation.trim().length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_confirmation: "El minimo de caracteres es de 6",
    });
  } else if (password_confirmation.trim().length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, password_confirmation: "Campo requerido" });
  }

  if (password.trim() !== password_confirmation.trim()) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_confirmation: "Las contraseñas no coinciden",
    });
  }

  return isValid;
};
