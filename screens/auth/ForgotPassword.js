import React, { useEffect, useState } from "react";
import { Text, View, ScrollView } from "react-native";
import { ForgotPasswordStyles } from "../styles/ForgotPasswordStyles";
import { useDispatch } from "react-redux";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../hooks/useMessage";
import InputText from "../../components/UI/custom-input";
import LargeButton from "../../components/UI/custom-bottom";
import { ValidForgotPassword } from "./valid-forgot";
import { forgotPassword } from "../../store/actions/auth";

const ForgotPassword = (props) => {
  const dispatch = useDispatch();
  const Toast = useToast();
  const [isLoading, setIsLoading] = useState(false);
  const { successMessage } = useMessage(Toast);
  const { token } = props.route.params;
  const [credentials, setCredentials] = useState({
    password: "",
    password_confirmation: "",
  });
  const [errors, setErrors] = useState({
    password: "",
    password_confirmation: "",
  });

  const handleResetPassword = () => {
    setIsLoading(true);
    const { password, password_confirmation } = credentials;
    const isValid = ValidForgotPassword(
      password,
      password_confirmation,
      errors,
      setErrors,
      setIsLoading
    );

    if (isValid) {
      dispatch(forgotPassword(password, password_confirmation, token)).then(
        (data) => {
          if (data.status === "Success") {
            successMessage(data.message);
            setIsLoading(false);
            setCredentials({
              ...credentials,
              password: "",
              password_confirmation: "",
            });
            setTimeout(() => {
              props.navigation.navigate("LoginScreen");
            }, 3000);
          } else {
            setErrors({ ...errors, password: data.message });
            setIsLoading(false);
          }
        }
      );
    }
  };

  return (
    <ScrollView style={ForgotPasswordStyles.scrollStyle}>
      <Text style={ForgotPasswordStyles.textAccount}>
        Completa todos los campos
      </Text>
      <View style={{ alignItems: "center", marginTop: 50 }}>
        <InputText
          name="password"
          value={credentials.password}
          setCredentials={setCredentials}
          creadentials={credentials}
          label="Contraseña"
          error={errors.password}
          setErrors={setErrors}
          errorsItem={errors}
          password={true}
          keyboardType="default"
        />
        <InputText
          name="password_confirmation"
          value={credentials.password_confirmation}
          setCredentials={setCredentials}
          creadentials={credentials}
          label="Confirmar contraseña"
          error={errors.password_confirmation}
          setErrors={setErrors}
          errorsItem={errors}
          password={true}
          keyboardType="default"
        />
        <LargeButton
          isLoading={isLoading}
          handleSubmit={handleResetPassword}
          text={"Continuar"}
          disable={isLoading}
        />
      </View>
    </ScrollView>
  );
};

export default ForgotPassword;
