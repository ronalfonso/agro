import React, { useState, useRef, useEffect } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import LargeButton from "../../components/UI/custom-bottom";
import {
  MaterialCommunityIcons,
  FontAwesome,
  MaterialIcons,
} from "@expo/vector-icons";
import { oneTimeCodeStyles } from "../styles/OneTimeCodeStyles";
import { FirebaseRecaptchaVerifierModal } from "expo-firebase-recaptcha";
import useMessage from "../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";
import { firebaseConfig } from "../../config";
import { percentageToDPSize } from "../../utils/CalculatePD";
import { useSendCodeOtp } from "../../hooks/useSendCodeOtp";

export const optionsOTP = [
  { id: 1, name: "SMS" },
  { id: 2, name: "WhatsApp" },
];

const SelectOptionOtp = (props) => {
  const Toast = useToast();
  const { dangerMessage } = useMessage(Toast);
  const { options, numberPhone, token } = props.route.params;
  const recaptchaVerifier = useRef(null);
  const [optionActive, setOptionActive] = useState(false);
  const [nameOption, setNameOption] = useState(false);
  const { sendCodeMessage, isLoading } = useSendCodeOtp(
    options,
    numberPhone,
    token,
    recaptchaVerifier,
    dangerMessage,
    props,
    nameOption
  );

  return (
    <>
      <FirebaseRecaptchaVerifierModal
        ref={recaptchaVerifier}
        firebaseConfig={firebaseConfig}
        attemptInvisibleVerification={false}
      />
      <ScrollView style={oneTimeCodeStyles.scrollView}>
        <View style={{ alignItems: "center", marginTop: 30 }}>
          <Text style={oneTimeCodeStyles.titleVerification}>
            Verifica que esta cuenta te pertenece
          </Text>
          <Text style={oneTimeCodeStyles.textVerification}>
            Elige como recibir el código de verificación.
          </Text>
          <Text style={oneTimeCodeStyles.textSpan}>Seleccione un opcion:</Text>
          {optionsOTP.map(({ id, name }) => {
            return (
              <TouchableOpacity
                style={{
                  ...oneTimeCodeStyles.containerOption,
                  borderWidth: optionActive === id ? 2 : 0,
                  borderColor: optionActive === id ? "#06C167" : "",
                }}
                key={id}
                onPress={() => {
                  setOptionActive(id);
                  setNameOption(name);
                }}
              >
                <View style={oneTimeCodeStyles.containerOptionLeft}>
                  <View style={oneTimeCodeStyles.containerIcons}>
                    {name === "SMS" ? (
                      <MaterialCommunityIcons
                        name="message-text-outline"
                        size={30}
                        color="#FFF"
                      />
                    ) : (
                      <FontAwesome name="whatsapp" size={30} color="#FFF" />
                    )}
                  </View>
                </View>
                <View style={oneTimeCodeStyles.containerOptionRight}>
                  <View>
                    <Text style={oneTimeCodeStyles.textName}>{name}</Text>
                    <Text
                      style={{
                        color: "rgba(33,33,33,0.3)",
                        fontFamily: "Poppins",
                        fontSize: percentageToDPSize(15),
                      }}
                    >
                      Al número
                      {`***${numberPhone.slice(-4)}`}
                    </Text>
                  </View>
                  <MaterialIcons
                    name="arrow-forward-ios"
                    size={17}
                    color="rgba(33,33,33,0.3)"
                  />
                </View>
              </TouchableOpacity>
            );
          })}
          {optionActive !== false && (
            <LargeButton
              isLoading={isLoading}
              handleSubmit={sendCodeMessage}
              text={"Continuar"}
              disable={isLoading}
            />
          )}
        </View>
      </ScrollView>
    </>
  );
};

export default SelectOptionOtp;
