import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { Feather, MaterialIcons } from "@expo/vector-icons";

const OffertTransportScreen = (props) => {
  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={styles.container}>
        <View style={styles.containerSearch}>
          <Feather name="search" size={24} style={styles.iconContainer} />
          <TextInput
            style={styles.input}
            placeholder="Buscar vehículos y ofertas de cargo"
          />
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "rgba(33,33,33,0.5)",
          }}
        />
        <View style={styles.containterContent}>
          <View style={{ alignItems: "center" }}>
            <View style={styles.containerOffer}>
              <Text style={styles.textDestacados}>
                Transportadores disponibles
              </Text>

              <View style={{ marginBottom: 20 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Image
                    source={require("../../../assets/transporte/farmaAgro.png")}
                  />
                  <Image
                    source={require("../../../assets/transporte/farmaAgro.png")}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Image
                    source={require("../../../assets/transporte/farmaAgro.png")}
                  />
                  <Image
                    source={require("../../../assets/transporte/farmaAgro.png")}
                  />
                </View>
              </View>

              <TouchableOpacity>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={styles.textSell3}>Ver todo</Text>
                  <MaterialIcons
                    name="arrow-forward-ios"
                    size={20}
                    color="#5385E4"
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ alignItems: "center" }}>
            <View style={styles.containerImg}>
              <View style={{ maxWidth: wp(45) }}>
                <Image
                  source={require("../../../assets/transporte/offert2.png")}
                />
                <Text style={styles.textOffer}>
                  Cargos disponibles para hacer oferta
                </Text>
              </View>
              <View style={{ maxWidth: wp(45) }}>
                <Image
                  source={require("../../../assets/transporte/offert1.png")}
                />
                <Text style={styles.textOffer}>hacer ofertas de cargas</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "rgba(33,33,33,0.5)",
              marginTop: 20,
            }}
          />
          <View style={{ alignItems: "center" }}>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate("Transporter");
              }}
            >
              <View style={styles.customButton}>
                <Text
                  style={{
                    fontFamily: "Poppins-bold",
                    color: "#fff",
                    fontSize: fz(2),
                  }}
                >
                  Registrarme con transportador
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default OffertTransportScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp(100),
    marginTop: 20,
  },
  containerSearch: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  input: {
    height: 50,
    paddingLeft: wp(10),
    backgroundColor: "#F4F4F4",
    width: wp(90),
    fontFamily: "Poppins",
    borderRadius: 50,
  },
  containterContent: {
    paddingTop: 20,
  },
  containerOffer: {
    backgroundColor: "#F4F4F4",
    width: wp(90),
    borderRadius: 6,
    padding: 20,
    marginBottom: 20,
  },
  containerImg: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: wp(80),
    paddingHorizontal: 20,
  },
  textOffer: {
    fontFamily: "Poppins",
    color: "rgba(0,0,0,0.5)",
    fontSize: fz(1.4),
    marginTop: 10,
    width: wp(32),
  },
  textDestacados: {
    fontFamily: "Poppins-medium",
    fontSize: fz(2.2),
    marginBottom: hp(2),
  },
  textSell3: {
    textAlign: "left",
    fontWeight: "400",
    fontSize: fz(2),
    color: "#4B64Bc",
    fontFamily: "Poppins",
  },
  customButton: {
    width: wp(90),
    height: hp(7),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#06C167",
    marginTop: 20,
    borderRadius: 6,
  },
  iconContainer: {
    color: "rgba(33,33,33,0.5)",
    position: "absolute",
    zIndex: 10,
    bottom: hp(1.7),
    left: wp(8),
  },
});
