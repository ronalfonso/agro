import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { Ionicons } from "@expo/vector-icons";

const TransporterScreenRegister = (props) => {
  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={{ width: wp(100) }}>
        <View style={styles.containerCenter}>
          <Text style={styles.textDates}>Registrarme como transportador</Text>

          <View style={styles.containerInputs}>
            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="Nombre"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="Apellido"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholderTextColor={"#000"}
                keyboardType="numeric"
                placeholder="Número de documento"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={11}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="E-mail"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={{ ...styles.borderInput, ...styles.flexInput }}>
              <TextInput
                style={styles.input}
                placeholder="Número de licencia de conducir:"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
              <TouchableOpacity>
                <Ionicons
                  name="eye"
                  size={24}
                  color="black"
                  style={styles.iconEye}
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="Marca y model del vehículo:"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="Año:"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="N# de Placa:"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>

            <View style={styles.borderInput}>
              <TextInput
                style={styles.input}
                placeholder="Nombre de quien esta registrado el vehículo:"
                placeholderTextColor={"#000"}
                keyboardType="email-address"
                autoCapitalize="none"
                returnKeyType="next"
                maxLength={40}
              />
            </View>
            <Text style={styles.textBottom}>
              Ingresa tal como figura en tu documento
            </Text>
          </View>

          <View style={styles.containerCheck}>
            <BouncyCheckbox
              size={25}
              fillColor="#06C167"
              unfillColor="#FFFFFF"
            />

            <Text style={styles.containerText}>
              Acepto las{" "}
              <Text style={{ color: "#8091E9" }}>
                Politicas de Privacidad y los Terminos
              </Text>{" "}
              y{" "}
              <Text style={{ color: "#8091E9" }}>condiciones de Digiagro.</Text>
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("OffertTransporter");
            }}
          >
            <View style={styles.bottomRegister}>
              <Text style={styles.textRegister}>Continuar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default TransporterScreenRegister;

const styles = StyleSheet.create({
  containerCenter: {
    display: "flex",
    alignItems: "center",
    width: wp(100),
    zIndex: -1,
  },
  textDates: {
    textAlign: "center",
    fontSize: fz(2),
    fontFamily: "Poppins-bold",
    paddingTop: hp(5),
    color: "#212121",
    paddingBottom: hp(5),
  },
  containerInputs: {
    width: wp(90),
  },
  borderInput: {
    borderBottomColor: "#000000",
    borderBottomWidth: 1,
  },
  input: {
    fontSize: fz(2),
    fontFamily: "Poppins-medium",
  },
  textBottom: {
    fontSize: fz(1.4),
    fontFamily: "Poppins",
    color: "rgba(33,33,33,0.5)",
    paddingBottom: hp(3),
    width: wp(90),
  },
  containerCheck: {
    flexDirection: "row",
    paddingTop: hp(2),
    width: wp(90),
    paddingBottom: hp(7),
    justifyContent: "center",
  },
  containerText: {
    width: wp(70),
    textAlign: "center",
    fontFamily: "Poppins",
    fontSize: fz(1.5),
  },
  bottomRegister: {
    width: wp(90),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#06C167",
    borderRadius: 6,
    height: hp(7),
    marginBottom: 20,
  },
  textRegister: {
    textAlign: "center",
    fontWeight: "400",
    fontSize: fz(2),
    color: "#fff",
    fontFamily: "Poppins",
  },
  iconEye: {
    position: "absolute",
    right: 0,
    bottom: hp(1),
    fontSize: fz(3.5),
  },
});
