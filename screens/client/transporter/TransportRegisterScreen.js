import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Ionicons} from "@expo/vector-icons";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import {RFPercentage as fz} from "react-native-responsive-fontsize";
import React, {useState} from "react";
import ImagesTrucks from "../../components/ImagesTrucks";
import {useDispatch} from "react-redux";
import {useToast} from "react-native-toast-notifications";
import useMessage from "../../../hooks/useMessage";
import {createTruck} from "../../../store/actions/trucks";


const TransportRegisterScreen = (props) => {
    const Toast = useToast();
    const { successMessage, dangerMessage } = useMessage(Toast);
    const dispatch = useDispatch();

    const [truck, setTruck] = useState({
        name: '',
        brand: '',
        model: '',
        year: '',
        image: '',
        type_truck_id: 9,
    });

    const handleSubmit = () => {
        dispatch(createTruck(truck)).then((resp) => {
            console.log('respuesta en componente ',resp);
        })
    }

    return (
        <ScrollView style={{ backgroundColor: "#fff" }}>
            <View style={{ width: wp(100) }}>
                <View style={styles.containerCenter}>
                    <Text style={styles.textDates}>Registro de vehículo</Text>

                    <View style={styles.containerInputs}>
                        <View style={styles.borderInput}>
                            <TextInput
                                style={styles.input}
                                placeholder="Nombre del vehículo:"
                                placeholderTextColor={"#000"}
                                keyboardType="default"
                                autoCapitalize="none"
                                returnKeyType="next"
                                value={truck.name}
                                name={'name'}
                                onChangeText={(text) => {
                                    setTruck({...truck, name: text})
                                }}
                                maxLength={40}
                            />
                        </View>
                        <Text style={styles.textBottom}>
                            Ingresa tal como figura en tu documento
                        </Text>

                        <View style={styles.borderInput}>
                            <TextInput
                                style={styles.input}
                                placeholder="Marca del vehículo:"
                                placeholderTextColor={"#000"}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                returnKeyType="next"
                                value={truck.brand}
                                name={'brand'}
                                onChangeText={(text) => {
                                    setTruck({...truck, brand: text})
                                }}
                                maxLength={40}
                            />
                        </View>
                        <Text style={styles.textBottom}>
                            Ingresa tal como figura en tu documento
                        </Text>

                        <View style={styles.borderInput}>
                            <TextInput
                                style={styles.input}
                                placeholder="Model del vehículo:"
                                placeholderTextColor={"#000"}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                returnKeyType="next"
                                value={truck.model}
                                name={'model'}
                                onChangeText={(text) => {
                                    setTruck({...truck, model: text})
                                }}
                                maxLength={40}
                            />
                        </View>
                        <Text style={styles.textBottom}>
                            Ingresa tal como figura en tu documento
                        </Text>

                        <View style={styles.borderInput}>
                            <TextInput
                                style={styles.input}
                                placeholder="Año:"
                                placeholderTextColor={"#000"}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                returnKeyType="next"
                                value={truck.year}
                                name={'year'}
                                onChangeText={(text) => {
                                    setTruck({...truck, year: text})
                                }}
                                maxLength={40}
                            />
                        </View>
                        <Text style={styles.textBottom}>
                            Ingresa tal como figura en tu documento
                        </Text>

                        {/*<View style={styles.borderInput}>*/}
                        {/*    <TextInput*/}
                        {/*        style={styles.input}*/}
                        {/*        placeholder="N# de Placa:"*/}
                        {/*        placeholderTextColor={"#000"}*/}
                        {/*        keyboardType="email-address"*/}
                        {/*        autoCapitalize="none"*/}
                        {/*        returnKeyType="next"*/}
                        {/*        value={truck.name}*/}
                        {/*        name={'name'}*/}
                        {/*        onChangeText={(text) => {*/}
                        {/*            setTruck({...truck, name: text})*/}
                        {/*        }}*/}
                        {/*        maxLength={40}*/}
                        {/*    />*/}
                        {/*</View>*/}
                        {/*<Text style={styles.textBottom}>*/}
                        {/*    Ingresa tal como figura en tu documento*/}
                        {/*</Text>*/}

                        {/*<View style={styles.borderInput}>*/}
                        {/*    <TextInput*/}
                        {/*        style={styles.input}*/}
                        {/*        placeholder="Nombre de quien esta registrado el vehículo:"*/}
                        {/*        placeholderTextColor={"#000"}*/}
                        {/*        keyboardType="email-address"*/}
                        {/*        autoCapitalize="none"*/}
                        {/*        returnKeyType="next"*/}
                        {/*        value={truck.name}*/}
                        {/*        name={'name'}*/}
                        {/*        onChangeText={(text) => {*/}
                        {/*            setTruck({...truck, name: text})*/}
                        {/*        }}*/}
                        {/*        maxLength={40}*/}
                        {/*    />*/}
                        {/*</View>*/}
                        {/*<Text style={styles.textBottom}>*/}
                        {/*    Ingresa tal como figura en tu documento*/}
                        {/*</Text>*/}
                    </View>

                    <ImagesTrucks truck={truck} setTruck={setTruck}/>


                    <View style={styles.containerCheck}>
                        <BouncyCheckbox
                            size={25}
                            fillColor="#06C167"
                            unfillColor="#FFFFFF"
                        />

                        <Text style={styles.containerText}>
                            Acepto las{" "}
                            <Text style={{ color: "#8091E9" }}>
                                Politicas de Privacidad y los Terminos
                            </Text>{" "}
                            y{" "}
                            <Text style={{ color: "#8091E9" }}>condiciones de Digiagro.</Text>
                        </Text>
                    </View>


                    <TouchableOpacity
                        onPress={handleSubmit}
                    >
                        <View style={styles.bottomRegister}>
                            <Text style={styles.textRegister}>Continuar</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );
};

export default TransportRegisterScreen;

const styles = StyleSheet.create({
    containerCenter: {
        display: "flex",
        alignItems: "center",
        width: wp(100),
        zIndex: -1,
    },
    textDates: {
        textAlign: "center",
        fontSize: fz(2),
        fontFamily: "Poppins-bold",
        paddingTop: hp(5),
        color: "#212121",
        paddingBottom: hp(5),
    },
    containerInputs: {
        width: wp(90),
    },
    borderInput: {
        borderBottomColor: "#000000",
        borderBottomWidth: 1,
    },
    input: {
        fontSize: fz(2),
        fontFamily: "Poppins-medium",
    },
    textBottom: {
        fontSize: fz(1.4),
        fontFamily: "Poppins",
        color: "rgba(33,33,33,0.5)",
        paddingBottom: hp(3),
        width: wp(90),
    },
    containerCheck: {
        flexDirection: "row",
        paddingTop: hp(2),
        width: wp(90),
        paddingBottom: hp(7),
        justifyContent: "center",
    },
    containerText: {
        width: wp(70),
        textAlign: "center",
        fontFamily: "Poppins",
        fontSize: fz(1.5),
    },
    bottomRegister: {
        width: wp(90),
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#06C167",
        borderRadius: 6,
        height: hp(7),
        marginBottom: 20,
    },
    textRegister: {
        textAlign: "center",
        fontWeight: "400",
        fontSize: fz(2),
        color: "#fff",
        fontFamily: "Poppins",
    },
    iconEye: {
        position: "absolute",
        right: 0,
        bottom: hp(1),
        fontSize: fz(3.5),
    },
});
