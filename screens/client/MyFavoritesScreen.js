import React, { useEffect, useState } from "react";
import { Text, View, FlatList } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { FavoritiesStyles } from "../styles/FavoritiesStyles";
import { useDispatch, useSelector } from "react-redux";
import { homeStyles } from "../styles/HomeStyles";
import ItemsMyFavorities from "../components/ItemsMyFavorities";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../hooks/useMessage";
import * as Progress from "react-native-progress";
import {
  getProductFavorities,
  setLoading,
} from "../../store/actions/favorities";
import { useNavigation } from "@react-navigation/native";
import { checkInternetConnection } from "../../store/actions/noConnection";
import NoConnection from "../components/NoConnection";

const MyFavoritesScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const Toast = useToast();
  const { showMessage } = useMessage(Toast);
  const isLoading = useSelector((state) => state.favorities.loading);
  const connection = useSelector((state) => state.noConnection.noConnection);

  const allProductFavorities = useSelector(
    (state) => state.favorities.allProductFavorities
  );

  const [isLoadingIconID, setIsLoadingIconID] = useState(null);

  const getFavoritiesData = async () => {
    dispatch(setLoading());
    const fetchData = async () => {
      try {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
          dispatch(getProductFavorities());
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  };

  useEffect(() => {
    getFavoritiesData();
  }, []);

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      getFavoritiesData();
    });
    return onFocusData;
  }, [navigation]);

  if (isLoading) {
    return (
      <View
        style={{ ...homeStyles.containerLoading, top: hp(-5), width: wp(100) }}
      >
        <Progress.CircleSnail
          color={["#06C167", "#000"]}
          size={70}
          strokeCap={"square"}
        />
      </View>
    );
  }

  if (connection === false) {
    return <NoConnection functionLoading={getFavoritiesData} />;
  }

  if (!isLoading && allProductFavorities.length === 0) {
    return (
      <View style={FavoritiesStyles.containerEnableBackground}>
        <Text style={FavoritiesStyles.titleNotEnable}>No tienes favoritos</Text>
        <Text style={FavoritiesStyles.textNotEnable}>
          Aun no tienes productos en Favoritos
        </Text>
      </View>
    );
  }

  return (
    <>
      <View style={FavoritiesStyles.container}>
        <View style={FavoritiesStyles.containerFav}>
          <FlatList
            refreshing={isLoading}
            onRefresh={getFavoritiesData}
            showsVerticalScrollIndicator={false}
            key={"#"}
            numColumns={1}
            data={allProductFavorities}
            scrollEnabled={true}
            keyExtractor={(item) => item.id.toString()}
            renderItem={(itemData) => (
              <ItemsMyFavorities
                showMessage={showMessage}
                setIsLoadingIconID={setIsLoadingIconID}
                isLoadingIconID={isLoadingIconID}
                description={itemData.item.description}
                price={itemData.item.price}
                favorite_id={itemData.item.favorite_id}
                image={itemData.item.image}
              />
            )}
          />
        </View>
      </View>
    </>
  );
};

export default MyFavoritesScreen;
