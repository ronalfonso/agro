import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { MyAccountStyles } from "../../styles/MyAccountStyles";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useSelector } from "react-redux";
import { percentageToDPSize } from "../../../utils/CalculatePD";

const UserInfoScreen = () => {
  const userData = useSelector((state) => state.auth.userData);
  const userAddress = useSelector((state) => state.auth.userAddress);
  const departament = useSelector((state) => state.auth.departament);
  const municipalities = useSelector((state) => state.auth.municipalities);
  const filterDepartament = departament.data.filter(
    (item) => item.id === userAddress.department_id
  );
  const filterMunicipalite = municipalities.data.filter(
    (item) => item.id === userAddress.municipalite_id
  );

  return (
    <ScrollView
      style={{ ...MyAccountStyles.scrollStyle, paddingVertical: 20 }}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ marginBottom: 40 }}>
        <View style={styles.containerInfo}>
          <Text
            style={{
              fontFamily: "Poppins-medium",
              fontSize: percentageToDPSize(20),
            }}
          >
            Datos de cuenta:
          </Text>
          <View style={styles.containerCardInfo}>
            <View style={{ ...styles.contentCardInfo, borderBottomWidth: 0 }}>
              <Text style={styles.textTitle}>E-mail:</Text>
              <Text style={styles.textSubTitle}>
                {userData.email !== null ? userData.email : "No disponible"}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ ...styles.containerInfo, paddingTop: 40 }}>
          <Text
            style={{
              fontFamily: "Poppins-medium",
              fontSize: percentageToDPSize(20),
            }}
          >
            Datos personales:
          </Text>
          <View style={styles.containerCardInfo}>
            <View style={styles.contentCardInfo}>
              <Text style={styles.textTitle}>Nombre y apellido:</Text>
              <Text style={styles.textSubTitle}>{userData.name}</Text>
            </View>
            <View style={styles.contentCardInfo}>
              <Text style={styles.textTitle}>Documento:</Text>
              <Text style={styles.textSubTitle}>{userData.document}</Text>
            </View>
            <View style={{ ...styles.contentCardInfo, borderBottomWidth: 0 }}>
              <Text style={styles.textTitle}>Telefono:</Text>
              <Text
                style={styles.textSubTitle}
              >{`+${userData.code}-${userData.phone}`}</Text>
            </View>
          </View>
        </View>

        <View style={{ ...styles.containerInfo, paddingTop: 40 }}>
          <Text
            style={{
              fontFamily: "Poppins-medium",
              fontSize: percentageToDPSize(20),
            }}
          >
            Ubicación:
          </Text>
          <View style={styles.containerCardInfo}>
            <View style={styles.contentCardInfo}>
              <Text style={styles.textTitle}>Dirección:</Text>
              <Text style={styles.textSubTitle}>{userAddress.address}</Text>
            </View>
            <View style={styles.contentCardInfo}>
              <Text style={styles.textTitle}>Vereda:</Text>
              <Text style={styles.textSubTitle}>{userAddress.vereda}</Text>
            </View>
            <View style={styles.contentCardInfo}>
              <Text style={styles.textTitle}>Departamento:</Text>
              <Text style={styles.textSubTitle}>
                {filterDepartament?.[0]?.name}
              </Text>
            </View>
            <View style={{ ...styles.contentCardInfo, borderBottomWidth: 0 }}>
              <Text style={styles.textTitle}>Municipio:</Text>
              <Text style={styles.textSubTitle}>
                {filterMunicipalite?.[0]?.name}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  containerInfo: {
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  containerCardInfo: {
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    elevation: 10,
    marginTop: 20,
  },
  contentCardInfo: {
    height: hp(8),
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  textTitle: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(16),
    color: "#000",
  },
  textSubTitle: {
    fontFamily: "Poppins",
    fontSize: percentageToDPSize(15),
    color: "#21212180",
  },
});

export default UserInfoScreen;
