import React from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { dataPolicy } from "../../../data/user-data";

const UserPrivacyScreen = () => {
  return (
    <ScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
      <View style={{ marginBottom: 40 }}></View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    height: hp(100),
    width: wp(100),
    backgroundColor: "#F4F4F4",
    paddingTop: 20,
  },
});

export default UserPrivacyScreen;
