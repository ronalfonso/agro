import React, { useState, useEffect } from "react";
import { ScrollView } from "react-native";
import { MyAccountStyles } from "../../styles/MyAccountStyles";
import { useDispatch, useSelector } from "react-redux";
import * as ImagePicker from "expo-image-picker";
import useMessage from "../../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";
import {
  deletePhotoProfile,
  handleUpdateProfile,
} from "../../../store/actions/profile";
import { optionsProfile } from "../../../data/user-data";
import UserProfileHeader from "../../components/UserProfileHeader";
import UserProfileOptions from "../../components/UserProfileOptions";
import { getDepartament, getMunicipalities } from "../../../store/actions/auth";

const UserProfileScreen = (props) => {
  const dispatch = useDispatch();
  const Toast = useToast();
  const userData = useSelector((state) => state.auth.userData);
  const { dangerMessage, successMessage } = useMessage(Toast);
  const userAddress = useSelector((state) => state.auth.userAddress);
  const [isLoadingImage, setIsLoadingImage] = useState(false);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
      base64: true,
    });

    if (!result.canceled) {
      setIsLoadingImage(true);
      dispatch(handleUpdateProfile(result.assets[0].base64)).then(() => {
        setIsLoadingImage(false);
        successMessage("Operación exitosa");
      });
    } else {
      dangerMessage("Operación cancela");
    }
  };

  const deletePhoto = () => {
    setIsLoadingImage(true);
    dispatch(deletePhotoProfile()).then(() => {
      setIsLoadingImage(false);
      successMessage("Operación exitosa");
    });
  };

  useEffect(() => {
    dispatch(getDepartament()).then(() => {
      dispatch(getMunicipalities(userAddress.department_id));
    });
  }, []);

  return (
    <ScrollView
      style={MyAccountStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <UserProfileHeader
        userData={userData}
        pickImage={pickImage}
        isLoadingImage={isLoadingImage}
        deletePhoto={deletePhoto}
      />
      <UserProfileOptions
        options={optionsProfile}
        navigate={props.navigation.navigate}
      />
    </ScrollView>
  );
};

export default UserProfileScreen;
