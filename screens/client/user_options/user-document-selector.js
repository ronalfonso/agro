import React from "react";
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { SimpleLineIcons } from "@expo/vector-icons";
import { MyAccountStyles } from "../../styles/MyAccountStyles";
import { percentageToDPSize } from "../../../utils/CalculatePD";
import { optionUserEdit } from "../../../data/user-data";
import UserProfileOptions from "../../components/UserProfileOptions";

const UserDocumentSelectorScreen = (props) => {
  return (
    <ScrollView
      style={MyAccountStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ padding: 20 }}>
        <TouchableOpacity style={styles.cardSecurity}>
          <View style={styles.cardLeftItem}>
            <Image
              source={require("../../../assets/security-safe-green.png")}
              style={{
                width: 55,
                height: 55,
              }}
            />
          </View>
          <View style={styles.cardCenterItem}>
            <Text style={styles.textCardCenter}>
              Reportar un problema de seguridad
            </Text>
          </View>
          <View style={styles.cardRightItem}>
            <SimpleLineIcons name="arrow-right" size={20} color="#06C167" />
          </View>
        </TouchableOpacity>
      </View>

      <UserProfileOptions
        options={optionUserEdit}
        navigate={props.navigation.navigate}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  cardSecurity: {
    height: hp(7),
    borderRadius: 5,
    backgroundColor: "#FFF",
    alignItems: "center",
    flexDirection: "row",
    elevation: 10,
    width: wp(90),
  },

  cardLeftItem: {
    width: "15%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  cardCenterItem: {
    width: "75%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  textCardCenter: {
    fontFamily: "Poppins-medium",
    fontSize: percentageToDPSize(12),
    color: "#06C167",
  },

  cardRightItem: {
    width: "10%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default UserDocumentSelectorScreen;
