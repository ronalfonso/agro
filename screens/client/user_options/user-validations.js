import {
  emailRegex,
  numberDocumentRegex,
  phoneNumberRegex,
} from "../../../environment/regex";

export const validateOne = (
  password,
  password_confirmation,
  password_old,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;

  if (password_old.length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_old: "La contraseña actual es requerida",
    });
  } else if (password_old.length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_old: "La contraseña debe tener mínimo 6 caracteres",
    });
  }

  if (password.length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password: "La contraseña es requerida",
    });
  } else if (password.length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password: "La contraseña debe tener mínimo 6 caracteres",
    });
  }

  if (password_confirmation.length === 0) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_confirmation: "La contraseña es requerida",
    });
  } else if (password_confirmation.length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_confirmation: "La contraseña debe tener mínimo 6 caracteres",
    });
  }

  if (password !== password_confirmation) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      password_confirmation: "Las contraseñas ingresadas no son iguales",
    });
  }

  return isValid;
};

export const validateTwo = (
  name,
  document,
  email,
  phone,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;

  if (name.length === 0) {
    setErrors({
      ...errors,
      name: "El nombre es requerido",
    });
    isValid = false;
    setIsLoading(false);
  }

  if (name.length > 0 && name.length < 3) {
    setErrors({
      ...errors,
      name: "El nombre debe tener al menos 3 caracteres",
    });
    isValid = false;
    setIsLoading(false);
  }

  if (email.length > 0 && !emailRegex.test(email)) {
    isValid = false;
    setErrors({ ...errors, email: "El E-mail es inválido" });
    setIsLoading(false);
  }

  if (document.length > 0 && document.length < 6) {
    isValid = false;
    setErrors({
      ...errors,
      document: "El documento debe tener al menos 6 caracteres",
    });
    setIsLoading(false);
  } else if (document.length === 0) {
    isValid = false;
    setErrors({ ...errors, document: "El número de documento es requerido" });
    setIsLoading(false);
  }

  if (document.length > 0 && !numberDocumentRegex.test(document)) {
    isValid = false;
    setErrors({ ...errors, document: "El número de documento no valido" });
    setIsLoading(false);
  }

  if (phone.length === 0) {
    isValid = false;
    setErrors({ ...errors, phone: "El número de telefono es requerido" });
    setIsLoading(false);
  }

  if (phone.length !== 0 && !phoneNumberRegex.test(phone)) {
    isValid = false;
    setErrors({ ...errors, phone: "El número de telefono no es valido" });
    setIsLoading(false);
  }

  return isValid;
};

export const validateTheree = (
  address,
  vereda,
  department_id,
  municipalite_id,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;

  if (address.length === 0) {
    isValid = false;
    setErrors({ ...errors, address: "La direccion es requerida" });
  } else if (address.length !== 0 && address.length < 6) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      address: "La direccion debe tener al menos 6 caracteres",
    });
  }

  if (vereda.length === 0) {
    isValid = false;
    setErrors({ ...errors, vereda: "La vereda es requerida" });
  } else if (vereda.length > 0 && vereda.length < 6) {
    isValid = false;
    setErrors({
      ...errors,
      vereda: "La vereda debe tener al menos 6 caracteres",
    });
    setIsLoading(false);
  }

  if (department_id.length === 0) {
    isValid = false;
    setErrors({
      ...errors,
      department_id: "El departamento es requerido",
    });
    setIsLoading(false);
  }

  if (municipalite_id.length === 0) {
    isValid = false;
    setErrors({
      ...errors,
      municipalite_id: "El municipio es requerido",
    });
    setIsLoading(false);
  }

  return isValid;
};
