import React, { useState, useEffect } from "react";
import { View, ScrollView, Text, StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { validateOne, validateTwo, validateTheree } from "./user-validations";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../../hooks/useMessage";
import { getMunicipalities } from "../../../store/actions/auth";
import { MyAccountStyles } from "../../styles/MyAccountStyles";
import { percentageToDPSize } from "../../../utils/CalculatePD";
import LargeButton from "../../../components/UI/custom-bottom";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import {
  editPassword,
  editProfile,
  editAddress,
} from "../../../store/actions/profile";
import FormsEditUser from "../../components/FormsEditUser";

const UserEditInfo = (props) => {
  const dispatch = useDispatch();
  const Toast = useToast();
  const { successMessage } = useMessage(Toast);
  const { options } = props.route.params;
  const userData = useSelector((state) => state.auth.userData);
  const userAddress = useSelector((state) => state.auth.userAddress);
  const [isLoadingShow, setIsLoadingShow] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState({
    name: "",
    document: "",
    email: "",
    phone: "",
    password: "",
    password_confirmation: "",
    password_old: "",
    department_id: "",
    municipalite_id: "",
    address: "",
    vereda: "",
  });
  const [creadential, setCredentials] = useState({
    name: userData.name,
    document: userData.document,
    email: userData.email ?? "",
    code: userData.code,
    phone: userData.phone,
    password: "",
    password_confirmation: "",
    password_old: "",
    department_id: userAddress.department_id,
    municipalite_id: userAddress.municipalite_id,
    address: userAddress.address,
    vereda: userAddress.vereda,
  });

  useEffect(() => {
    if (options[0].type === "select") {
      setIsLoadingShow(true);
      dispatch(getMunicipalities(creadential.department_id)).then(() => {
        setIsLoadingShow(false);
      });
    }
  }, [creadential.department_id]);

  const handleEdit = () => {
    setIsLoading(true);
    const {
      department_id,
      municipalite_id,
      name,
      code,
      document,
      email,
      phone,
      password,
      password_confirmation,
      password_old,
      address,
      vereda,
    } = creadential;
    let isValid;

    switch (options[0].type) {
      case "password":
        isValid = validateOne(
          password,
          password_confirmation,
          password_old,
          errors,
          setErrors,
          setIsLoading
        );
        break;
      case "text":
        isValid = validateTwo(
          name,
          document,
          email,
          phone,
          errors,
          setErrors,
          setIsLoading
        );
        break;
      case "select":
        isValid = validateTheree(
          address,
          vereda,
          department_id,
          municipalite_id,
          errors,
          setErrors,
          setIsLoading
        );
        break;
      default:
        break;
    }

    if (isValid && options[0].type === "password") {
      dispatch(
        editPassword(
          password_old,
          password,
          password_confirmation,
          errors,
          setErrors
        )
      ).then((data) => {
        if (data.status === "Success") {
          successMessage(data.message);
          props.navigation.navigate("user-profile");
        }
        setIsLoading(false);
      });
    }

    if (isValid && options[0].type === "text") {
      dispatch(editProfile(name, document, email, code, phone)).then((data) => {
        if (data.status === "Success") {
          successMessage(data.message);
          props.navigation.navigate("user-profile");
        }
        setIsLoading(false);
      });
    }

    if (isValid && options[0].type === "select") {
      dispatch(
        editAddress(department_id, municipalite_id, address, vereda)
      ).then((data) => {
        if (data.status === "Success") {
          successMessage(data.message);
          props.navigation.navigate("user-profile");
        }
        setIsLoading(false);
      });
    }
  };

  if (isLoadingShow) {
    return <ProgressLoading label={false} />;
  }

  return (
    <ScrollView
      style={MyAccountStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ alignItems: "center", paddingTop: 20 }}>
        <Text style={styles.textTitle}>Completa todos los campos</Text>
        <FormsEditUser
          options={options}
          creadential={creadential}
          setCredentials={setCredentials}
          setErrors={setErrors}
          errors={errors}
        />
        <LargeButton
          isLoading={isLoading}
          handleSubmit={handleEdit}
          text={"Continuar"}
          disable={isLoading}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  textTitle: {
    fontSize: percentageToDPSize(20),
    fontFamily: "Poppins-medium",
    textAlign: "center",
    marginBottom: 20,
  },
});

export default UserEditInfo;
