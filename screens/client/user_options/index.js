import React, { useEffect } from "react";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
  Alert,
} from "react-native";
import { getInformationUser, logout } from "../../../store/actions/auth";
import { useDispatch, useSelector } from "react-redux";
import { MyAccountStyles } from "../../styles/MyAccountStyles";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import LargeButton from "../../../components/UI/custom-bottom";
import MyAccountList from "../../components/MyAccountList";
import { listOptionAccount } from "../../../data/user-data";

const MyAccountScreen = (props) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.auth.userData);

  useEffect(() => {
    dispatch(getInformationUser());
  }, []);

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      getInformationUser();
    });
    return onFocusData;
  }, [navigation]);

  const handleLogout = () => {
    Alert.alert("Confirmar", "¿Está seguro de que desea cerrar sesión?", [
      { text: "Volver", style: "cancel" },
      {
        text: "Si, salir",
        onPress: () => {
          dispatch(logout());
        },
      },
    ]);
  };

  return (
    <ScrollView
      style={MyAccountStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <View style={MyAccountStyles.containerHeader}>
        <LargeButton
          isLoading={false}
          handleSubmit={() => {
            props.navigation.navigate("create-product");
          }}
          text={"Vender"}
          disable={false}
        />
      </View>

      <MyAccountList
        optionsScreen={listOptionAccount}
        navigate={props.navigation.navigate}
      />

      <TouchableOpacity
        style={MyAccountStyles.containerFooter}
        onPress={handleLogout}
      >
        <View style={MyAccountStyles.containerLogoutLeft}>
          <Image
            source={require("../../../assets/profile-icon.png")}
            style={MyAccountStyles.imageIcon}
          />
        </View>
        <View style={MyAccountStyles.containerLogoutRight}>
          <Text style={MyAccountStyles.footerName}>{userData.name}</Text>
          <AntDesign name="poweroff" size={25} color="#21212180" />
        </View>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default MyAccountScreen;
