import React, { useEffect, useState } from "react";
import {
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getStoresDetail } from "../../../store/actions/stores";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import { storeStyles } from "../../styles/StoreSuppiesStyles";
import { Entypo, FontAwesome } from "@expo/vector-icons";

const StoreSuppliesDetailScreen = (props) => {
  const dispatch = useDispatch();
  const { idStore } = props.route.params;
  const [isLoading, setIsLoading] = useState(true);
  const storesDetail = useSelector((state) => state.stores.storesDetail);

  const fetchDetailStore = () => {
    dispatch(getStoresDetail(idStore)).then(() => {
      setIsLoading(false);
    });
  };

  useEffect(() => {
    fetchDetailStore();
  }, []);

  if (isLoading) {
    return <ProgressLoading />;
  }

  return (
    <ScrollView
      style={storeStyles.scrollView}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ alignItems: "center" }}>
        <View style={storeStyles.containerHeader}>
          <View style={storeStyles.containerImage}>
            <Image
              source={{ uri: storesDetail.data.imagen }}
              style={storesDetail.imageBackground}
            />
          </View>
          <View style={storeStyles.containerName}>
            <Text style={storeStyles.nameStore} numberOfLines={2}>
              {storesDetail.data.name}
            </Text>
          </View>
        </View>
        <View style={storeStyles.containerDescription}>
          <Text style={storeStyles.titleDescription}>
            Descripción de la tienda
          </Text>
          <Text style={storeStyles.textDescription} numberOfLines={10}>
            {storesDetail.data.description}
          </Text>
          <Text style={{ ...storeStyles.directionStore }}>
            {storesDetail.data.department_name}/
            {storesDetail.data.municipalite_name}
          </Text>
        </View>
        <View style={storeStyles.containerButtons}>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`tel:${storesDetail?.data?.phone}`);
            }}
            style={{
              ...storeStyles.contactBottom,
              marginBottom: 20,
              backgroundColor: "#fff",
            }}
          >
            <Text style={storeStyles.textBottom}>Contactar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                `whatsapp://send?phone=+${storesDetail?.data?.phone}`
              );
            }}
            style={{ ...storeStyles.contactBottom, backgroundColor: "#06C167" }}
          >
            <Text style={{ ...storeStyles.textBottom, color: "#fff" }}>
              Whatsapp
            </Text>
          </TouchableOpacity>
        </View>
        <View style={storeStyles.containerReputation}>
          <Text style={storeStyles.titleDescription}>
            Reputación como vendedor
          </Text>
          <Text style={storeStyles.directionStore}>
            Crea una y empieza a vender cuando quieras.
          </Text>
          <Text style={storeStyles.opinionsStore}>251 Opiniones</Text>
          <View style={{ flexDirection: "row" }}>
            {[1, 2, 3, 4, 5].map(() => {
              return <Entypo name="star" size={24} color="#FFC300" />;
            })}
          </View>
        </View>
        <View style={storeStyles.containerOpitions}>
          <Text style={storeStyles.titleDescription}>Opiniones</Text>
          <Text style={storeStyles.directionStore}>
            Crea una y empieza a vender cuando quieras.
          </Text>
        </View>

        {[1, 2, 3, 4, 5].map(() => {
          return (
            <View style={storeStyles.container}>
              <View style={storeStyles.containerLeft}>
                <FontAwesome name="user-circle" size={40} color="#FFC300" />
              </View>
              <View style={storeStyles.containerRight}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text style={storeStyles.textContainer}>Carlos Olarte</Text>
                  {[1, 2, 3, 4, 5].map(() => {
                    return <Entypo name="star" size={24} color="#FFC300" />;
                  })}
                </View>
                <Text style={storeStyles.subTitleContainer}>
                  Crea una y empieza a vender cuando quieras. Crea una y empieza
                  a vender cuando quieras.
                </Text>
              </View>
            </View>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default StoreSuppliesDetailScreen;
