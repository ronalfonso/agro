import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { getStoresList } from "../../../store/actions/stores";
import { useDispatch, useSelector } from "react-redux";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import {
  percentageToDPHeight,
  percentageToDPSize,
  percentageToDPWidth,
} from "../../../utils/CalculatePD";
import { useNavigation } from "@react-navigation/native";
import TextInputSearch from "../../../components/UI/custom-input-search";
import EmptyDateScreen from "../../../components/UI/screen-emty-data";
import { storeStyles } from "../../styles/StoreSuppiesStyles";

const StoreSuppliesScreen = (props) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const stores = useSelector((state) => state.stores.storesAll);
  const [filteredData, setFilteredData] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      getStores();
    });
    return onFocusData;
  }, [navigation]);

  const getStores = async () => {
    dispatch(getStoresList()).then(() => {
      setIsLoading(false);
    });
  };

  useEffect(() => {
    const filteredArray = stores?.data.filter((item) => {
      return item.name.toLowerCase().includes(searchTerm.toLowerCase());
    });
    setFilteredData(filteredArray);
  }, [searchTerm]);

  if (isLoading) {
    return <ProgressLoading />;
  }

  return (
    <View style={{ alignItems: "center", backgroundColor: "#fff" }}>
      <TextInputSearch
        placeholder="Buscar tienda"
        value={searchTerm}
        setValue={setSearchTerm}
      />
      <ScrollView
        style={storeStyles.scrollView}
        showsVerticalScrollIndicator={false}
      >
        {filteredData.length > 0 ? (
          <>
            {filteredData.map(({ id, name, imagen }) => {
              return (
                <TouchableOpacity
                  style={styles.containerStore}
                  key={id}
                  onPress={() => {
                    props.navigation.navigate("StoreSuppliesDetail", {
                      idStore: id,
                    });
                  }}
                >
                  <View style={storeStyles.containerImage}>
                    <Image
                      source={{ uri: imagen }}
                      style={storeStyles.imageBackground}
                    />
                  </View>
                  <View style={storeStyles.containerName}>
                    <Text style={storeStyles.nameStore} numberOfLines={2}>
                      {name}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </>
        ) : (
          <EmptyDateScreen
            title={`0 Resultados para (${searchTerm})`}
            subtitle="Verifique que la palabra esté bien escrita"
          />
        )}
      </ScrollView>
    </View>
  );
};

export default StoreSuppliesScreen;

const styles = StyleSheet.create({
  containerStore: {
    height: percentageToDPHeight(120),
    width: wp(100),
    borderBottomWidth: 1,
    borderBottomColor: " rgba(33, 33, 33, 0.25)",
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
  },
});
