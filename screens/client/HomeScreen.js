import React, { useEffect } from "react";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Animated,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { homeStyles } from "../styles/HomeStyles";
import ItemFeatured from "./dashboard/ItemFeatured";
import ItemFavorities from "./dashboard/ItemFavorities";
import ItemSupplyStore from "./dashboard/ItemSupplyStore";
import ItemOfferTransport from "./dashboard/ItemOfferTransport";
import ItemCategoriesJobs from "./dashboard/ItemCategoriesJobs";
import {
  getProductFavoritiesHomes,
  setLoadingHomes,
} from "../../store/actions/favorities";
import SkeletonLoading from "../components/SkeletonLoading";
import {
  getProductsAuthHomes,
  getProductsOfflineHomes,
  setLoadingHomesProduct,
} from "../../store/actions/products";
import { getStoresList } from "../../store/actions/stores";
import {
  getCategoriesJobs,
  getCategoriesProduct,
  setLoadingHomesCategoriesJobs,
  setLoadingHomesCategoriesProduct,
} from "../../store/actions/categories";
import UseSearchBar from "../../hooks/useSearchBar";
import ItemsCategories from "../components/ItemsCategories";
import { checkInternetConnection } from "../../store/actions/noConnection";
import NoConnection from "../components/NoConnection";
import CardImageHome from "../../components/UI/CardImageHome";
import IconsOptionsHome from "../../components/UI/IconsOptionsHome";
import {
  getTotalNotification,
  saveTokenDevice,
} from "../../store/actions/notifications";
import { getInformationUser } from "../../store/actions/auth";
import MetricsItems from "../components/MetricsItems";
import { widthPercentageToDP } from "react-native-responsive-screen";
import {registerForPushNotificationsAsync} from "../../utils/RegisterForPushNotificationsAsync";
import {getTrucks} from "../../store/actions/trucks";

const HomeScreen = (props) => {
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);
  const loadingFavorities = useSelector(
    (state) => state.favorities.loadingDataHomes
  );
  const loadingProduct = useSelector((state) => state.products.loadingProduct);
  const loadingStores = useSelector((state) => state.stores.loadingStoresHome);
  const connection = useSelector((state) => state.noConnection.noConnection);
  const loadingCategoriesHomesJobs = useSelector(
    (state) => state.categories.loadingCategoriesHomesJobs
  );
  const loadingCategoriesHomesProduct = useSelector(
    (state) => state.categories.loadingCategoriesHomesProduct
  );
  const isAuth = useSelector((state) => !!state.auth.token);
  const fadeAnim = new Animated.Value(0);

  useEffect(() => {
    registerForPushNotificationsAsync().then((value) => {
      const regex = /\[(.*?)\]/;
      const match = regex.exec(value);
      if (match && match.length > 1) {
        const token = match[1];
        dispatch(saveTokenDevice(token));
      }
    });
  }, []);

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
    }).start();
  }, [fadeAnim]);

  const loadingDataHomes = async () => {
    // LOADING
    dispatch(setLoadingHomes());
    dispatch(setLoadingHomesCategoriesProduct());
    dispatch(setLoadingHomesCategoriesJobs());
    dispatch(setLoadingHomesProduct());
    // LOADING
    const fetchData = async () => {
      try {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
          dispatch(getInformationUser());
          dispatch(getTotalNotification());
          dispatch(getProductFavoritiesHomes());
          dispatch(getStoresList());
          dispatch(getCategoriesJobs());
          if (isAuth) {
            dispatch(getProductsAuthHomes());
          } else {
            dispatch(getProductsOfflineHomes());
          }
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  };

  const reloadingDatas = async () => {
    dispatch(getTotalNotification());
    dispatch(getProductFavoritiesHomes());
    dispatch(getStoresList());
    dispatch(getCategoriesProduct());
    dispatch(getCategoriesJobs());
    dispatch(getTrucks());
    if (isAuth) {
      dispatch(getProductsAuthHomes());
    } else if (!isAuth) {
      dispatch(getProductsOfflineHomes());
    }
  };

  useEffect(() => {
    const onFocusData = props.navigation.addListener("focus", () => {
      reloadingDatas();
    });
    return onFocusData;
  }, [props.navigation]);

  useEffect(() => {
    loadingDataHomes();
    // props.navigation.setOptions({
    //   headerRight: () => <UseSearchBar />,
    // });
  }, [props.route.params]);

  if (loadingFavorities || loadingProduct || loadingCategoriesHomesJobs) {
    return <SkeletonLoading />;
  }

  if (connection === false) {
    return <NoConnection functionLoading={loadingDataHomes} />;
  }

  return (
    <ScrollView
      style={homeStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <Animated.View style={[{ transform: [{ scaleX: fadeAnim }] }]}>
        {/* QUIERO VENDER */}
        <View style={homeStyles.container}>
          <CardImageHome
            props={props}
            image={require("../../assets/Home/HomePerson.png")}
            text="Quiero vender"
            colorBottom="#FFFFFF"
            backgroundBottom="#06C167"
            path="create-product"
            position={{
              justifyContent: "flex-end",
            }}
            sizeBottom={{
              width: "60%",
            }}
          >
            <Text style={homeStyles.textCardBackground}>
              El campo en tus manos
            </Text>
          </CardImageHome>
        </View>
        {/* QUIERO VENDER */}

        {/* CATEGORIAS */}
        {token !== null && (
          <View style={homeStyles.container}>
            <Text
              style={{
                fontFamily: "Poppins-medium",
                alignSelf: "flex-start",
                paddingHorizontal: 20,
              }}
            >
              Compra por categoría:
            </Text>
            <ItemsCategories props={props} />
          </View>
        )}
        {/* END CATEGORIAS */}

        {token === null && (
          <View style={homeStyles.container}>
            <View style={homeStyles.containerLogout}>
              <Text style={homeStyles.textAccoutsExperiencia}>
                ¡Crea una cuenta y mejora tu experiencia!
              </Text>
              <TouchableOpacity
                style={homeStyles.containerBtnAccout}
                onPress={() => props.navigation.navigate("RegisterScreen")}
              >
                <Text style={{ fontFamily: "Poppins-medium", color: "#FFF" }}>
                  Crear cuenta
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={homeStyles.btnRegisterAccoutns}
                onPress={() => props.navigation.navigate("LoginScreen")}
              >
                <Text
                  style={{ fontFamily: "Poppins-medium", color: "#06C167" }}
                >
                  Ingresar a mi cuenta
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {/* ADD PRODUCT */}
        <View style={homeStyles.container}>
          <CardImageHome
            props={props}
            image={require("../../assets/Home/HomePerson2.png")}
            text=" Publica un producto"
            colorBottom="#FFFFFF"
            backgroundBottom="#FFC300"
            path="create-product"
            position={{
              justifyContent: "flex-end",
            }}
            sizeBottom={{
              width: "70%",
            }}
          >
            <Text style={homeStyles.textCardBackground}>
              Vende productos a un precio mas amplio sin comiciones
            </Text>
          </CardImageHome>
        </View>
        {/* ADD PRODUCT */}

        {/* ICONS */}
        <View style={homeStyles.container}>
          <IconsOptionsHome props={props} />
        </View>
        {/* ICONS */}

        {/* SUMINISTROS */}
        <View style={{ ...homeStyles.container }}>
          <CardImageHome
            props={props}
            image={require("../../assets/Home/Suministros.png")}
            text="Ver todo"
            colorBottom="#068749"
            backgroundBottom="#FFC300"
            path="StoreSupplies"
            position={{
              alignItems: "center",
              justifyContent: "center",
            }}
            sizeBottom={{
              width: "70%",
            }}
          >
            <Text
              style={{
                ...homeStyles.textCardBackgroundBold,
                textAlign: "center",
              }}
            >
              Suministros para tu siembra
            </Text>
          </CardImageHome>
        </View>
        {/* SUMINISTROS */}

        {/* DESTACADOS */}
        <View style={homeStyles.container}>
          <ItemFeatured token={token} navigate={props.navigation} />
        </View>
        {/* DESTACADOS */}

        {/* QUIERO VENDER */}
        <View style={homeStyles.container}>
          <CardImageHome
            props={props}
            image={require("../../assets/Home/granga.png")}
            text="Quiero vender"
            colorBottom="#068749"
            backgroundBottom="#FFC300"
            path="StoreSupplies"
            position={{
              justifyContent: "flex-end",
            }}
            sizeBottom={{
              width: "60%",
            }}
          />
        </View>
        {/* QUIERO VENDER */}

        {/* BASADO EN FAVORITOS */}
        {token !== null && (
          <View style={homeStyles.container}>
            <ItemFavorities token={token} navigate={props.navigation} />
          </View>
        )}
        {/* BASADO EN FAVORITOS */}

        {/* TIENDA DE SUMINISTROS */}
        <View style={homeStyles.container}>
          <ItemSupplyStore token={token} navigate={props.navigation} />
        </View>
        {/* TIENDA DE SUMINISTROS */}

        {/* OFERTAS DE TRANSPORTE */}
        <View style={{ ...homeStyles.container }}>
          <CardImageHome
            props={props}
            image={require("../../assets/Home/camion.png")}
            text="Registrar vehículo"
            colorBottom="#FFF"
            backgroundBottom="#FFC300"
            path="Transport"
            position={{
              justifyContent: "center",
              alignItems: "center",
            }}
            sizeBottom={{
              width: "90%",
            }}
          >
            <Text style={homeStyles.textCardBackground}>
              Ofrece o descubre transportes
            </Text>
            <Text style={homeStyles.textCardBackgroundMedium}>
              Ofertas de transportes
            </Text>
          </CardImageHome>
        </View>
        {/* OFERTAS DE TRANSPORTE */}

        {/* OFERTAS DE TRANSPORTE */}
        <View style={homeStyles.container}>
          <TouchableOpacity style={homeStyles.offerTransportBtn}>
            <Text style={homeStyles.offerTransportText}>
              Transportadores Disponibles
            </Text>
          </TouchableOpacity>
        </View>
        {/* OFERTAS DE TRANSPORTE */}

        {/* OFERTAS DE TRANSPORTE */}
        <View style={homeStyles.container}>
          <ItemOfferTransport token={token} navigate={props.navigation} />
        </View>
        {/* OFERTAS DE TRANSPORTE */}

        {/* BUSCO EMPLEO */}
        <View style={homeStyles.container}>
          <CardImageHome
            props={props}
            image={require("../../assets/transporte/work.png")}
            text="Busco empleo"
            colorBottom="#FFF"
            backgroundBottom="#06C167"
            path="job-board"
            position={{
              justifyContent: "center",
              alignItems: "center",
            }}
            sizeBottom={{
              width: "90%",
            }}
          >
            <Text
              style={{ ...homeStyles.textCardBackground, textAlign: "center" }}
            >
              Encuentra un trabajo soñado
            </Text>
            <Text
              style={{
                ...homeStyles.textCardBackgroundMedium,
                textAlign: "center",
              }}
            >
              Bolsa de empleos
            </Text>
          </CardImageHome>
        </View>
        {/* BUSCO EMPLEO */}

        {/* BUSCO EMPLEO */}
        <View style={homeStyles.container}>
          <ItemCategoriesJobs token={token} navigate={props.navigation} />
        </View>
        {/* BUSCO EMPLEO */}

        {/* BOLSA DE PRECIOS */}
        <View style={homeStyles.container}>
          <CardImageHome
            props={props}
            image={require("../../assets/transporte/verduras.png")}
            text="Explorar"
            colorBottom="#00504E"
            backgroundBottom="#FFC300"
            path="PriceExchange"
            position={{
              alignItems: "center",
              justifyContent: "center",
            }}
            sizeBottom={{
              width: "90%",
            }}
          >
            <Text
              style={{
                ...homeStyles.textCardBackgroundMedium,
                textAlign: "center",
              }}
            >
              Bolsa de Precios
            </Text>
            <Text
              style={{ ...homeStyles.textCardBackground, textAlign: "center" }}
            >
              Lista de precios diaria en tu localidad
            </Text>
          </CardImageHome>
        </View>
        {/* BOLSA DE PRECIOS */}
        {/* END */}
      </Animated.View>
    </ScrollView>
  );
};

export default HomeScreen;
