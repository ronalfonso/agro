import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useDispatch, useSelector } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import {
  addFavorities,
  deleteFavoritiesProduct,
  FAVORITIES_ID,
} from "../../../store/actions/favorities";
import * as Animatable from "react-native-animatable";
import CardContainerGray from "../../../components/UI/CardContainerGray";

const ItemFeatured = (props) => {
  const dispatch = useDispatch();
  const { token, navigate } = props;
  const isAuth = useSelector((state) => !!state.auth.token);
  const favoritiesID = useSelector((state) => state.favorities.favoritiesID);
  const productsFeatured = useSelector(
    (state) => state.products.productsFeatured
  );

  useEffect(() => {
    if (productsFeatured.lenght !== 0) {
      const filterFavoritedIds = () => {
        const filteredData = productsFeatured.filter((element) => {
          return element.favorite_id !== null;
        });
        const filteredIds = filteredData.map((element) => element.id);
        dispatch({ type: FAVORITIES_ID, favoritiesID: filteredIds });
      };
      filterFavoritedIds();
    }
  }, [productsFeatured]);

  const toggleFavorite = (id, favorite_id) => {
    const index = favoritiesID.indexOf(id);
    if (index !== -1) {
      const newFavorites = [...favoritiesID];
      newFavorites.splice(index, 1);
      dispatch({ type: FAVORITIES_ID, favoritiesID: newFavorites });
      dispatch(deleteFavoritiesProduct(favorite_id));
    } else {
      const newFavorites = [...favoritiesID, id];
      dispatch({ type: FAVORITIES_ID, favoritiesID: newFavorites });
      dispatch(addFavorities(id));
    }
  };

  const sendDetail = (id) => {
    if (token !== null) {
      navigate.navigate("product-detail", {
        idProduct: id,
      });
    } else if (token === null) {
      navigate.navigate("WelcomeScreen");
    }
  };

  const FavoritiesIcon = ({ favorite_id, id }) => {
    const isFavorite = favoritiesID.includes(id);

    return (
      <>
        {isAuth && (
          <Animatable.View
            style={{
              zIndex: 100,
            }}
          >
            <TouchableOpacity
              style={styles.containerIcon}
              onPress={() => {
                toggleFavorite(id, favorite_id);
              }}
            >
              <Ionicons
                name={isFavorite ? "heart" : "heart-outline"}
                size={25}
                color="#06C167"
              />
            </TouchableOpacity>
          </Animatable.View>
        )}
      </>
    );
  };

  if (productsFeatured.length === 0) {
    return (
      <CardContainerGray title="Destacados" footer={false}>
        <View style={styles.containerNoneItem}>
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemNone}>
            No hay productos disponibles
          </Text>
        </View>
      </CardContainerGray>
    );
  }

  return (
    <CardContainerGray
      title="Destacados"
      footer={true}
      titleFooter="Ver todo"
      path="allProducts"
      props={props}
      positionFooter={{
        alignItems: "flex-end",
      }}
    >
      <View style={styles.containerProducts}>
        {productsFeatured.map(
          ({ id, image, price, description, favorite_id }) => {
            return (
              <TouchableOpacity
                key={id}
                style={styles.productItemContainer}
                onPress={() => {
                  sendDetail(id);
                }}
              >
                <FavoritiesIcon favorite_id={favorite_id} id={id} />
                <View style={styles.containerImageProduct}>
                  <Image
                    style={{ width: "100%", height: "100%" }}
                    source={{ uri: image }}
                  />
                </View>
                <View style={styles.containerTextProduct}>
                  <Text style={styles.titleProduct}>$ {price}</Text>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={styles.infoProduct}
                  >
                    {description}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }
        )}
      </View>
    </CardContainerGray>
  );
};

export default ItemFeatured;

const styles = StyleSheet.create({
  containerIcon: {
    height: hp(3.5),
    width: wp(7.5),
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    left: "80%",
    position: "absolute",
  },

  containerProducts: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },

  productItemContainer: {
    width: "50%",
    height: hp(27),
    padding: 10,
    position: "relative",
  },

  containerImageProduct: {
    backgroundColor: "#EAEAEA",
    height: "60%",
    width: "100%",
    padding: 10,
  },

  containerTextProduct: {
    height: "40%",
    width: "100%",
    flexWrap: "nowrap",
  },

  titleProduct: {
    marginTop: hp(1),
    fontFamily: "Poppins-medium",
    fontSize: fz(1.9),
  },

  infoProduct: {
    fontSize: fz(1.4),
    fontFamily: "Poppins",
  },

  containerNoneItem: {
    height: hp(15),
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },

  itemNone: {
    fontFamily: "Poppins-medium",
    fontSize: fz(1.8),
    textAlign: "center",
  },
});
