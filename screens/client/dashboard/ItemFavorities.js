import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteFavorities,
  getProductFavoritiesHomes,
  PRODUCTS_FAVORITIES,
} from "../../../store/actions/favorities";
import CardContainerGray from "../../../components/UI/CardContainerGray";

const ItemFavorities = (props) => {
  const dispatch = useDispatch();
  const favoritiesID = useSelector((state) => state.favorities.favoritiesID);
  const productFavorities = useSelector(
    (state) => state.favorities.productFavorities
  );

  useEffect(() => {
    dispatch(getProductFavoritiesHomes());
  }, [favoritiesID]);

  const handleDeleteFav = async (id) => {
    const updateData = await productFavorities.filter(
      (item) => item.favorite_id !== id
    );

    await dispatch({
      type: PRODUCTS_FAVORITIES,
      productFavorities: updateData,
    });
    await dispatch(deleteFavorities(id));
  };

  if (productFavorities.length === 0) {
    return (
      <CardContainerGray title="Basado en tus favoritos" footer={false}>
        <View style={styles.containerNoneItem}>
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemNone}>
            Aun no tienes favoritos disponibles
          </Text>
        </View>
      </CardContainerGray>
    );
  }

  return (
    <CardContainerGray
      title="Basado en tus favoritos"
      footer={true}
      titleFooter="Ir a mis favoritos"
      path="Favorites"
      props={props}
      positionFooter={{
        alignItems: "flex-end",
      }}
    >
      {productFavorities.map(
        ({ id, description, price, image, favorite_id }) => {
          return (
            <View style={{ height: hp(19) }} key={id}>
              <View style={styles.containerProducts}>
                <View style={styles.containerItemProduct}>
                  <View style={styles.containerLeftItem}>
                    <Image
                      style={{ width: "100%", height: "100%" }}
                      source={{ uri: image }}
                    />
                  </View>

                  <View style={styles.containerRightItem}>
                    <Text numberOfLines={2} style={styles.textTitle}>
                      {description}
                    </Text>
                    <Text style={styles.textItemPrice}>${price}</Text>
                    <TouchableOpacity
                      style={{ paddingTop: hp(1) }}
                      onPress={() => {
                        handleDeleteFav(favorite_id);
                      }}
                    >
                      <Text style={styles.textDelete}>Eliminar</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          );
        }
      )}
    </CardContainerGray>
  );
};

export default ItemFavorities;

const styles = StyleSheet.create({
  containerProducts: {
    flexDirection: "column",
    padding: 10,
    width: "100%",
    height: hp(17),
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },

  containerItemProduct: {
    height: "100%",
    width: "100%",
    flexDirection: "row",
  },

  containerLeftItem: {
    width: "40%",
    height: "100%",
    backgroundColor: "#EAEAEA",
    padding: 10,
    position: "relative",
  },

  containerRightItem: {
    width: "60%",
    height: "100%",
    paddingHorizontal: 10,
  },

  textTitle: {
    fontFamily: "Poppins",
    fontSize: fz(1.5),
    lineHeight: fz(2),
  },

  textItemPrice: {
    paddingTop: hp(2),
    fontFamily: "Poppins-medium",
    fontSize: fz(2),
    lineHeight: fz(2),
  },

  textDelete: {
    fontFamily: "Poppins",
    fontSize: fz(1.5),
    color: "#5385E4",
  },

  containerContent: {
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    padding: 20,
    width: wp(90),
    elevation: 5,
  },

  title: {
    color: "#000",
    fontSize: fz(2.5),
    fontFamily: "Poppins-medium",
  },

  containerNoneItem: {
    height: hp(15),
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },

  itemNone: {
    textAlign: "center",
    fontFamily: "Poppins-medium",
    fontSize: fz(1.8),
  },
});
