import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import CardContainerGray from "../../../components/UI/CardContainerGray";
import { Ionicons } from "@expo/vector-icons";
import {useSelector} from "react-redux";

const ItemOfferTransport = (props) => {
  const {data} = useSelector((state) => state.trucks.truckList);
  console.log(data);
  // const data = [
  //   {
  //     id: 1,
  //     price: "36,00",
  //     title: "Lorem inpsundolor sit amet",
  //     image: require("../../../assets/transporte/camion1.png"),
  //     send: false,
  //     product: "Producto de Multiagro",
  //   },
  // ];

  const FavoritiesIcon = () => {
    return (
      <View style={{ zIndex: 55 }}>
        <TouchableOpacity style={styles.containerIcon}>
          <Ionicons name={"heart-outline"} size={25} color="#06C167" />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <CardContainerGray
      title="Ofertas de transportes"
      footer={true}
      titleFooter="Ver todo"
      path="OffertTransporter"
      props={props}
      positionFooter={{
        alignItems: "flex-start",
      }}
    >
      {
        data !== undefined &&
        data.map(({ id, name, image }) => {
        return (
          <View style={{ height: hp(19) }} key={id}>
            <View style={styles.containerOfferTransport}>
              <View style={styles.containerItemOffer}>
                <View style={styles.containerLeftItem}>
                  <FavoritiesIcon />
                  <Image
                    style={{ width: "100%", height: "100%" }}
                    source={{uri: image}}
                  />
                </View>
                <View style={styles.containerRightItem}>
                  <Text numberOfLines={2} style={styles.textTitle}>
                    {name}
                  </Text>
                  <Text style={styles.textItemPrice}>$ 400</Text>
                </View>
              </View>
            </View>
          </View>
        );
      })}
    </CardContainerGray>
  );
};

export default ItemOfferTransport;

const styles = StyleSheet.create({
  containerOfferTransport: {
    flexDirection: "column",
    padding: 10,
    width: "100%",
    height: hp(17),
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },

  containerItemOffer: {
    height: "100%",
    width: "100%",
    position: "relative",
    flexDirection: "row",
  },

  containerLeftItem: {
    width: "40%",
    height: "100%",
    backgroundColor: "#EAEAEA",
    padding: 10,
  },

  containerRightItem: {
    width: "60%",
    height: "100%",
    paddingHorizontal: 10,
  },

  textTitle: {
    fontFamily: "Poppins",
    fontSize: fz(1.5),
    lineHeight: fz(2),
  },

  textItemPrice: {
    paddingTop: hp(2),
    fontFamily: "Poppins-medium",
    fontSize: fz(2),
    lineHeight: fz(2),
  },

  containerIcon: {
    height: hp(3.5),
    width: wp(7.5),
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    left: "80%",
    position: "absolute",
  },
});
