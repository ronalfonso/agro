import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { Octicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import CardContainerGray from "../../../components/UI/CardContainerGray";

const ItemCategoriesJobs = (props) => {
  const { token, navigate } = props;
  const categoriesJobs = useSelector(
    (state) => state.categories.categoriesJobs
  );

  const sendDetail = (id) => {
    if (token !== null) {
      navigate.navigate("list-jobs", {
        idCategori: id,
      });
    } else if (token === null) {
      navigate.navigate("WelcomeScreen");
    }
  };

  if (categoriesJobs.data.length === 0) {
    return (
      <CardContainerGray title="Categorías" footer={false}>
        <View style={styles.containerNoneItem}>
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemNone}>
            No hay categorías disponibles en este momento
          </Text>
        </View>
      </CardContainerGray>
    );
  }

  return (
    <CardContainerGray
      title="Categorías"
      footer={true}
      titleFooter="Ver todas las categorías"
      path="job-board"
      props={props}
      positionFooter={{
        alignItems: "flex-start",
      }}
    >
      <View style={styles.containerLine} />
      {categoriesJobs?.data?.slice(0, 4).map(({ id, name }) => {
        return (
          <View style={{ height: hp(10) }} key={id}>
            <View style={styles.containerCategoriItems}>
              <TouchableOpacity
                onPress={() => {
                  sendDetail(id);
                }}
                style={styles.containerCategoriItem}
              >
                <View style={styles.containerItemLeft}>
                  <View style={styles.circleCategori} />
                </View>

                <View style={styles.containerItemRight}>
                  <Text
                    style={{ fontFamily: "Poppins-medium", fontSize: fz(2.1) }}
                    numberOfLines={2}
                  >
                    {name}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        );
      })}
      <View style={styles.containerLine} />
    </CardContainerGray>
  );
};

export default ItemCategoriesJobs;

const styles = StyleSheet.create({
  containerLine: {
    marginVertical: hp(2),
    borderWidth: 1,
    borderColor: "#ccc",
  },

  containerCategoriItems: {
    flexDirection: "column",
    padding: 10,
    width: "100%",
    height: "100%",
  },

  containerCategoriItem: {
    height: "100%",
    width: "100%",
    position: "relative",
    flexDirection: "row",
  },

  containerItemLeft: {
    width: "20%",
    height: "100%",
    padding: 10,
    justifyContent: "center",
  },

  containerItemRight: {
    width: "80%",
    height: "100%",
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    justifyContent: "center",
  },

  circleCategori: {
    height: hp(5),
    width: wp(11),
    backgroundColor: "#D9D9D9",
    borderRadius: 100,
  },

  containerNoneItem: {
    height: hp(15),
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },

  itemNone: {
    fontFamily: "Poppins-medium",
    fontSize: fz(1.8),
    textAlign: "center",
  },
});
