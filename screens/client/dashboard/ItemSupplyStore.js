import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { useSelector } from "react-redux";
import CardContainerGray from "../../../components/UI/CardContainerGray";

const ItemSupplyStore = (props) => {
  const { token, navigate } = props;
  const stores = useSelector((state) => state.stores.storesAll);
  const [tiendas, setTiendas] = useState([]);

  useEffect(() => {
    setTiendas(stores?.data?.slice(0, 6));
  }, [stores]);

  const sendDetail = (id) => {
    if (token !== null) {
      navigate.navigate("StoreSuppliesDetail", {
        idStore: id,
      });
    } else if (token === null) {
      navigate.navigate("WelcomeScreen");
    }
  };

  if (tiendas?.length === 0) {
    return (
      <CardContainerGray title="Tienda de suministros" footer={false}>
        <View style={styles.containerNoneItem}>
          <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemNone}>
            No hay tiendas de suministros disponibles en este momento
          </Text>
        </View>
      </CardContainerGray>
    );
  }

  return (
    <CardContainerGray
      title="Tienda de suministros"
      footer={true}
      titleFooter="Ver todas las tiendas"
      path="StoreSupplies"
      props={props}
      positionFooter={{
        alignItems: "flex-start",
      }}
    >
      <View style={styles.containerStores}>
        {tiendas?.map(({ id, imagen, name }) => {
          return (
            <TouchableOpacity
              key={id}
              style={styles.storeItemContainer}
              onPress={() => {
                sendDetail(id);
              }}
            >
              <View style={styles.containerImageStores}>
                <Image
                  style={{ width: "100%", height: "100%" }}
                  source={{ uri: imagen }}
                />
              </View>
              <View style={styles.containerTextStores}>
                <Text style={styles.nameStores}>{name}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </CardContainerGray>
  );
};

export default ItemSupplyStore;

const styles = StyleSheet.create({
  containerStores: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },

  storeItemContainer: {
    width: "50%",
    height: hp(27),
    padding: 10,
    position: "relative",
  },

  containerImageStores: {
    backgroundColor: "#EAEAEA",
    height: "60%",
    width: "100%",
    padding: 10,
  },

  containerTextStores: {
    height: "40%",
    width: "100%",
    flexWrap: "nowrap",
  },

  nameStores: {
    color: "rgba(0, 0, 0, 0.50)",
    marginTop: hp(1),
    fontFamily: "Poppins",
    fontSize: fz(1.5),
    textAlign: "center",
  },

  containerNoneItem: {
    height: hp(15),
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },

  itemNone: {
    fontFamily: "Poppins-medium",
    textAlign: "center",
    fontSize: fz(1.8),
  },
});
