import React, { useEffect, useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { registerStyles } from "../../styles/RegisterStyles";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import {useDispatch, useSelector} from "react-redux";
import {
    getDepartament,
    getMunicipalities,
    register, registerReferral,
} from "../../../store/actions/auth";
import { MaterialIcons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native";
import useMessage from "../../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";
import RegisterScreenOne from "../../auth/register/index-screen-one";
import RegisterScreenTwo from "../../auth/register/index-screen-two";
import RegisterScreenThree from "../../auth/register/index-screen-theree";
import RegisterScreenFour from "../../auth/register/index-screen-four";
import RegisterScreenFive from "../../auth/register/index-screen-five";
import RegisterScreenSix from "../../auth/register/index-screen-six";
import { NetworkConsumer, useIsConnected } from 'react-native-offline';
import {registerUser} from "../../../store/actions/offline";

const RegisterReferral = (props) => {
    const auth = useSelector((state) => state.auth.userData);
    const dispatch = useDispatch();
    const Toast = useToast();
    const { successMessage, dangerMessage } = useMessage(Toast);
    const [verificationId, setVerificationId] = useState("");
    const [codeReceived, setCodeReceived] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [stepsRegister, setStepRegister] = useState(1);
    const [sendMessage, setSendMessage] = useState(false);
    const [credentials, setCredentials] = useState({
        first_name: "",
        last_name: "",
        document: "",
        email: "",
        password: "",
        code: "+58",
        phone: "",
        department_id: "",
        municipalite_id: "",
        address: "",
        vereda: "",
        terms: false,
        codeOPT: "",
        user_id: "",
    });
    const registerValue = {
        first_name: credentials.first_name,
        last_name: credentials.last_name,
        document: credentials.document,
        email: credentials.email,
        code: credentials.code.slice(1),
        phone: credentials.phone,
        password: credentials.password,
        department_id: credentials.department_id,
        municipalite_id: credentials.municipalite_id,
        address: credentials.address,
        latitude: null,
        longitude: null,
        vereda: credentials.vereda,
        user_id: credentials.user_id,
    };
    const [errors, setErrors] = useState({
        first_name: "",
        last_name: "",
        document: "",
        email: "",
        password: "",
        code: "",
        phone: "",
        department_id: "",
        municipalite_id: "",
        address: "",
        vereda: "",
        terms: "",
        codeOPT: "",
        user_id: "",
    });

    useEffect(() => {
        dispatch(getDepartament()).then(() => {
            setIsLoading(false);
        });
    }, []);

    useEffect(() => {
        if (credentials.department_id !== "") {
            dispatch(getMunicipalities(credentials.department_id));
        }
    }, [credentials.department_id]);

    if (isLoading) {
        return <ProgressLoading />;
    }

    const handleRegister = async (setIsLoading) => {
        registerValue.user_id = auth.id;
        dispatch(registerUser(registerValue)).then((data) => {
            if (data.status === "Success") {
                successMessage("Registro exitoso");
                setStepRegister((prev) => prev + 1);
            } else {
                for (const [errorKey, error] of Object.entries(data.errors)) {
                    setErrors({ ...errors, [errorKey]: error[0] });
                    dangerMessage(error[0]);
                }
                setStepRegister(4);
            }
            setIsLoading(false);
        });
    };

    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            style={registerStyles.scrollView}
        >
            <View style={registerStyles.containerGlobal}>
                {stepsRegister === 1 && (
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                        style={registerStyles.containerIconBack}
                    >
                        <MaterialIcons name="keyboard-arrow-left" size={30} color="#000" />
                    </TouchableOpacity>
                )}
                {stepsRegister > 1 && stepsRegister !== 6 && (
                    <TouchableOpacity
                        onPress={() => {
                            setStepRegister((prev) => prev - 1);
                        }}
                        style={registerStyles.containerIconBack}
                    >
                        <MaterialIcons name="keyboard-arrow-left" size={30} color="#000" />
                    </TouchableOpacity>
                )}
                <Text style={registerStyles.titleRegister}>
                    {stepsRegister === 1 && "Completa tus datos"}
                    {stepsRegister === 2 && "Dirección"}
                    {stepsRegister === 3 && "Verificación"}
                    {stepsRegister === 4 && " Ingresa tu número telefónico"}
                </Text>
                <View style={{ alignItems: "center" }}>
                    {stepsRegister === 1 && (
                        <RegisterScreenOne
                            credentials={credentials}
                            setCredentials={setCredentials}
                            errors={errors}
                            setErrors={setErrors}
                            setStepRegister={setStepRegister}
                        />
                    )}
                    {stepsRegister === 2 && (
                        <RegisterScreenTwo
                            credentials={credentials}
                            setCredentials={setCredentials}
                            errors={errors}
                            setErrors={setErrors}
                            setStepRegister={setStepRegister}
                        />
                    )}
                    {stepsRegister === 3 && (
                        <RegisterScreenThree
                            setSendMessage={setSendMessage}
                            setStepRegister={setStepRegister}
                        />
                    )}
                    {stepsRegister === 4 && (
                        <RegisterScreenFour
                            credentials={credentials}
                            setCredentials={setCredentials}
                            errors={errors}
                            setErrors={setErrors}
                            setStepRegister={setStepRegister}
                            sendMessage={sendMessage}
                            setVerificationId={setVerificationId}
                            setCodeReceived={setCodeReceived}
                        />
                    )}
                    {stepsRegister === 5 && (
                        <RegisterScreenFive
                            credentials={credentials}
                            setCredentials={setCredentials}
                            errors={errors}
                            setErrors={setErrors}
                            setStepRegister={setStepRegister}
                            sendMessage={sendMessage}
                            verificationId={verificationId}
                            codeReceived={codeReceived}
                            handleRegister={handleRegister}
                        />
                    )}
                    {stepsRegister === 6 && (
                        <RegisterScreenSix credentials={credentials} />
                    )}
                </View>
            </View>
        </ScrollView>
    );
};

export default RegisterReferral;
