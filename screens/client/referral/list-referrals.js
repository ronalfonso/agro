import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import {ActivityIndicator, FlatList, TouchableOpacity, View} from "react-native";
import {Entypo} from "@expo/vector-icons";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import EmptyDateScreen from "../../../components/UI/screen-emty-data";
import {getMyReferrals} from "../../../store/actions/referrals";
import ListItemsReferral from "../../components/ListItemsReferral";


const ListReferrals = (props) => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const referrals = useSelector((state) => state.referrals.myReferrals);
    const { data, to, total } = referrals;
    const [pagination, setPagination] = useState(12);
    const [isLoadingData, setIsLoadingData] = useState(false);
    const [page, setPage] = useState(1);

    const _getReferrals = () => {
        setIsLoading(true);
        dispatch(getMyReferrals(pagination, page, setIsLoadingData)).then(() => {
            setIsLoading(false);
        });
    };

    useEffect(() => {
        props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity
                    onPress={() => {
                        props.navigation.navigate("RegisterReferral");
                    }}
                >
                    <Entypo name="plus" size={30} color="#000" />
                </TouchableOpacity>
            ),
        });
    }, [props.route.params]);

    useEffect(() => {
        _getReferrals();
    }, [dispatch]);

    useEffect(() => {
        const onFocusData = props.navigation.addListener("focus", () => {
            _getReferrals();
        });
        return onFocusData;
    }, [props.navigation]);

    const reloadDataFetch = async () => {
        const referidosFaltantes = total - pagination;
        if (to !== total) {
            setIsLoadingData(true);
            const newPagination =
                referidosFaltantes > 12
                    ? pagination + 12
                    : pagination + referidosFaltantes;
            setPagination(newPagination);
            dispatch(getMyReferrals(newPagination, page, setIsLoadingData));
        }
    };

    if (isLoading) {
        return <ProgressLoading />;
    }

    if (referrals && referrals.length === 0) {
        return (
            <EmptyDateScreen
                title={"Aún no tienes referidos"}
                subtitle={"Crea uno."}
            />
        );
    }

    return (
        <FlatList
            showsVerticalScrollIndicator={false}
            refreshing={isLoading}
            onRefresh={_getReferrals}
            numColumns={1}
            scrollEnabled={true}
            data={data}
            onEndReached={reloadDataFetch}
            onEndReachedThreshold={0.5}
            renderItem={(itemData) => (
                <ListItemsReferral
                    isTouchableOpacity={true}
                    handleSubmit={() => {
                        props.navigation.navigate("referral-detail", {
                            idReferral: itemData.item.id,
                            name: itemData.item.name,
                        });
                    }}
                    image={itemData.item.profile_photo_url}
                    name={itemData.item.name}
                    phone={itemData.item.phone}
                    email={itemData.item.email}
                />
            )}
            ListFooterComponent={
                isLoadingData && (
                    <View
                        style={{
                            flex: 1,
                            justifyContent: "center",
                            alignItems: "center",
                            paddingBottom: 50,
                            paddingTop: 20,
                        }}
                    >
                        <ActivityIndicator color={"#000"} size={35} />
                    </View>
                )
            }
        />
    );
}

export default ListReferrals;