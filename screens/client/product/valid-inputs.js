export const validInputsOne = (
  category,
  subCategory,
  product_id,
  size_id,
  price,
  setErrors,
  errors
) => {
  let isValid = true;

  if (price === "") {
    isValid = false;

    setErrors({ ...errors, price: "El precio es requerido" });
  }

  if (size_id === "") {
    isValid = false;

    setErrors({ ...errors, size_id: "La unidad de medida es requerida" });
  }

  if (product_id === "") {
    isValid = false;

    setErrors({ ...errors, product_id: "Seleccione un producto" });
  }

  if (subCategory === "") {
    isValid = false;

    setErrors({
      ...errors,
      subCategory: "La subcategoría del producto es requerida",
    });
  }

  if (category === "") {
    isValid = false;

    setErrors({
      ...errors,
      category: "La categoría del producto es requerida",
    });
  }

  return isValid;
};

export const validInputsTwo = (
  stock,
  department_id,
  municipalite_id,
  description,
  address,
  directionRegister,
  images,
  setErrors,
  errors
) => {
  let isValid = true;

  if (stock.length === 0) {
    isValid = false;

    setErrors({ ...errors, stock: "La cantidad es requerida" });
  }

  if (!directionRegister && department_id === "") {
    isValid = false;

    setErrors({ ...errors, department_id: "Seleccione un departamento" });
  }

  if (!directionRegister && municipalite_id === "") {
    isValid = false;

    setErrors({ ...errors, municipalite_id: "Seleccione un municipio" });
  }

  if (description.length === 0) {
    isValid = false;

    setErrors({ ...errors, description: "La descripción es requerida" });
  } else if (description.length < 8) {
    isValid = false;

    setErrors({
      ...errors,
      description: "La descripción debe tener al menos 8 caracteres",
    });
  }

  if (!directionRegister && address === "") {
    isValid = false;

    setErrors({ ...errors, address: "La dirección en requerida" });
  } else if (!directionRegister && address.length < 8) {
    isValid = false;

    setErrors({
      ...errors,
      address: "La dirección debe tener al menos 8 caracteres",
    });
  }

  if (images.length === 0) {
    isValid = false;

    setErrors({ ...errors, images: "Debe subir al menos una imagen" });
  }

  return isValid;
};
