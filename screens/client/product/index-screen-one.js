import React from "react";
import { View } from "react-native";
import SelectList from "../../../components/UI/custom-select-list";
import InputTextBorder from "../../../components/UI/custom-input-border";
import { useSelector } from "react-redux";

const CreateProductStepOne = ({
  creadentials,
  setCredentials,
  errors,
  setErrors,
}) => {
  const sizes = useSelector((state) => state.categories.sizesProduct);
  const productC = useSelector((state) => state.categories.categoriesProduct);
  const sub = useSelector((state) => state.categories.subCategoriesProduct);
  const product = useSelector((state) => state.categories.product);

  const renderMap = (value) =>
    value.message.map((item) => {
      return { key: item.id, value: item.name };
    });

  const validObject = (item) => Object.entries(item).length > 0;

  return (
    <View style={{ marginTop: 30 }}>
      <SelectList
        label={"Categoria"}
        error={errors.category}
        placeholder="Categoria"
        data={renderMap(productC)}
        setSelected={(val) => {
          setCredentials({ ...creadentials, category: val });
          setErrors({ ...errors, category: "" });
        }}
      />
      <SelectList
        label={"Subcategoria"}
        error={errors.subCategory}
        placeholder="Subcategoria"
        data={validObject(sub) ? renderMap(sub) : []}
        setSelected={(val) => {
          setCredentials({ ...creadentials, subCategory: val });
          setErrors({ ...errors, subCategory: "" });
        }}
      />
      <SelectList
        label={"Producto"}
        error={errors.product_id}
        placeholder="Producto"
        data={validObject(product) ? renderMap(product) : []}
        setSelected={(val) => {
          setCredentials({ ...creadentials, product_id: val });
          setErrors({ ...errors, product_id: "" });
        }}
      />
      <SelectList
        label={"Unidad de medida"}
        error={errors.size_id}
        placeholder="Unidad de medida"
        data={validObject(sizes) ? renderMap(sizes) : []}
        setSelected={(val) => {
          setCredentials({ ...creadentials, size_id: val });
          setErrors({ ...errors, size_id: "" });
        }}
      />
      <InputTextBorder
        name="price"
        value={creadentials.price}
        setCredentials={setCredentials}
        creadentials={creadentials}
        label="$ Precio"
        error={errors.price}
        setErrors={setErrors}
        errorsItem={errors}
        password={false}
        keyboardType="number-pad"
        footerText=""
      />
    </View>
  );
};

export default CreateProductStepOne;
