import React, { useEffect, useState } from "react";
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Image,
  Linking,
} from "react-native";
import { useDispatch } from "react-redux";
import { getProductDetail } from "../../../store/actions/products";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import {
  Entypo,
  Ionicons,
  AntDesign,
  MaterialIcons,
  FontAwesome,
  Feather,
} from "@expo/vector-icons";
import { productStyles } from "../../styles/ListProductStyles";
import ImageSwiperList from "../../components/ImageSwiperList";
import { percentageToDPHeight } from "../../../utils/CalculatePD";
import LargeButton from "../../../components/UI/custom-bottom";

const ProductDetailScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [productData, setProductData] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    props.navigation.setOptions({
      title: `Producto ${props.route.params.name}`,
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("create-product");
          }}
        >
          <Entypo name="plus" size={30} color="#000" />
        </TouchableOpacity>
      ),
    });
    dispatch(getProductDetail(props.route.params.idProduct)).then(
      ({ data }) => {
        setIsLoading(false);
        setProductData(data);
      }
    );
  }, [props.route.params]);

  if (isLoading) {
    return <ProgressLoading />;
  }

  return (
    <ScrollView
      style={productStyles.scrollView}
      showsVerticalScrollIndicator={false}
    >
      <View style={productStyles.containerHeader}>
        <Text style={productStyles.textHeader}>
          Vista previa de tu producto
        </Text>
      </View>
      <View style={productStyles.containerBody}>
        <Image
          source={{ uri: productData.image }}
          style={productStyles.imageCover}
        />

        <ImageSwiperList
          imagenes={productData.imagenes}
          name={productData.name}
        />

        <Text
          style={{
            ...productStyles.textRating,
            alignSelf: "flex-start",
            marginLeft: 20,
          }}
        >
          {productData.category_name}/{productData.subcategory_name}
        </Text>

        <View style={productStyles.containerRatings}>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            {[1, 2, 3, 4, 5].map((index) => {
              return (
                <Entypo
                  key={index}
                  name="star-outlined"
                  size={20}
                  color="#FED500"
                />
              );
            })}
            <Text style={productStyles.textRating}>3 calificaciones</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <TouchableOpacity style={productStyles.buttonShere}>
              <Ionicons name="heart-circle-outline" size={30} color="#DE8657" />
            </TouchableOpacity>
            <TouchableOpacity style={productStyles.buttonShere}>
              <AntDesign name="sharealt" size={30} color="black" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ alignSelf: "flex-start", paddingHorizontal: 20 }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 10,
            }}
          >
            <MaterialIcons
              name="local-fire-department"
              size={30}
              color="#DE8657"
            />
            <Text style={productStyles.textDesc}>PRODUCTO DESTACADO</Text>
          </View>
          <Text style={productStyles.textDescription}>
            {productData.description}
          </Text>
          <View
            style={{
              flexDirection: "row",
              marginVertical: percentageToDPHeight(40),
            }}
          >
            <Text style={{ ...productStyles.textRebate, color: "#DE8657" }}>
              $27.95 (50% Menos)
            </Text>
            <Text
              style={{
                ...productStyles.textRebate,
                color: "#000",
                marginLeft: 15,
              }}
            >
              $59.99
            </Text>
          </View>
          <Text style={productStyles.textStock}>
            Unidades: {productData.stock}
          </Text>
          <LargeButton
            isLoading={false}
            handleSubmit={() => Linking.openURL(`tel:${productData.phone}`)}
            text={"Contactar"}
            disable={false}
          />

          <View
            style={{
              flexDirection: "row",
              marginTop: 50,
              alignItems: "center",
            }}
          >
            <FontAwesome name="eye" size={20} color="black" />
            <Text style={productStyles.textUserView}>
              150 Usuarios estan viendo este producto
            </Text>
          </View>

          <View style={{ flexDirection: "row", marginVertical: 20 }}>
            <Feather name="message-square" size={24} color="black" />
            <View style={{ flexDirection: "column" }}>
              <Text style={productStyles.textQuestion}>Tienes preguntas?</Text>
              <Text style={productStyles.textClient}>
                Chatea con el servicio al cliente
              </Text>
            </View>
          </View>

          <TouchableOpacity style={productStyles.bottomTransport}>
            <Text style={productStyles.textTransport}>
              VER OFERTAS DE TRANSPORTE
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default ProductDetailScreen;
