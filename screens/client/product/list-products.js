import React, { useEffect, useState } from "react";
import { Entypo } from "@expo/vector-icons";
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { getMyProducts } from "../../../store/actions/products";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import ListItemsProduct from "../../components/ListItemsProduct";
import EmptyDateScreen from "../../../components/UI/screen-emty-data";

const ListProducts = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const product = useSelector((state) => state.products.myProducts);
  const { data, to, total } = product;
  const [pagination, setPagination] = useState(12);
  const [isLoadingData, setIsLoadingData] = useState(false);
  const [page, setPage] = useState(1);

  useEffect(() => {
    props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("create-product");
          }}
        >
          <Entypo name="plus" size={30} color="#000" />
        </TouchableOpacity>
      ),
    });
  }, [props.route.params]);

  useEffect(() => {
    getProducts();
  }, [dispatch]);

  const getProducts = () => {
    setIsLoading(true);
    dispatch(getMyProducts(pagination, page, setIsLoadingData)).then(() => {
      setIsLoading(false);
    });
  };

  useEffect(() => {
    const onFocusData = props.navigation.addListener("focus", () => {
      getProducts();
    });
    return onFocusData;
  }, [props.navigation]);

  const reloadDataFetch = async () => {
    const productosFaltantes = total - pagination;
    if (to !== total) {
      setIsLoadingData(true);
      const newPagination =
        productosFaltantes > 12
          ? pagination + 12
          : pagination + productosFaltantes;
      setPagination(newPagination);
      dispatch(getMyProducts(newPagination, page, setIsLoadingData));
    }
  };

  if (isLoading) {
    return <ProgressLoading />;
  }

  if (product && product.length === 0) {
    return (
      <EmptyDateScreen
        title={"Aún no tienes publicaciones"}
        subtitle={"Crea una y empieza a vender cuando quieras."}
      />
    );
  }

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      refreshing={isLoading}
      onRefresh={getProducts}
      numColumns={1}
      scrollEnabled={true}
      data={data}
      onEndReached={reloadDataFetch}
      onEndReachedThreshold={0.5}
      renderItem={(itemData) => (
        <ListItemsProduct
          isTouchableOpacity={true}
          handleSubmit={() => {
            props.navigation.navigate("product-detail", {
              idProduct: itemData.item.id,
              name: itemData.item.name,
            });
          }}
          image={itemData.item.image}
          name={itemData.item.name}
          price={itemData.item.price}
          description={itemData.item.description}
          category_name={itemData.item.category_name}
          subcategory_name={itemData.item.subcategory_name}
        />
      )}
      ListFooterComponent={
        isLoadingData && (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              paddingBottom: 50,
              paddingTop: 20,
            }}
          >
            <ActivityIndicator color={"#000"} size={35} />
          </View>
        )
      }
    />
  );
};

export default ListProducts;
