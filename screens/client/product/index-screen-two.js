import React from "react";
import { Text, View } from "react-native";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { createProductStyles } from "../../styles/CreateProductStyles";
import SelectList from "../../../components/UI/custom-select-list";
import InputTextBorder from "../../../components/UI/custom-input-border";
import { useSelector } from "react-redux";
import ImagesProducts from "../../components/ImagesProducts";

const CreateProductStepTwo = ({
  creadentials,
  setCredentials,
  errors,
  setErrors,
}) => {
  const departament = useSelector((state) => state.auth.departament);
  const municipalities = useSelector((state) => state.auth.municipalities);

  const renderMap = (value) =>
    value.data.map((item) => {
      return { key: item.id, value: item.name };
    });

  const validObject = (item) => Object.entries(item).length > 0;

  return (
    <>
      <View style={createProductStyles.containerCheck}>
        <BouncyCheckbox
          size={25}
          fillColor="#06C167"
          unfillColor="#FFFFFF"
          isChecked={creadentials.directionRegister}
          onPress={(isChecked) => {
            setCredentials({ ...creadentials, directionRegister: isChecked });
          }}
        />
        <Text style={createProductStyles.labelCheck}>
          usar dirección del registro.
        </Text>
      </View>
      <View style={{ marginTop: 30 }}>
        {!creadentials.directionRegister && (
          <>
            <SelectList
              label={"Departamento"}
              error={errors.department_id}
              placeholder="Departamento"
              data={validObject(departament) ? renderMap(departament) : []}
              setSelected={(val) => {
                setCredentials({ ...creadentials, department_id: val });
                setErrors({ ...errors, department_id: "" });
              }}
            />
            <SelectList
              label={"Municipio"}
              error={errors.municipalite_id}
              placeholder="Municipio"
              data={
                validObject(municipalities) ? renderMap(municipalities) : []
              }
              setSelected={(val) => {
                setCredentials({ ...creadentials, municipalite_id: val });
                setErrors({ ...errors, municipalite_id: "" });
              }}
            />
            <InputTextBorder
              name="address"
              value={creadentials.address}
              setCredentials={setCredentials}
              creadentials={creadentials}
              label="Dirección"
              error={errors.address}
              setErrors={setErrors}
              errorsItem={errors}
              password={false}
              keyboardType="default"
              footerText=""
            />
          </>
        )}
        <InputTextBorder
          name="stock"
          value={creadentials.stock}
          setCredentials={setCredentials}
          creadentials={creadentials}
          label="Cantidad"
          error={errors.stock}
          setErrors={setErrors}
          errorsItem={errors}
          password={false}
          keyboardType="number-pad"
          footerText=""
        />

        <InputTextBorder
          name="description"
          value={creadentials.description}
          setCredentials={setCredentials}
          creadentials={creadentials}
          label="Descripción"
          error={errors.description}
          setErrors={setErrors}
          errorsItem={errors}
          password={false}
          keyboardType="default"
          footerText=""
        />

        <ImagesProducts
          creadentials={creadentials}
          setCredentials={setCredentials}
          setErrors={setErrors}
          errors={errors}
        />
      </View>
    </>
  );
};

export default CreateProductStepTwo;
