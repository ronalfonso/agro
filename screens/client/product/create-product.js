import React, { useEffect, useState } from "react";
import { ScrollView, Text, View } from "react-native";
import { createProductStyles } from "../../styles/CreateProductStyles";
import LargeButton from "../../../components/UI/custom-bottom";
import CreateProductStepOne from "./index-screen-one";
import CreateProductStepTwo from "./index-screen-two";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategoriesProduct,
  getProduct,
  getSubCategoriesProduct,
  getUnidadProduct,
} from "../../../store/actions/categories";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import { validInputsOne, validInputsTwo } from "./valid-inputs";
import { getDepartament } from "../../../store/actions/auth";
import {
  createImageProduct,
  createProductUser,
} from "../../../store/actions/products";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../../hooks/useMessage";

const CreateProductUser = (props) => {
  const Toast = useToast();
  const { successMessage, dangerMessage } = useMessage(Toast);
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.auth.userData);
  const [step, setStep] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingPage, setIsLoadingPage] = useState(false);
  const [creadentials, setCredentials] = useState({
    user_id: userData.id,
    product_id: "",
    size_id: "",
    price: "",
    stock: "",
    description: "",
    user_address: "",
    department_id: "",
    municipalite_id: "",
    address: "",
    category: "",
    subCategory: "",
    directionRegister: true,
    images: [],
  });
  const [errors, setErrors] = useState({
    product_id: "",
    size_id: "",
    price: "",
    stock: "",
    description: "",
    user_address: "",
    department_id: "",
    municipalite_id: "",
    address: "",
    category: "",
    subCategory: "",
    images: "",
  });

  useEffect(() => {
    setIsLoadingPage(true);
    dispatch(getCategoriesProduct()).then(() => {
      dispatch(getDepartament()).then(() => {
        dispatch(getUnidadProduct()).then(() => {
          setIsLoadingPage(false);
        });
      });
    });
  }, []);

  useEffect(() => {
    if (creadentials.category !== "") {
      dispatch(getSubCategoriesProduct(creadentials.category));
      setCredentials({ ...creadentials, subCategory: "", product_id: "" });
    }
  }, [creadentials.category]);

  useEffect(() => {
    if (creadentials.subCategory !== "") {
      dispatch(getProduct(creadentials.subCategory));
      setCredentials({ ...creadentials, product_id: "" });
    }
  }, [creadentials.subCategory]);

  if (isLoadingPage) {
    return <ProgressLoading />;
  }

  const handleSubmit = () => {
    const {
      user_id,
      category,
      subCategory,
      product_id,
      size_id,
      price,
      stock,
      department_id,
      municipalite_id,
      description,
      address,
      directionRegister,
      images,
    } = creadentials;
    let isValid;

    switch (step) {
      case 1:
        isValid = validInputsOne(
          category,
          subCategory,
          product_id,
          size_id,
          price,
          setErrors,
          errors
        );
        break;
      case 2:
        isValid = validInputsTwo(
          stock,
          department_id,
          municipalite_id,
          description,
          address,
          directionRegister,
          images,
          setErrors,
          errors
        );
        break;
      default:
        return;
    }

    if (isValid && step === 1) {
      setStep((prev) => prev + 1);
    } else if (isValid && step === 2) {
      setIsLoading(true);
      dispatch(
        createProductUser(
          user_id,
          product_id,
          size_id,
          price,
          stock,
          description,
          directionRegister,
          department_id,
          municipalite_id,
          address
        )
      ).then((data) => {
        if (data.status === "Success") {
          dispatch(createImageProduct(data.data.id, images)).then(() => {
            setIsLoading(false);
            successMessage("¡Su producto ha sido publicado con éxito!");
            setStep((prev) => prev - 1);
            setCredentials({
              ...creadentials,
              user_id: userData.id,
              product_id: "",
              size_id: "",
              price: "",
              stock: "",
              description: "",
              user_address: "",
              department_id: "",
              municipalite_id: "",
              address: "",
              category: "",
              subCategory: "",
              directionRegister: true,
              images: [],
            });
            setTimeout(() => {
              props.navigation.navigate("list-products");
            }, 3000);
          });
        } else {
          setIsLoading(false);
          dangerMessage(data.message);
        }
      });
    }
  };

  return (
    <ScrollView
      style={createProductStyles.scrollView}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ alignItems: "center", marginBottom: 40 }}>
        <View style={createProductStyles.containerHeader}>
          <Text style={createProductStyles.textTitle}>
            Informacion del producto
          </Text>
        </View>

        {step === 1 && (
          <CreateProductStepOne
            creadentials={creadentials}
            setCredentials={setCredentials}
            errors={errors}
            setErrors={setErrors}
          />
        )}

        {step === 2 && (
          <CreateProductStepTwo
            creadentials={creadentials}
            setCredentials={setCredentials}
            errors={errors}
            setErrors={setErrors}
          />
        )}

        <LargeButton
          isLoading={isLoading}
          handleSubmit={handleSubmit}
          text={step === 1 ? "Continuar" : "Publicar producto"}
          disable={isLoading}
        />
      </View>
    </ScrollView>
  );
};

export default CreateProductUser;
