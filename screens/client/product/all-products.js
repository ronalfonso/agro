import React, { useEffect, useState } from "react";
import ListItemsProduct from "../../components/ListItemsProduct";
import { getProductsAuth } from "../../../store/actions/products";
import { useDispatch, useSelector } from "react-redux";
import { FlatList, Text, View, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { productStyles } from "../../styles/ViewProductStyles";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import ModalFilter from "../../components/ModalFilter";
import { getCategoriesProduct } from "../../../store/actions/categories";
import { getDepartament } from "../../../store/actions/auth";

const ProductsAllScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [filterIsLoading, setFilterIsLoading] = useState(false);
  const [modalFilter, setModalFilter] = useState(false);
  const product = useSelector((state) => state.products.products);
  const categoriFilter = useSelector((state) => state.filters.categoriaFilter);
  const { data, to, total } = product;

  useEffect(() => {
    setIsLoading(true);
    dispatch(getProductsAuth(categoriFilter.id)).then(() => {
      setIsLoading(false);
    });
    dispatch(getCategoriesProduct());
    dispatch(getDepartament());
  }, []);

  if (isLoading) {
    return <ProgressLoading />;
  }

  return (
    <>
      <View style={productStyles.containerHeader}>
        <Text style={productStyles.textResult}>{to} resultados</Text>
        <TouchableOpacity
          style={productStyles.containerHeader}
          activeOpacity={0.5}
          onPress={() => {
            setModalFilter(!modalFilter);
          }}
        >
          <Text style={{ ...productStyles.textResult, color: "#06C167" }}>
            Filtrar (0)
          </Text>
          <MaterialIcons name="keyboard-arrow-down" size={20} color="#06C167" />
        </TouchableOpacity>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        numColumns={1}
        scrollEnabled={true}
        data={product.data}
        onEndReachedThreshold={0.5}
        renderItem={(itemData) => (
          <ListItemsProduct
            isTouchableOpacity={false}
            id={itemData.item.id}
            image={itemData.item.image}
            name={itemData.item.name}
            price={itemData.item.price}
            description={itemData.item.description}
            category_name={itemData.item.category_name}
            subcategory_name={itemData.item.subcategory_name}
          />
        )}
      />
      <ModalFilter
        modalVisible={modalFilter}
        setModalVisible={setModalFilter}
        isLoadingFilters={filterIsLoading}
      />
    </>
  );
};

export default ProductsAllScreen;
