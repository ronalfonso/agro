import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  ActivityIndicator,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { homeStyles } from "../../styles/HomeStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";
import {
  addFavorities,
  deleteFavoritiesProduct,
} from "../../../store/actions/favorities";
import { getProductsAuth, PRODUCTS } from "../../../store/actions/products";
import * as Progress from "react-native-progress";
import ModalFilter from "../../components/ModalFilter";
import { getCategoriesProduct } from "../../../store/actions/categories";
import { getDepartament, getMunicipalities } from "../../../store/actions/auth";
import { FavoritiesStyles } from "../../styles/FavoritiesStyles";
import { checkInternetConnection } from "../../../store/actions/noConnection";
import NoConnection from "../../components/NoConnection";

const AllProductsScreen = (props) => {
  const dispatch = useDispatch();
  const categoriFilter = useSelector((state) => state.filters.categoriaFilter);
  const orderFilter = useSelector((state) => state.filters.orderFilter);
  const priceFilter = useSelector((state) => state.filters.priceFilter);
  const locationMuniFilter = useSelector(
    (state) => state.filters.locationMuniFilter
  );
  const locationDepFilter = useSelector(
    (state) => state.filters.locationDepFilter
  );
  const product = useSelector((state) => state.products.products);
  const connection = useSelector((state) => state.noConnection.noConnection);

  const { data, to, total } = product;
  const [isLoading, setIsLoading] = useState(false);
  const [filtersCount, setFiltersCount] = useState(0);
  const [favorites, setFavorites] = useState([]);
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState(12);
  const [isLoadingData, setIsLoadingData] = useState(false);

  const [modalVisible, setModalVisible] = useState(false);
  const [isLoadingFilters, setIsLoadingFilters] = useState(false);

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    let count = 0;
    if (
      categoriFilter.id !== "" ||
      categoriFilter.value !== "" ||
      categoriFilter.label !== ""
    ) {
      count++;
    }
    if (
      orderFilter.id !== "" ||
      orderFilter.value !== "" ||
      orderFilter.label !== ""
    ) {
      count++;
    }
    if (priceFilter.maxPrice !== "" || priceFilter.minPrice !== "") {
      count++;
    }
    if (
      locationMuniFilter.id !== "" ||
      locationMuniFilter.value !== "" ||
      locationMuniFilter.label !== ""
    ) {
      count++;
    }
    if (
      locationDepFilter.id !== "" ||
      locationDepFilter.value !== "" ||
      locationDepFilter.label !== ""
    ) {
      count++;
    }
    setFiltersCount(count);
  }, [
    categoriFilter,
    orderFilter,
    priceFilter,
    locationMuniFilter,
    locationDepFilter,
  ]);

  const fetchProducts = async () => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const isConnected = await checkInternetConnection();
        if (isConnected) {
          dispatch(getCategoriesProduct());
          dispatch(getDepartament());
          dispatch(
            getProductsAuth(
              setIsLoading,
              setIsLoadingFilters,
              categoriFilter.id,
              priceFilter.maxPrice,
              priceFilter.minPrice,
              locationDepFilter.id,
              locationMuniFilter.id,
              page,
              pagination,
              setIsLoadingData
            )
          );
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  };

  useEffect(() => {
    if (locationDepFilter.id !== "") {
      dispatch(getMunicipalities(locationDepFilter.id));
    }
  }, [locationDepFilter.id]);

  useEffect(() => {
    setIsLoadingFilters(true);
    dispatch(
      getProductsAuth(
        setIsLoading,
        setIsLoadingFilters,
        categoriFilter.id,
        priceFilter.maxPrice,
        priceFilter.minPrice,
        locationDepFilter.id,
        locationMuniFilter.id,
        page,
        pagination,
        setIsLoadingData
      )
    );
  }, [
    categoriFilter.id,
    priceFilter.maxPrice,
    priceFilter.minPrice,
    locationDepFilter.id,
    locationMuniFilter.id,
  ]);

  const orderProductPrice = async () => {
    let newProducts = [];
    if (Array.isArray(data)) {
      newProducts = [...data];
      if (orderFilter.label === "Mayor precio") {
        newProducts.sort((a, b) => b.price - a.price);
        dispatch({
          type: PRODUCTS,
          payload: {
            data: newProducts,
            to: to,
            total: total,
          },
        });
      } else if (orderFilter.label === "Menor precio") {
        newProducts.sort((a, b) => a.price - b.price);
        dispatch({
          type: PRODUCTS,
          payload: {
            data: newProducts,
            to: to,
            total: total,
          },
        });
      }
    }
    setTimeout(() => {
      setIsLoadingFilters(false);
    }, 1500);
  };

  useEffect(() => {
    if (orderFilter.id !== "") {
      setIsLoadingFilters(true);
      orderProductPrice();
    } else if (orderFilter.label === "") {
      dispatch(
        getProductsAuth(
          setIsLoading,
          setIsLoadingFilters,
          categoriFilter.id,
          priceFilter.maxPrice,
          priceFilter.minPrice,
          locationDepFilter.id,
          locationMuniFilter.id,
          page,
          pagination,
          setIsLoadingData
        )
      );
    }
  }, [orderFilter.id]);

  const toggleFavorite = (id, favorite_id) => {
    const index = favorites.indexOf(id);
    if (index !== -1) {
      const newFavorites = [...favorites];
      newFavorites.splice(index, 1);
      setFavorites(newFavorites);
      dispatch(deleteFavoritiesProduct(favorite_id));
    } else {
      const newFavorites = [...favorites, id];
      setFavorites(newFavorites);
      dispatch(addFavorities(id));
    }
  };

  useEffect(() => {
    if (Array.isArray(product) && product?.length !== 0) {
      const filterFavoritedIds = () => {
        const filteredData = product.filter((element) => {
          return element.favorite_id !== null;
        });
        const filteredIds = filteredData.map((element) => element.id);
        setFavorites(filteredIds);
      };
      filterFavoritedIds();
    }
  }, [product]);

  useEffect(() => {
    const onFocusData = props.navigation.addListener("focus", () => {
      fetchProducts();
    });
    return onFocusData;
  }, [props.navigation]);

  const reloadDataFetch = async () => {
    const productosFaltantes = total - pagination;
    if (to !== total) {
      setIsLoadingData(true);
      const newPagination =
        productosFaltantes > 12
          ? pagination + 12
          : pagination + productosFaltantes;
      setPagination(newPagination);
      dispatch(
        getProductsAuth(
          setIsLoading,
          setIsLoadingFilters,
          categoriFilter.id,
          priceFilter.maxPrice,
          priceFilter.minPrice,
          locationDepFilter.id,
          locationMuniFilter.id,
          page,
          newPagination,
          setIsLoadingData
        )
      );
    } else {
      return false;
    }
  };

  if (connection === false) {
    return <NoConnection functionLoading={fetchProducts} />;
  }

  const DataItems = ({
    name,
    image,
    price,
    id,
    favorite_id,
    category_name,
    subcategory_name,
    description,
  }) => {
    const isFavorite = favorites.includes(id);

    return (
      <View style={styles.containerContentItems}>
        <View
          style={{
            width: wp(100),
            borderBottomWidth: 1,
            borderBottomColor: "rgba(33,33,33,0.3)",
            padding: 7,
          }}
        >
          <TouchableOpacity
            style={styles.containerIcon}
            onPress={async () => {
              toggleFavorite(id, favorite_id);
            }}
          >
            <Ionicons
              name={isFavorite ? "heart" : "heart-outline"}
              size={25}
              color="#06C167"
            />
          </TouchableOpacity>
        </View>
        <View style={{ padding: 20 }}>
          <TouchableOpacity
            style={{ flexDirection: "row", justifyContent: "space-between" }}
            onPress={() => {
              props.navigation.navigate("product-detail", {
                idProduct: id,
              });
            }}
          >
            <View style={{ width: wp(25), marginRight: 10 }}>
              <Image
                style={{ width: wp(25), height: hp(12), borderRadius: 5 }}
                source={{ uri: image }}
              />
            </View>
            <View style={{ width: wp(50) }}>
              <Text style={styles.titleContent}>{description}</Text>
              <Text style={styles.textPrice}>$ {price}</Text>
              <Text
                style={{
                  ...productStyles.titleContent,
                  fontSize: fz(1.8),
                }}
              >
                {category_name}/ {subcategory_name}
              </Text>
              <Text
                style={{ ...productStyles.titleContent, fontSize: fz(1.8) }}
              >
                {description}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  const RenderFooter = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          paddingBottom: 50,
          paddingTop: 20,
        }}
      >
        <ActivityIndicator color={"#000"} size={35} />
      </View>
    );
  };

  if (isLoading) {
    return (
      <View
        style={{ ...homeStyles.containerLoading, top: hp(-5), width: wp(100) }}
      >
        <Progress.CircleSnail
          color={["#06C167", "#000"]}
          size={70}
          strokeCap={"square"}
        />
      </View>
    );
  }

  return (
    <>
      <View
        style={{
          height: hp(5),
          width: wp(100),
          paddingHorizontal: 20,
          backgroundColor: "#fff",
          elevation: 15,
          justifyContent: "space-between",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <Text style={{ color: "#ccc", fontFamily: "Poppins-medium" }}>
          {total} resultados
        </Text>
        <TouchableOpacity
          style={{ flexDirection: "row", alignItems: "center" }}
          onPress={() => setModalVisible(true)}
          activeOpacity={0.3}
        >
          <Text style={{ color: "#000000", fontFamily: "Poppins-medium" }}>
            Filtrar {filtersCount !== 0 && `(${filtersCount})`}
          </Text>
          <MaterialIcons name="keyboard-arrow-down" size={24} color="#000" />
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        {!isLoading &&
        !isLoadingData &&
        filtersCount !== 0 &&
        data?.length === 0 ? (
          <View style={FavoritiesStyles.containerEnableBackground}>
            <Text style={FavoritiesStyles.titleNotEnable}>
              No encontramos publicaciones
            </Text>
            <Text style={FavoritiesStyles.textNotEnable}>
              Revisa que la palabra este bien escrita
            </Text>
          </View>
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            key={"#"}
            numColumns={1}
            data={data}
            scrollEnabled={true}
            keyExtractor={(item) => item.id.toString()}
            onEndReachedThreshold={0.5}
            onEndReached={reloadDataFetch}
            renderItem={(itemData) => (
              <DataItems
                image={itemData.item.image}
                price={itemData.item.price}
                name={itemData.item.name}
                id={itemData.item.id}
                favorite_id={itemData.item.favorite_id}
                category_name={itemData.item.category_name}
                subcategory_name={itemData.item.subcategory_name}
                description={itemData.item.description}
              />
            )}
            ListFooterComponent={!!isLoadingData && <RenderFooter />}
          />
        )}
      </View>
      <ModalFilter
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        isLoadingFilters={isLoadingFilters}
      />
    </>
  );
};

export default AllProductsScreen;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: wp(100),
    paddingBottom: 50,
  },

  containerContentItems: {
    marginBottom: hp(2),
    marginTop: 20,
    backgroundColor: "#fff",
    height: hp(23),
    width: wp(90),
    borderRadius: 10,
    elevation: 7,
  },

  background: {
    padding: 10,
    backgroundColor: "#EAEAEA",
    justifyContent: "center",
    alignItems: "center",
  },

  containerIcon: {
    height: hp(3.5),
    width: wp(8),
    borderRadius: 100,
    left: wp(80),
  },

  title: {
    color: "#000",
    fontSize: fz(2.5),
    fontFamily: "Poppins-medium",
  },

  textPrice: {
    fontFamily: "Poppins-medium",
    fontSize: fz(2.2),
  },

  titleContent: {
    width: wp(50),
    flexWrap: "wrap",
    fontSize: fz(1.8),
    marginBottom: 5,
    fontFamily: "Poppins",
  },
});
