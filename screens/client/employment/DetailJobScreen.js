import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Linking,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { Feather } from "@expo/vector-icons";
import { getDetailJobs } from "../../../store/actions/employeers";
import { useDispatch, useSelector } from "react-redux";
import { homeStyles } from "../../styles/HomeStyles";

const DetailJobScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const idDetail = props.route.params.idDetail;
  const listDetailJobs = useSelector(
    (state) => state.employeers.listDetailJobs
  );
  console.log(listDetailJobs);
  const fetchDetailJob = async () => {
    setIsLoading(true);
    try {
      await dispatch(getDetailJobs(idDetail));
    } catch (error) {
      console.log(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchDetailJob();
  }, []);

  if (isLoading) {
    return (
      <View style={{ ...homeStyles.containerLoading }}>
        <ActivityIndicator size="large" color="#06C167" />
      </View>
    );
  }

  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={styles.container}>
        <View style={styles.containerSearch}>
          <Feather
            name="search"
            size={24}
            style={{
              color: "rgba(33,33,33,0.5)",
              position: "absolute",
              zIndex: 10,
              bottom: hp(1.7),
              left: wp(8),
            }}
          />
          <TextInput style={styles.input} placeholder="Administrador" />
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "rgba(33,33,33,0.5)",
          }}
        />
        <View style={styles.containterContent}>
          <View style={{ alignItems: "center" }}>
            <Text style={styles.titleJob}>{listDetailJobs?.data?.name}</Text>
            <Text style={styles.message}>Importante empresa del sector</Text>
          </View>

          <View style={styles.containerSelected}></View>
          <View style={styles.newOffert}>
            <View>
              <Text
                style={{
                  fontFamily: "Poppins-bold",
                }}
              >
                Nuevas ofertas de empleo
              </Text>
            </View>
            <View style={{ marginLeft: wp(22) }}>
              <TouchableOpacity>
                <Text
                  style={{
                    fontFamily: "Poppins",
                    color: "#5385E4",
                  }}
                >
                  Avisame
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "rgba(33,33,33,0.5)",
            }}
          />
          <View style={styles.content}>
            <Text style={styles.titleDescription}>
              Descripcion de la oferta
            </Text>
            <Text style={styles.textDescription}>
              {listDetailJobs?.data?.description}
            </Text>
            <View style={{ marginTop: 20, alignItems: "center" }}>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Empleador: {listDetailJobs?.data?.user_name}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Beneficios: {listDetailJobs?.data?.benefits}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Departamento: {listDetailJobs?.data?.department_name}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Municipio: {listDetailJobs?.data?.municipality_name}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Honorarios mensuales: {listDetailJobs?.data?.salary_from}
              </Text>
              <Text
                style={{
                  fontFamily: "Poppins-medium",
                  fontSize: fz(1.8),
                  alignSelf: "flex-start",
                }}
              >
                Estado:{" "}
                {listDetailJobs?.data?.status === 1
                  ? "Activa"
                  : "No disponible"}
              </Text>

              <TouchableOpacity
                style={styles.customButton}
                onPress={() => {
                  Linking.openURL(`tel:${listDetailJobs?.data?.phone}`);
                }}
              >
                <Text
                  style={{
                    fontFamily: "Poppins-bold",
                    color: "#fff",
                    fontSize: fz(2),
                  }}
                >
                  Postularme
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default DetailJobScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    width: wp(100),
    marginTop: 20,
  },
  containerSearch: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
  },
  input: {
    height: 50,
    paddingLeft: wp(10),
    backgroundColor: "#F4F4F4",
    width: wp(90),
    fontFamily: "Poppins",
    borderRadius: 50,
  },
  containterContent: {
    paddingTop: 20,
  },
  titleJob: {
    fontFamily: "Poppins-medium",
    width: wp(60),
    textAlign: "center",
    fontSize: fz(2.5),
  },
  message: {
    fontFamily: "Poppins",
    color: "#06C167",
  },
  newOffert: {
    padding: 20,
    alignSelf: "flex-start",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  containerSelected: {},
  content: {
    alignSelf: "flex-start",
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  titleDescription: {
    fontFamily: "Poppins-medium",
    fontSize: fz(2),
  },
  textDescription: {
    fontFamily: "Poppins",
    fontSize: fz(2),
  },
  customButton: {
    width: wp(90),
    height: hp(7),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#06C167",
    marginTop: 20,
    borderRadius: 6,
  },
});
