import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { ListJobsStyles } from "../../styles/ListJobsStyles";
import { getListJobs } from "../../../store/actions/employeers";
import { useDispatch, useSelector } from "react-redux";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import EmptyDateScreen from "../../../components/UI/screen-emty-data";
import TextInputSearch from "../../../components/UI/custom-input-search";
import { SimpleLineIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const ListJobsScreen = (props) => {
  const navigation = useNavigation();
  const { idCategori } = props.route.params;
  const dispatch = useDispatch();
  const listJobs = useSelector((state) => state.employeers.listJobs);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      setIsLoading(true);
      dispatch(getListJobs(idCategori)).then(() => {
        setIsLoading(false);
      });
    });
    return onFocusData;
  }, [navigation]);

  useEffect(() => {
    const filteredArray = listJobs?.data?.filter((item) => {
      return item.name.toLowerCase().includes(searchTerm.toLowerCase());
    });
    setFilteredData(filteredArray);
  }, [searchTerm]);

  if (isLoading) {
    return <ProgressLoading />;
  }

  if (listJobs?.data?.length === 0) {
    return (
      <EmptyDateScreen
        title={"No se encontraron resultados"}
        subtitle={"No hay empleos publicados en esta categoría"}
      />
    );
  }

  const DataItems = ({
    benefits,
    id,
    name,
    address,
    description,
    salary_from,
  }) => {
    return (
      <TouchableOpacity
        style={ListJobsStyles.containerOffers}
        onPress={() => {
          props.navigation.navigate("DetailJobBoard", {
            idDetail: id,
          });
        }}
      >
        <Text style={ListJobsStyles.textOtherOffers}>{name}</Text>
        <Text style={ListJobsStyles.textOtherOffers}>
          <Text style={ListJobsStyles.textBlackOffers}>Descripcion:</Text>{" "}
          {description}
        </Text>
        <Text style={ListJobsStyles.textOtherOffers}>
          <Text style={ListJobsStyles.textBlackOffers}>Ubicacion:</Text>{" "}
          {address}
        </Text>
        <Text style={ListJobsStyles.textOtherOffers}>
          <Text style={ListJobsStyles.textBlackOffers}>Salario:</Text>{" "}
          {salary_from}
        </Text>
        <Text style={ListJobsStyles.textOtherOffers}>
          <Text style={ListJobsStyles.textBlackOffers}>Beneficios:</Text>{" "}
          {benefits}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={ListJobsStyles.container}>
      <View style={{ alignItems: "center" }}>
        <TextInputSearch
          placeholder="Buscar empleo"
          value={searchTerm}
          setValue={setSearchTerm}
        />
        <View style={ListJobsStyles.containerHeader}>
          <TouchableOpacity style={ListJobsStyles.containerOrder}>
            <Text style={ListJobsStyles.textOrder}>Ordenar por</Text>
            <SimpleLineIcons name="arrow-down" size={15} color="#06C167" />
          </TouchableOpacity>
          <Text style={ListJobsStyles.textOffers}>
            <Text
              style={{
                ...ListJobsStyles.textOffers,
                fontFamily: "Poppins-medium",
                color: "#21212180",
              }}
            >
              {filteredData?.length}{" "}
            </Text>
            Ofertas de trabajo
          </Text>
        </View>
        {filteredData?.length > 0 ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            refreshing={isLoading}
            // onRefresh={fetchGetList}
            key={"#"}
            numColumns={1}
            scrollEnabled={true}
            data={filteredData}
            renderItem={(itemData) => (
              <DataItems
                id={itemData.item.id}
                name={itemData.item.name}
                benefits={itemData.item.benefits}
                address={itemData.item.address}
                description={itemData.item.description}
                salary_from={itemData.item.salary_from}
              />
            )}
          />
        ) : (
          <EmptyDateScreen
            title={`0 Resultados para (${searchTerm})`}
            subtitle="Verifique que la palabra esté bien escrita"
          />
        )}
      </View>
    </View>
  );
};

export default ListJobsScreen;
