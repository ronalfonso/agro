import React from "react";
import { useSelector } from "react-redux";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import SelectList from "../../../components/UI/custom-select-list";
import LargeButton from "../../../components/UI/custom-bottom";
import InputTextBorder from "../../../components/UI/custom-input-border";
import { Text, View } from "react-native";
import { createProductStyles } from "../../styles/CreateProductStyles";
import { validEmployeer } from "./valid-inputs";

const ListRegisterEmployeer = ({
  setIsLoading,
  isLoading,
  handleRegister,
  optionsList,
  setCredentials,
  creadentials,
  setErrors,
  errors,
}) => {
  const categories = useSelector((state) => state.categories.categoriesJobs);
  const departament = useSelector((state) => state.auth.departament);
  const municipalities = useSelector((state) => state.auth.municipalities);

  const validObject = (item) => Object.entries(item).length > 0;

  const renderMap = (value) =>
    value.data.map((item) => {
      return { key: item.id, value: item.name };
    });

  const handleSubmit = () => {
    setIsLoading(true);
    const {
      category_worker_id,
      description,
      name,
      department_id,
      municipalite_id,
      benefits,
      salary_from,
      address,
      directionRegister,
    } = creadentials;

    let isValid = validEmployeer(
      category_worker_id,
      description,
      name,
      department_id,
      municipalite_id,
      benefits,
      salary_from,
      address,
      directionRegister,
      errors,
      setErrors,
      setIsLoading
    );

    if (isValid) {
      handleRegister();
    }
  };

  return (
    <>
      <View style={{ ...createProductStyles.containerCheck, marginBottom: 30 }}>
        <BouncyCheckbox
          size={25}
          fillColor="#06C167"
          unfillColor="#FFFFFF"
          isChecked={creadentials.directionRegister}
          onPress={(isChecked) => {
            setCredentials({
              ...creadentials,
              directionRegister: isChecked,
            });
          }}
        />
        <Text style={createProductStyles.labelCheck}>
          usar dirección del registro.
        </Text>
      </View>

      {optionsList.map(({ keyboardType, type, label, name, footerText }) => {
        return (
          <View key={name}>
            {type === "select" && name === "category_worker_id" && (
              <SelectList
                name="category_worker_id"
                label="Categorías"
                errors={errors}
                error={errors.category_worker_id}
                placeholder="Categorías"
                data={validObject(categories) ? renderMap(categories) : []}
                setSelected={(val) => {
                  setCredentials({
                    ...creadentials,
                    category_worker_id: val,
                  });
                  setErrors({ ...errors, category_worker_id: "" });
                }}
              />
            )}

            {type === "select" &&
              name === "department_id" &&
              creadentials.directionRegister === false && (
                <SelectList
                  name="department_id"
                  label="Departamento"
                  errors={errors}
                  error={errors.department_id}
                  placeholder="Departamento"
                  data={validObject(departament) ? renderMap(departament) : []}
                  setSelected={(val) => {
                    setCredentials({
                      ...creadentials,
                      department_id: val,
                    });
                    setErrors({ ...errors, department_id: "" });
                  }}
                />
              )}

            {type === "select" &&
              name === "municipalite_id" &&
              creadentials.directionRegister === false && (
                <SelectList
                  name="municipalite_id"
                  label="Municipio"
                  errors={errors}
                  error={errors.municipalite_id}
                  placeholder="Municipio"
                  data={
                    validObject(municipalities) ? renderMap(municipalities) : []
                  }
                  setSelected={(val) => {
                    setCredentials({
                      ...creadentials,
                      municipalite_id: val,
                    });
                    setErrors({ ...errors, municipalite_id: "" });
                  }}
                />
              )}

            {type === "text" && name !== "address" ? (
              <InputTextBorder
                name={name}
                value={creadentials[name]}
                setCredentials={setCredentials}
                creadentials={creadentials}
                label={label}
                error={errors[name]}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType={keyboardType}
                footerText={footerText}
              />
            ) : (
              false
            )}

            {name === "address" && creadentials.directionRegister === false ? (
              <InputTextBorder
                name={name}
                value={creadentials[name]}
                setCredentials={setCredentials}
                creadentials={creadentials}
                label={label}
                error={errors[name]}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType={keyboardType}
                footerText={footerText}
              />
            ) : (
              false
            )}
          </View>
        );
      })}
      <LargeButton
        isLoading={isLoading}
        handleSubmit={handleSubmit}
        text={"Continuar"}
        disable={isLoading}
      />
    </>
  );
};

export default ListRegisterEmployeer;
