import React from "react";
import { Text, TouchableOpacity, View, Platform } from "react-native";
import * as DocumentPicker from "expo-document-picker";
import { useSelector } from "react-redux";
import { AntDesign } from "@expo/vector-icons";
import SelectList from "../../../components/UI/custom-select-list";
import LargeButton from "../../../components/UI/custom-bottom";
import InputTextBorder from "../../../components/UI/custom-input-border";
import { JobEmployeerStyles } from "../../styles/JobEmployeerStyles";
import { validEmployee } from "./valid-inputs";

const ListRegisterEmployee = ({
  setIsLoading,
  isLoading,
  handleRegister,
  optionsList,
  setCredentials,
  creadentials,
  setErrors,
  errors,
}) => {
  const categories = useSelector((state) => state.categories.categoriesJobs);

  const validObject = (item) => Object.entries(item).length > 0;

  const renderMap = (value) =>
    value.data.map((item) => {
      return { key: item.id, value: item.name };
    });

  const handlePdf = async () => {
    const file = await DocumentPicker.getDocumentAsync({ type: 'application/pdf' });
    if (file.type === 'success') {
      const fileInfo = {
        uri: file.uri,
        name: file.name,
        type: 'application/pdf',
      };
      setCredentials({ ...creadentials, nameCV: file.name, cv: fileInfo })
    }
  };

  const handleSubmit = () => {
    const { category_worker_id, description, cv } = creadentials;

    let isValid = validEmployee(
      category_worker_id,
      description,
      cv,
      errors,
      setErrors,
      setIsLoading
    );

    if (isValid) {
      handleRegister();
    }
  };

  return (
    <>
      {optionsList.map(({ keyboardType, type, label, name, footerText }) => {
        return (
          <View key={name}>
            {type === "select" && name === "category_worker_id" && (
              <SelectList
                name="category_worker_id"
                label="Categorías"
                errors={errors}
                error={errors.category_worker_id}
                placeholder="Categorías"
                data={validObject(categories) ? renderMap(categories) : []}
                setSelected={(val) => {
                  setCredentials({
                    ...creadentials,
                    category_worker_id: val,
                  });
                  setErrors({ ...errors, category_worker_id: "" });
                }}
              />
            )}

            {type === "text" && name !== "address" ? (
              <InputTextBorder
                name={name}
                value={creadentials[name]}
                setCredentials={setCredentials}
                creadentials={creadentials}
                label={label}
                error={errors[name]}
                setErrors={setErrors}
                errorsItem={errors}
                password={false}
                keyboardType={keyboardType}
                footerText={footerText}
              />
            ) : (
              false
            )}
          </View>
        );
      })}
      <View style={JobEmployeerStyles.containerUploadDates}>
        <TouchableOpacity
          style={JobEmployeerStyles.bottomUploadDates}
          onPress={handlePdf}
        >
          <AntDesign name="clouduploado" size={27} color="#06C167" />
          <Text style={JobEmployeerStyles.textDatesUpload}>
            {creadentials.nameCV !== false
              ? creadentials.nameCV
              : "Subir mis datos (Hoja de vida)"}
          </Text>
        </TouchableOpacity>
        <Text style={JobEmployeerStyles.textError}>{errors.cv}</Text>
      </View>

      <LargeButton
        isLoading={isLoading}
        handleSubmit={handleSubmit}
        text={"Continuar"}
        disable={isLoading}
      />
    </>
  );
};

export default ListRegisterEmployee;
