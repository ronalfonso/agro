import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { JobEmployeerStyles } from "../../styles/JobEmployeerStyles";
import CardContainerGray from "../../../components/UI/CardContainerGray";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import { dataEmployeer } from "./data-listing";
import { validCvEmployee } from "../../../store/actions/employeers";
import useMessage from "../../../hooks/useMessage";
import { useToast } from "react-native-toast-notifications";
import ProgressLoading from "../../../components/UI/custom-progress-loading";
import { useNavigation } from "@react-navigation/native";

const JobBoardScren = (props) => {
  const Toast = useToast();
  const { dangerMessage } = useMessage(Toast);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [cv, setCv] = useState(false);
  const categoriesJobs = useSelector(
    (state) => state.categories.categoriesJobs
  );

  useEffect(() => {
    const onFocusData = navigation.addListener("focus", () => {
      setIsLoading(true);
      dispatch(validCvEmployee()).then((data) => {
        setCv(data.message);
        setIsLoading(false);
      });
    });
    return onFocusData;
  }, [navigation]);

  if (isLoading) {
    return <ProgressLoading />;
  }

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={JobEmployeerStyles.scrollStyle}
    >
      <View style={{ ...JobEmployeerStyles.container, marginBottom: 50 }}>
        <Text style={JobEmployeerStyles.textTitle}>
          Para facilitar tu proceso dinos que buscas
        </Text>
        <View style={JobEmployeerStyles.containerBottoms}>
          <TouchableOpacity
            style={JobEmployeerStyles.customButton}
            onPress={() => {
              if (cv === "No se encontraron datos") {
                props.navigation.navigate("registration-form", {
                  optionsList: dataEmployeer.employee,
                  title: "Busco empleo",
                  type: "employee",
                });
              } else {
                dangerMessage("Ya tiene sus datos cargados");
              }
            }}
          >
            <Text style={JobEmployeerStyles.textBottomEmployer}>
              Busco empleo
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={JobEmployeerStyles.customButton}
            onPress={() => {
              props.navigation.navigate("registration-form", {
                optionsList: dataEmployeer.employer,
                title: "Soy empleador",
                type: "employer",
              });
            }}
          >
            <Text style={JobEmployeerStyles.textBottomEmployer}>
              Soy empleador
            </Text>
          </TouchableOpacity>
        </View>

        <CardContainerGray
          title="Categorías de empleos"
          footer={false}
          titleFooter=""
          path=""
          props={props}
          positionFooter={{
            alignItems: "flex-start",
          }}
        >
          <View style={JobEmployeerStyles.containerLine} />
          {categoriesJobs?.data?.map(({ id, name }) => {
            return (
              <View style={{ height: hp(10) }} key={id}>
                <View style={JobEmployeerStyles.containerCategoriItems}>
                  <TouchableOpacity
                    onPress={() => {
                      props.navigation.navigate("list-jobs", {
                        idCategori: id,
                      });
                    }}
                    style={JobEmployeerStyles.containerCategoriItem}
                  >
                    <View style={JobEmployeerStyles.containerItemLeft}>
                      <View style={JobEmployeerStyles.circleCategori} />
                    </View>

                    <View style={JobEmployeerStyles.containerItemRight}>
                      <Text
                        style={{
                          fontFamily: "Poppins-medium",
                          fontSize: fz(2.1),
                        }}
                        numberOfLines={2}
                      >
                        {name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            );
          })}
          <View style={JobEmployeerStyles.containerLine} />
        </CardContainerGray>
      </View>
    </ScrollView>
  );
};

export default JobBoardScren;
