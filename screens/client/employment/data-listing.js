export const dataEmployeer = {
  employee: [
    {
      name: "category_worker_id",
      label: "Categorías",
      type: "select",
      keyboardType: "default",
    },
    {
      name: "description",
      label: "Descripción",
      type: "text",
      footerText: "Escriba una descripción mínimo de 12 caracteres",
      keyboardType: "default",
    },
  ],
  employer: [
    {
      name: "category_worker_id",
      label: "Categorías",
      type: "select",
      keyboardType: "default",
    },
    {
      name: "department_id",
      label: "Departamento",
      type: "select",
      keyboardType: "default",
    },
    {
      name: "municipalite_id",
      label: "Municipio",
      type: "select",
      keyboardType: "default",
    },
    {
      name: "name",
      label: "Nombre",
      type: "text",
      footerText: "Escriba el nombre tal y como figura en su documento",
      keyboardType: "default",
    },
    {
      name: "salary_from",
      label: "Salario",
      type: "text",
      footerText: "Escriba su salario esperado o la cantidad exacta",
      keyboardType: "default",
    },
    {
      name: "description",
      label: "Descripción",
      type: "text",
      footerText: "Escriba una descripción mínimo de 12 caracteres",
      keyboardType: "default",
    },
    {
      name: "benefits",
      label: "Beneficio",
      type: "text",
      footerText: "Escriba los beneficios a convenir",
      keyboardType: "default",
    },
    {
      name: "address",
      label: "Dirección",
      type: "text",
      footerText: "Escriba su dirección de residencia",
      keyboardType: "default",
    },
  ],
};
