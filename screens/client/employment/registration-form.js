import React, { useEffect, useState } from "react";
import { ScrollView, View, Text } from "react-native";
import { JobEmployeerStyles } from "../../styles/JobEmployeerStyles";
import { useDispatch, useSelector } from "react-redux";
import { getDepartament, getMunicipalities } from "../../../store/actions/auth";
import { useToast } from "react-native-toast-notifications";
import useMessage from "../../../hooks/useMessage";
import {
  registerEmployee,
  registerEmployeer,
} from "../../../store/actions/employeers";
import ListRegisterEmployee from "./index-employee";
import ListRegisterEmployeer from "./index-employeer";

const RegisterForm = (props) => {
  const dispatch = useDispatch();
  const Toast = useToast();
  const { successMessage, dangerMessage } = useMessage(Toast);
  const { optionsList, title, type } = props.route.params;
  const userData = useSelector((state) => state.auth.userData);
  const [isLoading, setIsLoading] = useState(false);
  const [creadentials, setCredentials] = useState({
    user_id: userData.id,
    category_worker_id: "",
    description: "",
    salary_from: "",
    benefits: "",
    department_id: "",
    municipalite_id: "",
    address: "",
    name: "",
    directionRegister: true,
    cv: "",
    nameCV: false,
  });
  
  const [errors, setErrors] = useState({
    category_worker_id: "",
    description: "",
    salary_from: "",
    benefits: "",
    department_id: "",
    municipalite_id: "",
    address: "",
    name: "",
    cv: "",
  });

  useEffect(() => {
    props.navigation.setOptions({
      title: title,
    });
    dispatch(getDepartament());
  }, [props.route.params]);

  useEffect(() => {
    if (creadentials.department_id !== "") {
      dispatch(getMunicipalities(creadentials.department_id));
    }
  }, [creadentials.department_id]);

  const handleRegister = () => {
    setIsLoading(true);
    switch (type) {
      case "employee":
        dispatch(registerEmployee(creadentials)).then((data) => {
          setIsLoading(false);
          if (data.status === "Success") {
            successMessage(data.message);
            props.navigation.navigate("job-board");
          } else {
            dangerMessage(data.message);
          }
        });
        break;
      case "employer":
        dispatch(registerEmployeer(creadentials)).then((data) => {
          setIsLoading(false);
          if (data.status === "Success") {
            successMessage(data.message);
            props.navigation.navigate("job-board");
          } else {
            dangerMessage(data.message);
          }
        });
        break;
      default:
        break;
    }
  };

  return (
    <ScrollView
      style={JobEmployeerStyles.scrollStyle}
      showsVerticalScrollIndicator={false}
    >
      <View style={{ alignItems: "center", marginBottom: 50 }}>
        <Text style={JobEmployeerStyles.textTitle}>{title}</Text>
        {type === "employer" && (
          <ListRegisterEmployeer
            setIsLoading={setIsLoading}
            isLoading={isLoading}
            handleRegister={handleRegister}
            optionsList={optionsList}
            setCredentials={setCredentials}
            creadentials={creadentials}
            setErrors={setErrors}
            errors={errors}
          />
        )}

        {type === "employee" && (
          <ListRegisterEmployee
            setIsLoading={setIsLoading}
            isLoading={isLoading}
            handleRegister={handleRegister}
            optionsList={optionsList}
            setCredentials={setCredentials}
            creadentials={creadentials}
            setErrors={setErrors}
            errors={errors}
          />
        )}
      </View>
    </ScrollView>
  );
};

export default RegisterForm;
