export const validEmployee = (
  category_worker_id,
  description,
  cv,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;

  if (category_worker_id === "") {
    setErrors({ ...errors, category_worker_id: "La categoría es requerida" });
    setIsLoading(false);
    isValid = false;
  }

  if (description.length === 0) {
    setErrors({ ...errors, description: "La descripción es requerida" });
    setIsLoading(false);
    isValid = false;
  } else if (description.length > 0 && description.length < 8) {
    setErrors({
      ...errors,
      description: "La descripción debe tener al menos 8 caracteres",
    });
    setIsLoading(false);
    isValid = false;
  }

  if (cv === "") {
    setErrors({ ...errors, cv: "La hoja de vida es requerida" });
    setIsLoading(false);
    isValid = false;
  }

  return isValid;
};

export const validEmployeer = (
  category_worker_id,
  description,
  name,
  department_id,
  municipalite_id,
  benefits,
  salary_from,
  address,
  directionRegister,
  errors,
  setErrors,
  setIsLoading
) => {
  let isValid = true;

  if (category_worker_id === "") {
    setErrors({ ...errors, category_worker_id: "La categoría es requerida" });
    setIsLoading(false);
    isValid = false;
  }

  if (description.length === 0) {
    setErrors({ ...errors, description: "La descripción es requerida" });
    setIsLoading(false);
    isValid = false;
  } else if (description.length > 0 && description.length < 8) {
    setErrors({
      ...errors,
      description: "La descripción debe tener al menos 8 caracteres",
    });
    setIsLoading(false);
    isValid = false;
  }

  if (benefits === "") {
    setErrors({ ...errors, benefits: "El beneficio es requerido" });
    setIsLoading(false);
    isValid = false;
  }

  if (salary_from === "") {
    setErrors({ ...errors, salary_from: "El salario es requerido" });
    setIsLoading(false);
    isValid = false;
  }

  if (name.length === 0) {
    setErrors({ ...errors, name: "El nombre requerido" });
    setIsLoading(false);
    isValid = false;
  } else if (name.length > 0 && name.length < 3) {
    setErrors({
      ...errors,
      name: "El nombre debe tener al menos 3 caracteres",
    });
    setIsLoading(false);
    isValid = false;
  }

  if (!directionRegister && department_id === "") {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, department_id: "Seleccione un departamento" });
  }

  if (!directionRegister && municipalite_id === "") {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, municipalite_id: "Seleccione un municipio" });
  }

  if (!directionRegister && address === "") {
    isValid = false;
    setIsLoading(false);
    setErrors({ ...errors, address: "La dirección en requerida" });
  } else if (!directionRegister && address.length < 8) {
    isValid = false;
    setIsLoading(false);
    setErrors({
      ...errors,
      address: "La dirección debe tener al menos 8 caracteres",
    });
  }

  return isValid;
};
