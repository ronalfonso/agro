import React, { useState } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text
} from "react-native";
import { homeStyles } from "../../styles/HomeStyles";
import MetricsItems from "../../components/MetricsItems";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";

const PriceExchangeScreen = () => {
  return (
    <View style={{ flex: 1, width: wp(100), alignItems: "center", backgroundColor: "#FFFFFF" }}>
      <View style={{ flex: 1, width: wp(90), paddingTop: 20 }}>
        <Text style={{ textAlign: "center", marginBottom: 20, fontSize: fz(2.5) }}>Para facilitar tu proceso dinos que buscas</Text>
        <MetricsItems />
      </View>
    </View>
  );
};

export default PriceExchangeScreen;

const styles = StyleSheet.create({
  lineContainer: {
    borderBottomWidth: 1,
    borderBottomColor: "rgba(33,33,33,0.5)",
  }
});
