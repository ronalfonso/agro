import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ForgotPassword from "../../screens/auth/ForgotPassword";
import { HomeNavigator } from "../homeStack/HomeNavigation";
import LoginScreen from "../../screens/auth/login";
import WelcomeScreen from "../../screens/auth/welcome-screen";
import RegisterScreen from "../../screens/auth/register";
import SelectOptionOtp from "../../screens/auth/options-opt";
import { defaultNavOptions, optionGoBack } from "../UtilsNavigation";
import EnterCodeOtpScreen from "../../screens/auth/validation-opt";

const AuthStackNavigator = createNativeStackNavigator();

export const AuthNavigator = () => {
  return (
    <AuthStackNavigator.Navigator>
      <AuthStackNavigator.Screen
        name="homes"
        component={HomeNavigator}
        options={{ headerShown: false }}
      />
      <AuthStackNavigator.Screen
        name="WelcomeScreen"
        component={WelcomeScreen}
        options={{ headerShown: false }}
      />
      <AuthStackNavigator.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <AuthStackNavigator.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{ headerShown: false }}
      />
      <AuthStackNavigator.Screen
        name="ForgotPasswordScreen"
        component={ForgotPassword}
        options={{ headerShown: false }}
      />
      <AuthStackNavigator.Screen
        name="SelectOpt"
        component={SelectOptionOtp}
        options={({ navigation }) => ({
          title: "Verificación",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AuthStackNavigator.Screen
        name="ValidCode"
        component={EnterCodeOtpScreen}
        options={({ navigation }) => ({
          title: "Verificación",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
    </AuthStackNavigator.Navigator>
  );
};
