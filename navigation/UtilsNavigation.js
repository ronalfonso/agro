import {percentageToDPSize} from "../utils/CalculatePD";
import {useSelector} from "react-redux";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {AntDesign, Entypo} from "@expo/vector-icons";
import {RFPercentage as fz} from "react-native-responsive-fontsize";
import React from "react";

const stylesNotification = StyleSheet.create({
    containerBadge: {
        backgroundColor: "#FF0000",
        borderRadius: 100,
        position: "absolute",
        right: 0,
        width: "50%",
        height: "50%",
        zIndex: 55,
        justifyContent: "center",
        alignItems: "center",
    },
    numberText: {
        fontSize: fz(1.5),
        color: "#fff",
        fontFamily: "Poppins-medium",
        textAlign: "center",
    },
});

export const defaultNavOptions = {
    headerLeftContainerStyle: { paddingLeft: 15 },
    headerRightContainerStyle: { paddingRight: 15 },
    headerStyle: { backgroundColor: "#06C167" },
    headerTitleStyle: {
        fontFamily: "Poppins",
        marginTop: 3,
        fontSize: percentageToDPSize(16),
    },
};

export const optionDrawer = (navigation, total) => {
    const token = useSelector((state) => state.auth.token);

    return {
        headerLeft: () => (
            <TouchableOpacity
                style={{ marginRight: 15, top: -2 }}
                onPress={() => {
                    if (token !== null) {
                        navigation.toggleDrawer();
                    } else if (token === null) {
                        navigation.navigate("WelcomeScreen");
                    }
                }}
            >
                {total > 0 && (
                    <View style={stylesNotification.containerBadge}>
                        <Text style={stylesNotification.numberText}>{total}</Text>
                    </View>
                )}
                <Entypo name="menu" size={35} color="black" />
            </TouchableOpacity>
        ),
    };
};

export const optionGoBack = (navigation) => {
    return {
        headerLeft: () => (
            <TouchableOpacity
                style={{ marginRight: 15, top: -2 }}
                onPress={() => {
                    navigation.goBack();
                }}
            >
                <AntDesign name="arrowleft" size={25} color="#000" />
            </TouchableOpacity>
        ),
    };
};

export const logoBar = (navigation) => {
    const src = require("../assets/auth/logo_top.png")
    return {
        headerRight: () => (<Image source={src} style={{width: 38, height: 34}} />)
    }
}