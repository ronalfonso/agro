import {
  createDrawerNavigator,
  DrawerItemList,
} from "@react-navigation/drawer";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { RFPercentage as fz } from "react-native-responsive-fontsize";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from "react-native";
import DrawerHeader from "../components/DrawerHeader";
import {
    FontAwesome,
    Ionicons,
    AntDesign,
    Entypo,
    MaterialIcons, FontAwesome6,
} from "@expo/vector-icons";
import SearchSreen from "../screens/client/SearchSreen";
import { useSelector } from "react-redux";
import { Linking } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NotificationsNavigator } from "./notificationStack/NotificationNavigation";
import { AccountNavigator } from "./accountStack/AccountNavigation";
import { MyProductNavigator } from "./productStack/ProductNavigation";
import { HomeNavigator } from "./homeStack/HomeNavigation";
import { MyFavoritesNavigator } from "./favoritiesStack/FavoritiesNavigation";
import { percentageToDPSize } from "../utils/CalculatePD";
import {MyReferralNavigator} from "./referralStack/ReferralStack";


const SearchStackNavigator = createNativeStackNavigator();

export const SearchNavigator = () => {
  return (
    <SearchStackNavigator.Navigator>
      <SearchStackNavigator.Screen
        name="Search"
        component={SearchSreen}
        options={{ headerShown: false }}
      />
    </SearchStackNavigator.Navigator>
  );
};

const CustomDrawerContent = (props) => {
  return (
    <ScrollView>
      <DrawerHeader />
      <DrawerItemList {...props} />
      <TouchableOpacity
        onPress={() => {
          const url = "https://digiagro.co/planes";
          Linking.openURL(url);
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingHorizontal: 20,
            marginTop: 15,
          }}
        >
          <AntDesign name="star" size={24} color="black" />
          <Text
            style={{
              marginLeft: 35,
              fontFamily: "Poppins-medium",
              fontSize: fz(2),
            }}
          >
            Planes
          </Text>
        </View>
      </TouchableOpacity>
    </ScrollView>
  );
};

const GeneralAppNavigator = createDrawerNavigator();

export const GeneralNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <GeneralAppNavigator.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      screenOptions={{
        drawerStyle: {
          width: wp(100),
          paddingBottom: 20,
        },
        drawerLabelStyle: { fontSize: fz(2) },
        drawerActiveTintColor: "#06C167",
        drawerInactiveTintColor: "#000",
      }}
    >
      <GeneralAppNavigator.Screen
        name="Inicio"
        component={HomeNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <FontAwesome name="home" size={30} color={props.color} />
          ),
        })}
      />
      <GeneralAppNavigator.Screen
        name="Buscar"
        component={SearchNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <FontAwesome name="search" size={30} color={props.color} />
          ),
        })}
      />
      <GeneralAppNavigator.Screen
        name="Notificaciones"
        component={NotificationsNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <>
              {totalNotification > 0 && (
                <View
                  style={{
                    backgroundColor: "#FF0000",
                    borderRadius: 100,
                    position: "absolute",
                    right: 7,
                    width: "7%",
                    height: "70%",
                    zIndex: 55,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      fontWeight: "bold",
                      color: "#fff",
                      fontSize: fz(1.5),
                    }}
                  >
                    {totalNotification}
                  </Text>
                </View>
              )}

              <Ionicons name="notifications" size={30} color={props.color} />
            </>
          ),
        })}
      />
      <GeneralAppNavigator.Screen
        name="Mis productos"
        component={MyProductNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <FontAwesome name="shopping-cart" size={30} color={props.color} />
          ),
        })}
      />
        <GeneralAppNavigator.Screen
            name="Mis referidos"
            component={MyReferralNavigator}
            options={() => ({
                headerShown: false,
                drawerIcon: (props) => (
                    <FontAwesome6 name="people-arrows" size={25} color={props.color} />
                ),
            })}
        />
      <GeneralAppNavigator.Screen
        name="Favoritos"
        component={MyFavoritesNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <FontAwesome name="heart" size={30} color={props.color} />
          ),
        })}
      />
      <GeneralAppNavigator.Screen
        name="Mi cuenta"
        component={AccountNavigator}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <FontAwesome name="user-circle" size={30} color={props.color} />
          ),
        })}
      />
      {/* <GeneralAppNavigator.Screen
        name="Ayuda"
        component={""}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <Entypo name="help-with-circle" size={30} color={props.color} />
          ),
        })}
      /> */}
      <GeneralAppNavigator.Screen
        name="Categorías"
        component={""}
        options={() => ({
          drawerItemStyle: {
            borderBottomColor: "#000",
            borderBottomWidth: 1,
            borderTopWidth: 1,
            paddingBottom: 20,
            paddingTop: 15,
          },
          headerShown: false,
          drawerIcon: (props) => (
            <MaterialIcons name="category" size={30} color={props.color} />
          ),
        })}
      />
      <GeneralAppNavigator.Screen
        name="Vender"
        component={""}
        options={() => ({
          headerShown: false,
          drawerIcon: (props) => (
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color={props.color}
            />
          ),
        })}
      />
    </GeneralAppNavigator.Navigator>
  );
};
