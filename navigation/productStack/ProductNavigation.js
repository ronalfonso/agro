import { useSelector } from "react-redux";
import CreateProductUser from "../../screens/client/product/create-product";
import ListProducts from "../../screens/client/product/list-products";
import ProductDetailScreen from "../../screens/client/product/product-detail";
import { defaultNavOptions, optionDrawer, optionGoBack } from "../UtilsNavigation";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const MyProductStackNavigator = createNativeStackNavigator();

export const MyProductNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <MyProductStackNavigator.Navigator>
      <MyProductStackNavigator.Screen
        name="list-products"
        component={ListProducts}
        options={({ navigation }) => ({
          title: "Mis productos",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
        })}
      />
      <MyProductStackNavigator.Screen
        name="product-detail"
        component={ProductDetailScreen}
        options={({ navigation }) => ({
          title: "Mis productos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <MyProductStackNavigator.Screen
        name="create-product"
        component={CreateProductUser}
        options={({ navigation }) => ({
          title: "Agregar Producto",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
    </MyProductStackNavigator.Navigator>
  );
};
