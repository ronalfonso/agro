import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { GeneralNavigator } from "./Navigation";
import { useSelector } from "react-redux";
import StartupScreen from "../screens/StartupScreen";
import { AuthNavigator } from "./authStack/AuthNavigation";

const AppNavigator = () => {
  const isAuth = useSelector((state) => !!state.auth.token);
  const didTryAutoLogin = useSelector((state) => !!state.auth.didTryAutoLogin);

  return (
    <NavigationContainer>
      {!isAuth && <AuthNavigator />}
      {isAuth && <GeneralNavigator />}
      {!isAuth && !didTryAutoLogin && <StartupScreen />}
    </NavigationContainer>
  );
};

export default AppNavigator;
