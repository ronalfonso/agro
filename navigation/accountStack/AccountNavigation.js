import { createNativeStackNavigator } from "@react-navigation/native-stack";

import MyAccountScreen from "../../screens/client/user_options/index";
import UserInfoScreen from "../../screens/client/user_options/user-info";
import UserDocumentSelectorScreen from "../../screens/client/user_options/user-document-selector";
import UserProfileScreen from "../../screens/client/user_options/user-profile";
import UserPrivacyScreen from "../../screens/client/user_options/privacy-policy";
import UserEditInfo from "../../screens/client/user_options/user-edit-info";
import SelectOptionOtp from "../../screens/auth/options-opt";
import EnterCodeOtpScreen from "../../screens/auth/validation-opt";
import { useSelector } from "react-redux";
import CreateProductUser from "../../screens/client/product/create-product";
import ListProducts from "../../screens/client/product/list-products";
import {defaultNavOptions, optionDrawer, optionGoBack} from "../UtilsNavigation";

const AccountStackNavigator = createNativeStackNavigator();

export const AccountNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <AccountStackNavigator.Navigator>
      <AccountStackNavigator.Screen
        name="my-account"
        component={MyAccountScreen}
        options={({ navigation }) => ({
          title: "Mi cuenta",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
        })}
      />
      <AccountStackNavigator.Screen
        name="user-profile"
        component={UserProfileScreen}
        options={({ navigation }) => ({
          title: "Mi Perfil",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="user-info"
        component={UserInfoScreen}
        options={({ navigation }) => ({
          title: "Mis datos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="user-document-selector"
        component={UserDocumentSelectorScreen}
        options={({ navigation }) => ({
          title: "Seguridad",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="SelectOpt"
        component={SelectOptionOtp}
        options={({ navigation }) => ({
          title: "Verificación",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="user-edit-info"
        component={UserEditInfo}
        options={({ navigation }) => ({
          title: "Editar datos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="list-products"
        component={ListProducts}
        options={({ navigation }) => ({
          title: "Mis productos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="create-product"
        component={CreateProductUser}
        options={({ navigation }) => ({
          title: "Agregar Producto",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="privacy-user"
        component={UserPrivacyScreen}
        options={({ navigation }) => ({
          title: "Privacidad",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <AccountStackNavigator.Screen
        name="ValidCode"
        component={EnterCodeOtpScreen}
        options={({ navigation }) => ({
          title: "Verificación",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
    </AccountStackNavigator.Navigator>
  );
};
