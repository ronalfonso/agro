import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { defaultNavOptions, optionDrawer } from "../UtilsNavigation";
import { useSelector } from "react-redux";
import MyFavoritesScreen from "../../screens/client/MyFavoritesScreen";

const MyFavoritesStackNavigator = createNativeStackNavigator();

export const MyFavoritesNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <MyFavoritesStackNavigator.Navigator>
      <MyFavoritesStackNavigator.Screen
        name="Favorites"
        component={MyFavoritesScreen}
        options={({ navigation }) => ({
          title: "Mis favoritos",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
        })}
      />
    </MyFavoritesStackNavigator.Navigator>
  );
};
