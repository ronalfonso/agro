import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {defaultNavOptions, logoBar, optionDrawer, optionGoBack} from "../UtilsNavigation";
import { useSelector } from "react-redux";
import MyFavoritesScreen from "../../screens/client/MyFavoritesScreen";
import StoreSuppliesDetailScreen from "../../screens/client/storeSupplies/StoreSuppliesDetailScreen";
import StoreSuppliesScreen from "../../screens/client/storeSupplies/StoreSuppliesScreen";
import PriceExchangeScreen from "../../screens/client/priceExchange/PriceExchangeScreen";
import OffertTransportScreen from "../../screens/client/transporter/OffertTransportScreen";
import DetailJobScreen from "../../screens/client/employment/DetailJobScreen";
import ListJobsScreen from "../../screens/client/employment/list-jobs";
import HomeScreen from "../../screens/client/HomeScreen";
import JobBoardScren from "../../screens/client/employment/job-board";
import TransporterScreenRegister from "../../screens/client/transporter/TransporterRegisterScreen";
import CreateProductUser from "../../screens/client/product/create-product";
import ListProducts from "../../screens/client/product/list-products";
import ProductDetailScreen from "../../screens/client/product/product-detail";
import ProductsAllScreen from "../../screens/client/product/all-products";
import RegisterForm from "../../screens/client/employment/registration-form";
import TransportRegisterScreen from "../../screens/client/transporter/TransportRegisterScreen";

const HomeStackNavigator = createNativeStackNavigator();

export const HomeNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <HomeStackNavigator.Navigator>
      <HomeStackNavigator.Screen
        name="Home"
        component={HomeScreen}
        options={({ navigation }) => ({
          title: "",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
          ...logoBar(navigation)
        })}
      />
      <HomeStackNavigator.Screen
        name="job-board"
        component={JobBoardScren}
        options={({ navigation }) => ({
          title: "Bolsa de empleos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="list-jobs"
        component={ListJobsScreen}
        options={({ navigation }) => ({
          title: "Listado de empleos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="DetailJobBoard"
        component={DetailJobScreen}
        options={({ navigation }) => ({
          title: "Bolsa de empleados",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="registration-form"
        component={RegisterForm}
        options={({ navigation }) => ({
          title: "",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="Transporter"
        component={TransporterScreenRegister}
        options={({ navigation }) => ({
          title: "Registro de transporte",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="Transport"
        component={TransportRegisterScreen}
        options={({ navigation }) => ({
            title: "Registro de vehículo",
            ...defaultNavOptions,
            ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="OffertTransporter"
        component={OffertTransportScreen}
        options={({ navigation }) => ({
          title: "",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="PriceExchange"
        component={PriceExchangeScreen}
        options={({ navigation }) => ({
          title: "Bolsa de precios",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="StoreSupplies"
        component={StoreSuppliesScreen}
        options={({ navigation }) => ({
          title: "Tiendas de suministros",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="StoreSuppliesDetail"
        component={StoreSuppliesDetailScreen}
        options={({ navigation }) => ({
          title: "",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="Favorites"
        component={MyFavoritesScreen}
        options={({ navigation }) => ({
          title: "Mis favoritos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="create-product"
        component={CreateProductUser}
        options={({ navigation }) => ({
          title: "Agregar Producto",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="allProducts"
        component={ProductsAllScreen}
        options={({ navigation }) => ({
          title: "Todos los productos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="product-detail"
        component={ProductDetailScreen}
        options={({ navigation }) => ({
          title: "Mis productos",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
      <HomeStackNavigator.Screen
        name="list-products"
        component={ListProducts}
        options={({ navigation }) => ({
          title: "Mis productos",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
        })}
      />
    </HomeStackNavigator.Navigator>
  );
};
