import {useSelector} from "react-redux";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {defaultNavOptions, logoBar, optionDrawer} from "../UtilsNavigation";
import ListReferrals from "../../screens/client/referral/list-referrals";
import RegisterReferral from "../../screens/client/referral/RegisterReferral";


const MyReferralStackNavigator = createNativeStackNavigator();

export const MyReferralNavigator = () => {
    const totalNotification = useSelector(
        (state) => state.notifications.totalNotification
    );

    return (
        <MyReferralStackNavigator.Navigator>
            <MyReferralStackNavigator.Screen
                name="list-referrals"
                component={ListReferrals}
                options={({ navigation }) => ({
                    title: "Mis referidos",
                    ...defaultNavOptions,
                    ...optionDrawer(navigation, totalNotification),
                })}
            />
            <MyReferralStackNavigator.Screen
                name="RegisterReferral"
                component={RegisterReferral}
                options={{headerShown: false}}
            />
        </MyReferralStackNavigator.Navigator>
    )
}