import { createNativeStackNavigator } from "@react-navigation/native-stack";
import NotificationScreen from "../../screens/notifications/NotificationScreen";
import DetailNotification from "../../screens/notifications/DetailNotification";
import { defaultNavOptions, optionDrawer, optionGoBack } from "../UtilsNavigation";
import { useSelector } from "react-redux";

const NotificationStackNavigator = createNativeStackNavigator();

export const NotificationsNavigator = () => {
  const totalNotification = useSelector(
    (state) => state.notifications.totalNotification
  );

  return (
    <NotificationStackNavigator.Navigator>
      <NotificationStackNavigator.Screen
        name="notifications"
        component={NotificationScreen}
        options={({ navigation }) => ({
          title: "Mis notificaciones",
          ...defaultNavOptions,
          ...optionDrawer(navigation, totalNotification),
        })}
      />
      <NotificationStackNavigator.Screen
        name="detail-notification"
        component={DetailNotification}
        options={({ navigation }) => ({
          title: "Mis notificaciones",
          ...defaultNavOptions,
          ...optionGoBack(navigation),
        })}
      />
    </NotificationStackNavigator.Navigator>
  );
};
