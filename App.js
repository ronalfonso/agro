import React, {useEffect} from "react";
import {StatusBar} from "react-native";
import AppNavigator from "./navigation/AppNavigator";
import {useFonts} from "expo-font";
import {Provider} from "react-redux";
import ReduxThunk from "redux-thunk";
import {createStore, applyMiddleware} from "redux";
import {ToastProvider} from "react-native-toast-notifications";
import {FontAwesome} from "@expo/vector-icons";
import {MaterialIcons} from "@expo/vector-icons";
import {AntDesign} from "@expo/vector-icons";
import * as SplashScreen from 'expo-splash-screen';
import {LogBox} from 'react-native';
import store from "./store/store";

LogBox.ignoreLogs(['new NativeEventEmitter']);

export default function App() {
    // Carga las fuentes y devuelve un valor booleano
    let [fontsLoaded] = useFonts({
        Poppins: require("./assets/fonts/Poppins-Regular.ttf"),
        "Poppins-medium": require("./assets/fonts/Poppins-Medium.ttf"),
        "Poppins-bold": require("./assets/fonts/Poppins-Bold.ttf"),
    });

    // Mantiene la pantalla de carga visible mientras se cargan las fuentes
    SplashScreen.preventAutoHideAsync();

    // Oculta la pantalla de carga cuando las fuentes estén listas
    useEffect(() => {
        if (fontsLoaded) {
            SplashScreen.hideAsync();
        }
    }, [fontsLoaded]);

    if (!fontsLoaded) {
        return null;
    }

    return (
        <ToastProvider
            successColor="#06C167"
            successIcon={<FontAwesome name="check" size={24} color="#fff"/>}
            dangerIcon={<MaterialIcons name="dangerous" size={24} color="#fff"/>}
            warningIcon={<AntDesign name="warning" size={24} color="#fff"/>}
        >
            <StatusBar backgroundColor="#06C167" barStyle="dark-content"/>
            <Provider store={store}>
                <AppNavigator/>
            </Provider>
        </ToastProvider>
    );
}


