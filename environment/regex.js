export const emailRegex =
  /^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const numberDocumentRegex = /^\d{5,10}((\s|[-])\d{1}[A-Z]{1})?$/;

export const phoneNumberRegex = /^\d{10}$/;
